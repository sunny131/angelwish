<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization","1",{packages:["corechart"]});google.setOnLoadCallback(drawChart);function drawChart(){var data=google.visualization.arrayToDataTable([['Company Name','Total Transaction'],['Deloitte',173],['Goodwin Procter ',11],['IvyLife',8],['Shake Your Network ',11],['Town',1],['Fidelity',233],['NYC',21],['Moodys',305],['Ex-Animo',13],['Princeton 2011',26],['Corporate',7],['Starbucks',2],['Mason\'s Cellar',2],['Ralph Lauren',12],['Telx',2],['TKE',11],['Stamos Capital',1],]);var options={title:'My Daily Activities'};var chart=new google.visualization.PieChart(document.getElementById('piechart'));chart.draw(data,options);}
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
   
  </body>
</html>
