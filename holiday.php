<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text"><h1>2011 Holiday Appeal</h1>
    <h2> Its Never Too Late to Help Children in Need with Chronic Illnesses</h2>

    <div class="donate">
        <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Please Support the Angelwish Holiday Appeal</b></div>
	<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}

		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
			document.getElementById('donation_form').submit();
		}
	</script>

	<td>	<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
			  <option value="50">$50</option>
			  <option value="100">$100</option>
			  <option value="500">$500</option>
              <option value="1000">$1,000</option>
              <option value="1250">$1,250</option>
              <option value="2500">$2,500</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <form id="donation_form" action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
        <input type="hidden" name="immediateReturn" value="1" >

	  <input type="hidden" name="collectShippingAddress" value="0" >

	  <input type="hidden" name="signatureVersion" value="2" >

	  <input type="hidden" name="signatureMethod" value="HmacSHA256" >

	  <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >

	  <input type="hidden" name="referenceId" value="Angelwish 2011 Holiday Giving" >

	  <input type="hidden" name="signature" value="l+jSW8g+zzl21vXFI9VsIrwyQQc0dz3aBiDQuO5VWbk=" >

	  <input type="hidden" name="isDonationWidget" value="1" >

	  <input type="hidden" name="description" value="Angelwish 2011 Holiday Giving" >

	  <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >

	  <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks/" >

	  <input type="hidden" name="processImmediate" value="1" >

	  <input type="hidden" name="cobrandingStyle" value="logo" >

	  <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >

	  <input type="hidden" id='amount' name="amount" size="8"  value="">

	  <td><input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0"></td>
	</form>
	</tr>
	</table>
    </div>

<p>The Holidays are here, yet for 10.2 Million children living with Chronic Illnesses such as Asthma, Diabetes, HIV/AIDS, the holidays might be canceled or postponed because their families can't afford holiday presents. <br/> <br/>
For over 12 years, Angelwish has been helping donors just like you stretch small contributions into amazing gifts that touch the lives of families and children who are dealing with illnesses that put a great deal of stress on an economically fragile group. This year, we need your help more than ever. Please Join us and grant a holiday or birthday wish to a child living with Chronic Illness. The Power to Change a Child's Life is in Your Hands! <br/> <br/>
If you are interested in rolling up your sleeves and want to discuss additional ways to get involved, please contact <a href="mailto:shimmy@angelwish.org">Shimmy Mehta</a>.<br/> <br/>
Happy Holidays!</p>
<p>&nbsp;</p>

<img src="/public/images/giving.jpg" class="kid"/>

    </div>
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>
