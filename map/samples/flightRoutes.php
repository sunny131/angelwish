<?php $con = mysql_connect("mysql.gothamangel.org","anandwani","awish2012");
//$con = mysql_connect("mysql.gothamangel.org","cubetech","angelwish2012");
if(!$con){
   die('Could not connect: ' . mysql_error());
}else{
   mysql_select_db("angelwish");
}


?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amMap examples</title>
        
        <link rel="stylesheet" href="../ammap/ammap.css" type="text/css">        
        <script src="../ammap/ammap.js" type="text/javascript"></script>
        <!-- map file should be included after ammap.js -->
		<script src="../ammap/maps/js/worldLow.js" type="text/javascript"></script>        
        
		<script type="text/javascript">
			var map;
			
			// svg path for target icon
			var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
			// svg path for plane icon
			var planeSVG = "M19.671,8.11l-2.777,2.777l-3.837-0.861c0.362-0.505,0.916-1.683,0.464-2.135c-0.518-0.517-1.979,0.278-2.305,0.604l-0.913,0.913L7.614,8.804l-2.021,2.021l2.232,1.061l-0.082,0.082l1.701,1.701l0.688-0.687l3.164,1.504L9.571,18.21H6.413l-1.137,1.138l3.6,0.948l1.83,1.83l0.947,3.598l1.137-1.137V21.43l3.725-3.725l1.504,3.164l-0.687,0.687l1.702,1.701l0.081-0.081l1.062,2.231l2.02-2.02l-0.604-2.689l0.912-0.912c0.326-0.326,1.121-1.789,0.604-2.306c-0.452-0.452-1.63,0.101-2.135,0.464l-0.861-3.838l2.777-2.777c0.947-0.947,3.599-4.862,2.62-5.839C24.533,4.512,20.618,7.163,19.671,8.11z";
			var imsg="../../public/images/icon3.png";
			
			AmCharts.ready(function() {
			    map = new AmCharts.AmMap();
			    map.pathToImages = "../ammap/images/"; 
			    //map.panEventsEnabled = true; // this line enables pinch-zooming and dragging on touch devices
			
			    var dataProvider = {
			        mapVar: AmCharts.maps.worldLow
			    };
			
			    map.areasSettings = {
			        unlistedAreasColor: "#FFCC00"
			    };
			
			    map.imagesSettings = {
			        color: "#CC0000",
			        rollOverColor: "#CC0000",
			        selectedColor: "#000000"
			    };
			
			    map.linesSettings = {
			        color: "#CC0000",
			        alpha: 0.4
			    };
				//Chicago
				var chicago = {
					id: "chicago",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Chicago($131.18)",
			        latitude: 41.8842506,
			        longitude: -87.6324463,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [41.8842506, 12.9669704],
							longitudes: [-87.6324463, 77.5872803]}
						/*,{
							latitudes: [41.8842506, 41.8842506],
							longitudes: [-87.6324463, -87.6324463]} */],
							
			        images: [{
			            label: "Chicago",
			            imageURL: imsg,
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				};
				//Edinburgh
			 	var edinburgh = {
					id: "edinburgh",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Edinburgh($218.63)",
			        latitude: 55.9541512,
			        longitude: -3.20277,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [55.9541512, 51.5064316],
							longitudes: [-3.20277, -0.12719]},
			       
			            {
							latitudes: [55.9541512, 53.4874992],
							longitudes: [-3.20277, -2.2458899]},
			            {
							latitudes: [55.9541512, 40.43],
							longitudes: [-3.20277, -74]}
						],
							
			        images: [{
			            label: "Edinburgh",
			            imageURL: imsg,
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
			    
				//Frankfurt am Main
			 	var frankfurtammain = {
					id: "FrankfurtamMain",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Frankfurt am Main($65.23)",
			        latitude: 50.1120796,
			        longitude: 8.6834097,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [50.1120796, 51.5064316],
							longitudes: [8.6834097, -0.12719]},
			       
			            {
							latitudes: [50.1120796, 53.4874992],
							longitudes: [8.6834097, -2.2458899]},
			            ],
							
			        images: [{
			            label: "Frankfurt am Main",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//Hong Kong
			 	var hongkong = {
					id: "HongKong",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Hong Kong($35.99)",
			        latitude: 22.3361568,
			        longitude: 114.1869659,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [22.3361568, 39.9522781],
							longitudes: [114.1869659, -75.1624527]},
					],
							
			        images: [{
			            label: "Hong Kong",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//London
			 	var london = {
					id: "london",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "London($1754.11)",
			        latitude: 51.5064316,
			        longitude: -0.12719,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [51.5064316, 50.7249985],
							longitudes: [-0.12719, -1.89902]},
			            {
							latitudes: [51.5064316, 40.8548393],
							longitudes: [-0.12719, -73.8694153]},
			            {
							latitudes: [51.5064316, 18.9167004],
							longitudes: [-0.12719, -71.9832993]},
			            {
							latitudes: [51.5064316, 53.4874992],
							longitudes: [-0.12719, -2.2458899]},
			            {
							latitudes: [51.5064316, 40.43],
							longitudes: [-0.12719, -74]},
			            {
							latitudes: [51.5064316, 13.3339996],
							longitudes: [-0.12719, 103.8509979]},
					],
							
			        images: [{
			            label: "London",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//Mexico City
			 	var mexico = {
					id: "MexicoCity",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Mexico City($28.75)",
			        latitude: 19.4319592,
			        longitude: -99.1331635,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [19.4319592, -1.4079],
							longitudes: [-99.1331635, 29.8313007]},
			            
					],
							
			        images: [{
			            label: "Mexico City",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//Moscow
			 	var moscow = {
					id: "moscow",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Moscow($30.75)",
			        latitude: 55.7569695,
			        longitude: 37.6150208,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [55.7569695, 18.9167004],
							longitudes: [37.6150208, -71.9832993]},
			            
					],
							
			        images: [{
			            label: "Moscow",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//New York
			 	var newyork = {
					id: "newyork",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "New York($5930.87)",
			        latitude: 40.7820015,
			        longitude: -73.8327026,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [40.7820015, 42.6515503],
							longitudes: [-73.8327026, -73.7552109]},
			            {
							latitudes: [40.7820015, 39.7093201],
							longitudes: [-73.8327026, -104.8145828]},
			            {
							latitudes: [40.7820015, 12.9669704],
							longitudes: [-73.8327026, 77.5872803]},
			            {
							latitudes: [40.7820015, 40.8548393],
							longitudes: [-73.8327026, -73.8694153]},
						{
							latitudes: [40.7820015, 40.8548393],
							longitudes: [-73.8327026, -73.8694153]},
							{
							latitudes: [40.7820015, 12.9669704],
							longitudes: [-73.8327026, -73.9493866]},
							{
							latitudes: [40.7820015, -1.4079],
							longitudes: [-73.8327026, 29.8313007]},
							{
							latitudes: [40.7820015, 39.9452515],
							longitudes: [-73.8327026, -75.119133]},
							{
							latitudes: [40.7820015, 18.9167004],
							longitudes: [-73.8327026, -71.9832993]},
							{
							latitudes: [40.7820015, 40.8860588],
							longitudes: [-73.8327026, -74.0444565]},
							{
							latitudes: [40.7820015, 39.7669106],
							longitudes: [-73.8327026, -86.1499634]},
							{
							latitudes: [40.7820015, 40.6996994],
							longitudes: [-73.8327026, -73.8080978]},
							{
							latitudes: [40.7820015, 40.3037605],
							longitudes: [-73.8327026, -73.9933701]},
							{
							latitudes: [40.7820015, 40.1977806],
							longitudes: [-73.8327026, -74.0304031]},
							{
							latitudes: [40.7820015, 40.4947701],
							longitudes: [-73.8327026, -74.4438095]},
							{
							latitudes: [40.7820015, 29.9536991],
							longitudes: [-73.8327026, -90.0777512]},
							{
							latitudes: [40.7820015, 40.7319717],
							longitudes: [-73.8327026, -74.1742096]},
							{
							latitudes: [40.7820015, 34.2235298],
							longitudes: [-73.8327026, -92.0034027]},
							{
							latitudes: [40.7820015, 41.0010605],
							longitudes: [-73.8327026, -73.6661911]},
							{
							latitudes: [40.7820015, 45.5117912],
							longitudes: [-73.8327026, -122.6756287]},
							{
							latitudes: [40.7820015, 37.540699],
							longitudes: [-73.8327026, -77.4336472]},
							{
							latitudes: [40.7820015, 32.5146103],
							longitudes: [-73.8327026, -93.7472687]},
							{
							latitudes: [40.7820015, 43.5453491],
							longitudes: [-73.8327026, -96.7312775]},
							{
							latitudes: [40.7820015, 40.5808678],
							longitudes: [-73.8327026, -74.1527481]},
							{
							latitudes: [40.7820015, 38.8903694],
							longitudes: [-73.8327026, -77.0319595]},
							
							
							
			            
					],
							
			        images: [{
			            label: "New York",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//San Francisco (MA)
			 	var sanfranciscoma = {
					id: "San Francisco (MA)",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "San Francisco (MA)($167.84)",
			        latitude: 37.7771187,
			        longitude: -122.4196396,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 37.7771187, 40.7820015],
							longitudes: [-122.4196396, -73.8327026]},
							{
							latitudes: [ 37.7771187 , 37.7771187],
							longitudes: [-122.4196396, -122.4196396]},
					],
							
			        images: [{
			            label: "San Francisco (MA)",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//San Francisco (MIS)
			 	var sanfranciscomis = {
					id: "San Francisco (MIS)",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "San Francisco (MIS)($65.99)",
			        latitude: 37.7771187,
			        longitude: -122.4196396,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 37.7771187, 40.7319717],
							longitudes: [-122.4196396, -74.1742096]},
							{
							latitudes: [ 37.7771187 , 38.5793304],
							longitudes: [-122.4196396, -121.4908981]},
					],
							
			        images: [{
			            label: "San Francisco (MIS)",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
			    //Sao Paulo
			 	var saopaulo = {
					id: "Sao Paulo",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Sao Paulo($27.59)",
			        latitude: -23.5628395,
			        longitude: -46.6546402,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [-23.5628395, 41.2606888],
							longitudes: [-46.6546402, -95.9405899]},
							],
							
			        images: [{
			            label: "Sao Paulo",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//Singapore
			 	var singapore = {
					id: "Singapore",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Singapore($115.00)",
			        latitude: 1.3219959,
			        longitude: 103.8205338,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 1.3219959, 13.3339996],
							longitudes: [103.8205338, 103.8509979]},
							
					],
							
			        images: [{
			            label: "Singapore",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
			    
				//South San Francisco
			 	var southsanfrancisco = {
					id: "southsanfrancisco",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "South San Francisco($52.59)",
			        latitude: 37.6560593,
			        longitude: -122.4141769,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 37.6560593, 40.7319717],
							longitudes: [-122.4141769, -74.1742096]},
			            {
							latitudes: [ 37.6560593, -1.4079],
							longitudes: [-122.4141769, 29.8313007]},
							
					],
							
			        images: [{
			            label: "South San Francisco",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				
				//Sydney
			 	var sydney = {
					id: "sydney",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Sydney($78.75)",
			        latitude: -33.8740005,
			        longitude: 151.2030029,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [  -33.8740005, 12.9669704],
							longitudes: [151.2030029, 77.5872803]},
			             
							
					],
							
			        images: [{
			            label: "Sydney",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				
				//Toronto (CSI)
			 	var torontocsi = {
					id: "torontocsi",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Toronto (CSI)($21.59)",
			        latitude: 43.6483994,
			        longitude: -79.4857025,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 43.6483994, 40.6452217],
							longitudes: [-79.4857025, -73.9493866]},
			             
							
					],
							
			        images: [{
			            label: "Toronto (CSI)",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
				//Westchester
			 	var westchester = {
					id: "westchester",
			        color: "#000000",
			        svgPath: targetSVG,
			        title: "Westchester($184.18)",
			        latitude: 41.1514206,
			        longitude: -73.7533875,
			        scale: 1.5,
			        zoomLevel: 2.74,
			        zoomLongitude: -20.1341,
			        zoomLatitude: 49.1712,
			         lines: [
			            {
							latitudes: [ 41.1514206, 39.9522781],
							longitudes: [-73.7533875, -75.1624527]},
			             
							
					],
							
			        images: [{
			            label: "Westchester",
			           
			            left: 100,
			            top: 45,
			            labelShiftY:5,
			            color: "#CC0000",
			            labelColor: "#CC0000",
			            labelRollOverColor: "#CC0000",
			            labelFontSize: 20}]
				}; 
			    
			
			
			    
			
			    // cities
			    var cities = [
					london,
					
					edinburgh,
					westchester,
					sanfranciscoma,
					chicago,
					singapore,
					sydney,
			        sanfranciscomis,
			        frankfurtammain,
					southsanfrancisco,
			        hongkong,
			        moscow,
			        mexico,
			        newyork,
			        saopaulo,
			        torontocsi,
			      
			         
			    
			     {
			        svgPath: targetSVG,
			        title: "Omaha",
			        latitude: 41.2606888,
			        longitude: -95.9405899},
			      
			        {
			        svgPath: targetSVG,
			        title: "Oakland",
			        latitude: 40.7319717,
			        longitude: -74.1742096},
			        {
			        svgPath: targetSVG,
			        title: "Sacramento",
			        latitude: 38.5793304,
			        longitude: -121.4908981},
			        {
			        svgPath: targetSVG,
			        title: "San Francisco",
			        latitude: 37.7771187,
			        longitude: -122.4196396},
			        {
			        svgPath: targetSVG,
			        title: "Staten Island",
			        latitude: 38.8903694,
			        longitude: -77.0319595},
			        {
			        svgPath: targetSVG,
			        title: "Staten Island",
			        latitude: 40.5808678,
			        longitude: -74.1527481},
			        {
			        svgPath: targetSVG,
			        title: "Sioux Falls",
			        latitude: 43.5453491,
			        longitude: -96.7312775},
			        {
			        svgPath: targetSVG,
			        title: "Shreveport",
			        latitude: 32.5146103,
			        longitude: -93.7472687},
			        {
			        svgPath: targetSVG,
			        title: "Richmond",
			        latitude: 37.540699,
			        longitude: -77.4336472},
			        
			    {
			        svgPath: targetSVG,
			        title: "Portland",
			        latitude: 45.5117912,
			        longitude: -122.6756287},
			        {
			        svgPath: targetSVG,
			        title: "Port Chester",
			        latitude: 41.0010605,
			        longitude: -73.6661911},
			        {
			        svgPath: targetSVG,
			        title: "Pine Bluff",
			        latitude: 34.2235298,
			        longitude: -92.0034027},
			         {
			        svgPath: targetSVG,
			        title: "Newark",
			        latitude: 40.7319717,
			        longitude: -74.1742096},
			        {
			        svgPath: targetSVG,
			        title: "New Orleans",
			        latitude: 29.9536991,
			        longitude: -90.0777512},
			        {
			        svgPath: targetSVG,
			        title: "Jamaica",
			        latitude: 40.6996994,
			        longitude: -73.8080978},
			        {
			        svgPath: targetSVG,
			        title: "New Brunswick",
			        latitude: 40.4947701,
			        longitude: -74.4438095},
			        {
			        svgPath: targetSVG,
			        title: "Neptune",
			        latitude: 40.1977806,
			        longitude: -74.0304031},
			        {
			        svgPath: targetSVG,
			        title: "Aurora",
			        latitude: 39.7093201,
			        longitude: -104.8145828},
			        {
			        svgPath: targetSVG,
			        title: "Long Branch",
			        latitude: 40.3037605,
			        longitude: -73.9933701},
						 
				{
			        svgPath: targetSVG,
			        title: "Hackensack",
			        latitude: 40.8860588,
			        longitude: -74.0444565},
			    {
			        svgPath: targetSVG,
			        title: "Camden",
			        latitude: 39.9452515,
			        longitude: -75.119133},
			    {
			        svgPath: targetSVG,
			        title: "Albany",
			        latitude: 42.6515503,
			        longitude: -73.7552109},
			    {
			        svgPath: targetSVG,
			        title: "Butaro",
			        latitude: -1.4079,
			        longitude: 29.8313007},
			    {
			        svgPath: targetSVG,
			        title: "Siem Reap",
			        latitude: 13.3339996,
			        longitude: 103.8509979},
			    {
			        svgPath: targetSVG,
			        title: "Cange",
			        latitude: 18.9167004,
			        longitude: -71.9832993 },
			    {
			        svgPath: targetSVG,
			        title: "Bronx",
			        latitude: 40.8548393,
			        longitude: -73.8694153 },
			    {
			        svgPath: targetSVG,
			        title: "Bournemouth",
			        latitude: 50.7249985,
			        longitude: -1.89902},
			         
			    {
			        svgPath: targetSVG,
			        title: "Philadelphia",
			        latitude: 39.9522781,
			        longitude: -75.1624527 },
			    {
			        svgPath: targetSVG,
			        title: "London",
			        latitude: 51.5002,
			        longitude: -0.1262 },
			    {
			        svgPath: targetSVG,
			        title: "Manchester",
			        latitude: 53.4874992,
			        longitude: -2.2458899 },
			   
			    {
			        svgPath: targetSVG,
			        title: "Bangalore",
			        latitude: 12.9669704,
			        longitude: 77.5872803},
			        
			    {
			        svgPath: targetSVG,
			        title: "Brooklyn",
			        latitude: 40.6452217,
			        longitude:  -73.9493866 },
			        
			        {
			        svgPath: targetSVG,
			        title: "Indianapolis",
			        latitude: 39.7669106,
			        longitude: -86.1499634},
			     
			    {
			        svgPath: targetSVG,
			        title: "New York",
			        latitude: 40.43,
			        longitude: -74}];
			
			
			    dataProvider.linkToObject = chicago;
			    dataProvider.images = cities;
			    map.dataProvider = dataProvider;
				map.backgroundZoomsToTop = true;
				map.linesAboveImages = true;
			    map.write("mapdiv");
			
			});
            
        </script>
    </head>
    <body>
        <div id="mapdiv" style="width: 100%; background-color:#eeeeee; height: 500px;"></div>
    </body>

</html>isme 
