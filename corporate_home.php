<link rel="stylesheet" href="/public/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<style>
    <?=$css?>
</style>

<div id="home">
    <h1>
        <?if(!empty(CompanyRouter::$company['tagline'])) {
            
            echo CompanyRouter::$company['tagline'];
            
        } else {?>

            Welcome to the 2011 <?=CompanyRouter::$company['name']?> Holiday Giving Drive!

        <?}?>
    </h1>
    <div class="motto">
        <h2>
            <i>
                <big>100%</big> of your individual donation goes directly towards
                program services
            </i>
        </h2>
    </div>

    <div class="sharing">
        <div>
            <div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="angelwish.org" send="true" width="230" show_faces="false" font="arial"></fb:like>
        </div>
        <div>
            <a href="http://twitter.com/angelwish" class="twitter-follow-button">Follow</a>
            <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
        </div>
    </div>

    <div id="welcome-message">
        <p>
            <?if(!empty(CompanyRouter::$company['welcome_text'])) {

                echo CompanyRouter::$company['welcome_text'];
            
            } else {?>

                Welcome to the 2011 <?=CompanyRouter::$company['name']?> Holiday Giving Drive!

            <?}?>
        </p>
    </div>

    <div id="whatwedo-container">
        <div class="box first">
            <div class="image">
                <img class="icon" src="/public/images/wings_ico.png" align="absmiddle" style="width:30px"/>
            </div>
            <div class="title">
                <h3>What we do</h3>
            </div>
            <a id="video-link" href="#video">
                <img class="video" src="/public/images/intro_video.jpg"/>
            </a>
            <div style="display:none;">
                <div id="video" class="overlay">
                    <iframe height="370" frameborder="0" width="620" src="about:blank"></iframe>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="image">
                <img class="icon" src="/public/images/icon2.png" align="absmiddle"/>
            </div>
            <div class="title">
                <h3>Where we work</h3>
            </div>
            <p>Angelwish works with over 107 Hospitals and Care Centers in the United States and an additional 43 Care Centers in 37 countries on 6 continents.</p>
            <a href="/about#wherewework" class="read-more"> Read More </a>
        </div>

        <div class="box last">
            <div class="image">
                <img class="icon" src="/public/images/icon3.png" align="absmiddle"/>
            </div>
            <div class="title">
                <h3>How can you help?</h3>
            </div>
            <p> We have partnered with the most respected organizations dealing with chronic illnesses all over the world to help you give back.
          </p>
            <a href="/grant_a_wish" class="read-more"> Grant a Wish </a>
        </div>
    </div>

    <div id="bottomrow-container">
        <div class="bottomrow-item big">
            <img class="pic" src="/public/images/kid_small.png"/>
            <div class="testimonial">
                <img class="open-quotes" alt="" src="/public/images/open_quotes.jpg">
                <p>
                    For many of our patients this is the only Christmas they get. This is either due to their parents being deceased or that there is no financial way to afford a holiday.
                    These presents are anticipated by our patients yearly.
                </p>
                <img class="close-quotes" alt="" src="/public/images/close_quotes.jpg">
            </div>
            <a href="/stories"> Read More Testimonials </a>
        </div>


        <div class="bottomrow-item small">
            <a href="http://www.flickr.com/photos/angelwish" target="_blank" title="Angelwish Photostream">
                <img src="/public/images/collage.png" alt="Angelwish Photostream"/>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript" src="/public/js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
    <?=$js?>
</script>