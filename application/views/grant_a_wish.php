<style>
    <?=$css?>
    tr.tabletr td.first_tr{
    width: 200px;
    float: left;
    text-align: left;
    padding-top:7px;
}
</style>
<!--added by cubet technologies -->
<div id="donation_container" class="page">
    <h1 class="grant">Grant A Wish</h1>
    <div class="donate">
	    <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Make a Quick Donation and We'll Help Those who Need it Most</b></div>
	<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr class="tabletr">
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}

		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
				
				$("#d_form,#black_layer").show();
				return false;
				/*document.getElementById('donation_form').submit();*/
			}
	$(document).ready(function(){
	    $("#black_layer,#close_icon").click(function(){
	    $("#d_form,#black_layer").hide();
	    			$('input[name="fname"]').attr('style','border:1px solid grey');
				$('input[name="lname"]').attr('style','border:1px solid grey');
				$('input[name="email"]').attr('style','border:1px solid grey');
	    });
	    
	    $('input[name="recurring"]').click(function(){
		if($('input[name="recurring"]:checked').length>0){
		$(this).val(1);
		$('.subscription').html('<select name="subscribe_period"><option value="1 month" selected="selected">Monthly</option><option value="3 month">Quarterly</option><option value="12 month" >Annual</option></select');
		$('.hidden_elm').html('<input type="hidden" name="subcribe_period" id="subcribe_period" value="1 month" />');
		}else {$(this).val(0);
		$('.subscription').html('');
		}
	    });
	    $( 'select[name="subscribe_period"]').live('change',function() {
		var period=$(this).val();
		$("#subcribe_period").val(period);
	    });
	});
	function validate_name(){
	var error=0;
	$('input[name="fname"]').attr('style','border:1px solid grey');
	$('input[name="lname"]').attr('style','border:1px solid grey');
	$('input[name="email"]').attr('style','border:1px solid grey');
	if($('input[name="fname"]').val() == '') {
		$('input[name="fname"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if($('input[name="lname"]').val() == '') {
		$('input[name="lname"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if($('input[name="email"]').val() == '') {
		$('input[name="email"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if (error > 0) {
	return false;
	}else{
	return true;
	}
	
}
	</script>

	<td class="first_tr">
	        <span class="subscription"></span>
		<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
			  <option value="25" selected="selected">$25</option>
			  <option value="50">$50</option>
			  <option value="100">$100</option>
			  <option value="500">$500</option>
              <option value="1000">$1000</option>
			  <option value="-1">Other</option>
			</select>
	    <div>Make this recurring <input type="checkbox" name="recurring" id="recurring" title="Check to schedule payment after interval" value="0" /></div>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <!--<form id="donation_form" action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
        <input type="hidden" name="immediateReturn" value="1">
        <input type="hidden" name="collectShippingAddress" value="0" >
        <input type="hidden" name="signatureVersion" value="2" >
        <input type="hidden" name="signatureMethod" value="HmacSHA256" >
        <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
        <input type="hidden" name="referenceId" value="Donation to Angelwish" >
        <input type="hidden" name="signature" value="uyu4do+AJHNfnnQy7Eej2Z6pP1RTcEpjN1j5LvgBccc=" >
        <input type="hidden" name="isDonationWidget" value="1" >
        <input type="hidden" name="description" value="Donation to Angelwish" >
        <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
        <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thank-you/" >
        <input type="hidden" name="processImmediate" value="1" >
        <input type="hidden" name="cobrandingStyle" value="logo" >
        <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
        <input type="hidden" id="amount" name="amount" value="" >

	  <td><input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0"></td>
	</form>-->
	<td>
	<form method="post" id="donation_form"> 
			<style>
			.form_style{background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #000000;
    border-radius: 10px 10px 10px 10px;
    display: none;
    height: 410px;
    left: 38%;
    position: fixed;
    top: 14%;
    width: 345px;
    z-index: 99999999;}
    .logo_from{ float:left; width:100%;  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAYAAABHLFpgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHdJREFUeNqEkcsNgDAMQ632yiYswx5MywbcOwBCQjL9oBBKAj1UivWs2inGaWZAPoHlAok2Kg1Fo6NR4KpRcY7tQihPvsfmrTZ+2TRnOKTRHVJzRN/8x9GXppHP4fxa1g6eiPU9au2WpoLzCNi3JTKtKWZ9OAUYAGipcMVOJd4FAAAAAElFTkSuQmCC") repeat scroll 0 0 transparent; }
    .logo_from img{ float:left; text-align:center; padding-left:55px;}
			
			</style>
					
		<div id="d_form" class="form_style">
		<div style="float:left; width:100%; position: relative;">
			<div style="position: absolute; right: 0; top: 0;cursor: pointer; width: auto;"><img id="close_icon" src="http://angelwish.org/public/images/close_icon.png" alt="" /></div>
			<div class="logo_from"><img src="http://angelwish.org/public/images/xaw_logo.png.pagespeed.ic.WiUTy-3UkG.png" alt="anglewish"></div>
			<table style="  padding-left: 19px; padding-top: 11px;">
				<tbody><tr>
					<td>First Name</td>
					<td><input type="text" placeholder="First Name" name="fname"></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" placeholder="Last Name" name="lname"></td>
				</tr>
				<tr>
					<td>Email Address </td>
					<td><input type="text" placeholder="Email Address" name="email"></td>
				</tr>
				<tr>
					<td>Address </td>
					<td><textarea name="address"></textarea></td>
				</tr>
				<tr>
					<td>City </td>
					<td><input type="text" placeholder="City" name="city"></td>
				</tr>
				<tr>
					<td>State </td>
					<td><input type="text" placeholder="State" name="state"></td>
				</tr>
				<tr>					<td>Zip Code </td>
					<td><input type="text" placeholder="Zip Code" name="zipcode"></td> 
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				 <tr>
					<td colspan="2" align="center"><input type="submit" style="background: url('http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif') no-repeat scroll 0% 0% transparent; border: medium none; width: 134px; height: 37px; text-indent: -999px;" value="Donate" name="submit" onclick="return validate_name();"></td>
				</tr>
			</tbody></table>
		</div></div>

<div class="hidden_elm"></div>
			<input type="hidden" id='amount' name="amount" size="8"  value="">
			<input type="image"  onclick="return validate();"  src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
		</form>
		</td>
	</tr>
	</table>
	</div>
    
    <div id="steps">
        <div class="step-2">
            <h4>Donate to a care center of your choice </h4>
            <ul class="tabs">
                <li><a href="#US">USA</a></li>
                <li><a href="#UK">UK</a></li>
                <li><a href="#International">International</a></li>
            </ul>
            <div id="map_canvas"></div>
            <div class="panes">
                <div class="pane">
                    <div id="listofstates">
                        <?for($row=0;$row<4;$row++){?>
                        <div class="states_div">
                           <!-- these are the states list -->
                           <ul class="states">
                            <?for($index=ceil((count($states)*$row)/4);$index<(count($states)*($row+1)/4);$index++){
                                $latitude = $us_carecenters[$states[$index]][0]['latitude'];
                                $longitude = $us_carecenters[$states[$index]][0]['longitude'];
                                ?>
                                <li>
                                    <a href="javascript:showstate('<?=str_replace(" ", "_", $states[$index])?>',<?=$latitude?>,<?=$longitude?>);"><?=$states[$index]?></a>
                                </li>
                            <?}?>
                            </ul>
                        </div>
                        <?}?>
                    </div>
<!--end states list -->

<!--care centers  list -->
                    <?foreach($states as $state){?>

                    <div id="<?=str_replace(" ", "_", $state)?>" style="display:none;" class="state">

                        <h3> Care Centers in  <?=$state?> </h3>
                        <ul>
                        <?foreach($us_carecenters[$state] as $carecenter){?>
                            <li>
                                <div class="name"><a id="carecenterid" href="/carecenter/index/<?=$carecenter['id']?>" onclick="insertGoogleAnalytics('Button', 'CarecenterDetails', 'go to carecenter',<?=$carecenter['id']?>, 1);return true;"><?=$carecenter['name']?></a></div>
                                <div class="address"><?=$carecenter['city']?>, <?=$state?></div>
                            </li>
                        <?}?>
                        </ul>
                        <div id="back" >
                            <a href="javascript:showlistofstates('<?=str_replace(" ", "_", $state)?>');">Back list of states</a>
                        </div>
                    </div>
                    <?}?>
<!--end centers  list -->
                </div>
<!--added by jobanjohn cubet technologies -->
<!--start uk states list  -->
                <div class="pane">
                    <div id="listofukstates">
                        <?for($row=0;$row<4;$row++){?>
                        <div class="states_div">
                           <!-- these are the states list -->
                           <ul class="states">
                            <?for($index=ceil((count($uk_states)*$row)/4);$index<(count($uk_states)*($row+1)/4);$index++){
                                $latitude = $uk_carecenters[$uk_states[$index]][0]['latitude'];
                                $longitude = $uk_carecenters[$uk_states[$index]][0]['longitude'];
                                ?>
                                <li>
                                    <a href="javascript:showukstate('<?=str_replace(" ", "_", $uk_states[$index])?>',<?=$latitude?>,<?=$longitude?>);"><?=$uk_states[$index]?></a>
                                </li>
                            <?}?>
                            </ul>
                        </div>
                        <?}?>
                    </div>
<!--end states list -->

<!--care centers  list -->
                    <?foreach($uk_states as $state){?>

                    <div id="<?=str_replace(" ", "_", $state)?>" style="display:none;" class="state">

                        <h3> Care Centers in  <?=$state?> </h3>
                        <ul>
                        <?foreach($uk_carecenters[$state] as $carecenter){?>
                            <li>
                                <div class="name"><a href="/carecenter/index/<?=$carecenter['id']?>"><?=$carecenter['name']?></a></div>
                                <div class="address"><?=$carecenter['city']?>, <?=$state?></div>
                            </li>
                        <?}?>
                        </ul>
                        <div id="back" >
                            <a href="javascript:showlistofukstates('<?=str_replace(" ", "_", $state)?>');">Back list of states</a>
                        </div>
                    </div>
                    <?}?>
<!--end centers  list -->
                </div>
<!--end by jobanjohn cubet technologies -->
                <div class="pane">
                    <div id="listofcountries">
                        <?for($row=0;$row<4;$row++){?>
                        <div class="states_div">
                           <!-- these are the states list -->
                           <ul class="states">
                            <?for($index=ceil((count($countries)*$row)/4);$index<(count($countries)*($row+1)/4);$index++){
                                $latitude = $intl_carecenters[$countries[$index]][0]['latitude'];
                                $longitude = $intl_carecenters[$countries[$index]][0]['longitude'];
                                ?>
                                <li>
                                    <a href="javascript:showcountry('<?=str_replace(" ", "_", $countries[$index])?>',<?=$latitude?>,<?=$longitude?>);"><?=$countries[$index]?></a>
                                </li>
                            <?}?>
                            </ul>
                        </div>
                        <?}?>
                    </div>
<!--end states list -->

<!--care centers  list -->
                    <?foreach($countries as $country){?>

                    <div id="<?=str_replace(" ", "_", $country)?>" style="display:none;" class="state">
                        <h3> Care Centers in  <?=$country?> </h3>
                        <ul>
                        <?foreach($intl_carecenters[$country] as $carecenter){?>
                            <li>
                                <div class="name">
                                    <a href="/carecenter/index/<?=$carecenter['id']?>"><?=$carecenter['name']?></a>
                                </div>
                                <div class="address"><?=$carecenter['city']?>, <?=$country?></div>
                            </li>
                        <?}?>
                        </ul>
                        <div id="back" >
                            <a href="javascript:showlistofcountries('<?=str_replace(" ", "_", $country)?>');">Back to list of countries</a>
                        </div>
                    </div>
                    <?}?>
<!--end centers  list -->
                </div>
            </div>
        </div>


    </div>
    <div id="side_img">

    </div>
</div>



<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript"
    src="/public/js/markerclusterer_packed.js">
</script>
<script type="text/javascript">
    <?=$js?>
</script>
<script type="text/javascript">
$(function(){
	$('#grant a.highlight').addClass('active');
});
</script>
