<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>Angelwish.org - Helping children with chronic illnesses</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <style type="text/css">
        <?=$templatecss?>
    </style>
    <link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    
    <style type="text/css" media="only screen and (max-device-width: 1024px)">
	</style>
    <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>-->
    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript" src="/public/js/jquery.tools.min.js"></script>
</head>

<body>

<div id="black_layer" style="position: FIXED; width: 100%; opacity: 0.6; background: none repeat scroll 0px 0px rgb(0, 0, 0); z-index: 99999; height: 100%; display:none"></div>

<div id="new_bg">
<div id="container" style="margin:0 auto;">
    <div id="header">
        <div class="width960"><?=$header?></div>
    </div>
    <div id="middle">
        <div id="contents" class="width960"><?= $contents ?></div>
    </div>
    <div id="footer">
        <div id="footer-inner" class="width960"><?=$footer?></div>
    </div>
</div>
    <div class="footer_policy"> <a href="/privacy">Privacy Policy</a> | <a href="/terms">Terms of Use</a> |  &#169; 2014 Angelwish</div>
    <script type="text/javascript">
        <?=$templatejs?>
    </script>
</div>
</body>
</html>
