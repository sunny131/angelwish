<link rel="stylesheet" href="/public/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<style>
    <?=$css?>
    .plus_sign {
    font-size: 23px;
    font-weight: bold;
    cursor: pointer;
}
</style>
<?php 
$this->load->library('session');
$wishss=$this->session->userdata('cart_wishes');

?>
<div id="care_center_container" class="page">
    <div class="care_center_content">
        <h1> <?=$details['name']?> </h1>
        <h3> <?=$details['city']?>, <?php echo $details['state']?$details['state']:$details['country']; ?> </h3>
        <p> <?=$details['desc']?></p>
<?php if($care_optss){ ?>
	    <div class="adopt_carecenter" style="border:1px solid;padding: 15px; width: 536px;margin: 0px;"><?php foreach($care_optss as $opts){  ?>
                    <p class="plus" style="margin-bottom:10px;">
		    <input id="price" type="hidden" value="<?=$opts['amount'];?>">
		    <span class="plus_sign" id="<?=$opts['auto_id'];?>">+</span> <?=$opts['option_name'];?> &nbsp; <strong><?=$opts['amount'];?> </strong>&nbsp;
<a class="active" rel="<?=$opts['auto_id'];?>" href="javascript:void(0);" <?php  if(isset($wishss) && $wishss!=''){ if(array_key_exists($opts['auto_id'], $wishss)){echo 'style="display:none"';}}?>><b>AddToCart</b></a>
                  </p>
		<p class="description_opt_<?=$opts['auto_id'];?>" style="margin-bottom:5px;padding: 6px;display: none"><?=$opts['option_desc'];?><br /></p>
		<?php } ?>
	    </div>
<?php } ?>
    </div>

	    
    <div class="care_center_content_image" style="overflow:hidden">
        <div class="sharing">
            <div class="">
                <div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://angelwish.org" send="true" width="230" show_faces="false" font="arial"></fb:like>
            </div>
            <div class=" twitter">
                <a href="http://twitter.com/angelwish" class="twitter-follow-button">Follow</a>
                <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
            </div>
        </div>
         <div class="actionBox">
            <?if($details['domestic']){?>
            <div class="wish">
                <a href="/public/images/wishImages/<?=$details['wishimage']?>" id="wishimage">
                   <img class="wishimagesmall" src="/public/images/wishImages/<?=$details['wishimage']?>" width="200"/>
                </a>
                <div style="text-align: center;">
                    <a id="wishimagelink" href="javascript:void(0);">
                    Click to enlarge
                    </a>
                </div>
            </div>
            <?}?>
            <div class="actionLinks" <?if(!$details['domestic']){?>style="margin-left: -10px;"<?}?>>
            </div>
        </div>

    </div>
    <div class="clear"></div>   
    <?=modules::run('Wishes',$details['id'])?>
</div>

<script type="text/javascript" src="/public/js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("a#wishimage").fancybox();
    $("#wishimagelink").click(function(){
       $("a#wishimage").trigger('click');
    });
});
</script>
<script>$('.plus_sign').live('click',function(){
var id=$(this).attr('id');
$('p.description_opt_'+id).toggle();
});

/*$('p.plus a.active').each(function(index,anchor){
        $(anchor).bind('click',addToCart);
        
});
    function addToCart(){
        var wish = $(this).attr('rel');
	$(this).closest('a').hide();
	$(this).unbind('click');
        $.post("/cart/addoptions",{
            "wish":wish,
        }).done(function(data) { 
	insertGoogleAnalytics('Button', 'Ad to Cart - WishId', 'Ad to Cart - WishId',wish, 2); 
        showWishInCart(data,wish);
        $('.rightArrow').trigger( "click" );
	});
    }*/
var i=0;
$('p.plus a.active').live('click',function(){
            if ($('.allWishes > div.wish').length==0) { 
var i=1;
     $('div.allWishes').removeAttr('style'); 
}
        var wish = $(this).attr('rel');
	$(this).closest('a').hide();
	$(this).unbind('click');
        $.post("/cart/addoptions",{
            "wish":wish,
        }).done(function(data) { 
	insertGoogleAnalytics('Button', 'Ad to Cart - WishId', 'Ad to Cart - WishId',wish, 2); 
        showWishInCart(data,wish);
        if (i==1) {
           $('.current').html('1');
        }
        else {
             $('.rightArrow').trigger( "click" );
        }
      
    

	});
});
    function showWishInCart(data,wish){
        if($('#cart .wish').size()==0){
            $('#emptyMessage').hide();
            $('#cart .wishes').show();
            $('#cart .allWishes').css('left','0px');
	  $('#cart .allWishes').append(data);
        }else {
	   $('#cart .allWishes').append(data);  
	}
	reCalculateTotal();
    }
	    
    function reCalculateTotal(){
        var newTotal = 0;
        $('#cart .wish').each(function(index,element){
            newTotal = newTotal+1;
        });
    $('#cart .pages .total').html(newTotal);
     var priceTotals = 0;
            $('#cart .wish').each(function(){
            priceTotal=$(this).find('.value').html();
	    priceTotals=parseFloat(priceTotal)+parseFloat(priceTotals);
        });
	    var Fifpercent=priceTotals*15/100;
	    var actual_price=parseFloat(priceTotals)+parseFloat(Fifpercent);
            actual_price=Math.round(actual_price,2);
	  $('#cartTotal').html(actual_price);  
    }
    $('.removeFromCart .active2').live('click',function(){
        var wish = $(this).attr('rel');
        $.post("/cart/remove",{
            "wish":wish
        });
        $(this).parents('.wish').remove();
        $(this).unbind('click');
        $('.plus a[rel="'+wish+'"]').show();
        $('.current').html($('.current').html()-1);
        $('#cart .allWishes').css('left','0px');
     reCalculateTotal();
		
       });
</script>