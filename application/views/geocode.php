<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="user-scalable=no" />
<style type="text/css">

</style>
</head>
<body>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Country</th>
            <th>Formatted Address</th>
            <th>Lat</th>
            <th>Long</th>
            <th>Update</th>
        </tr>

        <?foreach($carecenters as $carecenter){?>
        <tr id="cc_<?=$carecenter['id']?>">
            <td class="id"><?=$carecenter['id']?></td>
            <td class="address"><?=$carecenter['address']?></td>
            <td class="city"><?=$carecenter['city']?></td>
            <td class="state"><?=$carecenter['state']?></td>
            <td class="country"><?=$carecenter['country']?></td>
            <td class="formatted"><?
            $carecenter['address'] = $carecenter['address'] == NULL ? '': $carecenter['address'];
            $formatted_address = $carecenter['address'];
            $formatted_address .= $carecenter['city'] == NULL ? '' : ", ".$carecenter['city'];
            $formatted_address .= $carecenter['state'] == NULL ? '' : ", ".$carecenter['state'];
            $formatted_address .= $carecenter['country'] == NULL ? '' : ", ".$carecenter['country'];

            echo ($formatted_address);
            ?></td>
            <td class="latitude"></td>
            <td class="longitude"></td>
            <td><input type="button" value="Update" onclick="update(<?=$carecenter['id']?>);"/></td>
        </tr>
        <?}?>
    </table>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"
        type="text/javascript"></script>
    <script type="text/javascript"
        src="http://maps.google.com/maps/api/js?sensor=false">
    </script>
    <script type="text/javascript">
        $('tr').each(function(i,element){
            if(i==0||i>5)
                return;
            
            var address = $(element).children('.formatted').html();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address':address},function(results,status){
                 if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();
                    $($(element)).children('.latitude').html(lat);
                    $($(element)).children('.longitude').html(lng);
                  }else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
            });
        });
        function update(id){
            var lat = $('#cc_'+id+' .latitude').html();
            var lng = $('#cc_'+id+' .longitude').html();
            $.post('/googlemaps/updatelatlong',{'id':id,'lat':lat,'lng':lng});
        }
    </script>
</body>
</html>
