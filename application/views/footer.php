<style>
    <?=$css?>
</style>


<div class="footer_box">

    <div class="moreinfo">
		<div class="sharing">
			<div>
				<div id="fb-root"></div>
				<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://www.angelwish.org" send="true" width="230" show_faces="false" font="arial"></fb:like>
			</div>
			<div>
				<a href="http://twitter.com/angelwish" class="twitter-follow-button">Follow</a>
				<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
			</div>
		</div>
    </div>
</div>

<div class="footer_box">
    <ul>
        <li><a href="/wishes"> Grant Wishes </a></li>
        <li><a href="/living_with"> Children With Chronic Illnesses </a></li>
        <li><a href="/stories"> Stories </a></li>
        <li><a href="/media"> Media </a></li>
        <li><a href="/newsletter"> Newsletter </a></li>

    </ul>
</div>

<div class="footer_box">
    <h4><a href="/index.php/about">About Us</a></h4>
    <ul>
        <li> <a href="/about#mission">Mission Statement </a> </li>
        <li> <a href="/about#whatwedo">What We Do </a> </li>
        <li> <a href="/about#wherewework">Where We Work</a> </li>
        <li> <a href="/about#board">Board Members</a> </li>
        <li> <a href="/about#annualreports">Annual Reports</a> </li>
    </ul>
</div>

<div class="footer_box" style="text-align:center">
    <h4><a href="/contact">Contact Us</a></h4>
    <a href="http://www.facebook.com/angelwish" target="_blank" title="Follow us on Facebook">
        <img src="/public/images/facebook-24x24.png" alt="Follow us on Facebook"/>
    </a>
    <a href="http://twitter.com/angelwish" target="_blank" title="Follow us on Twitter">
        <img src="/public/images/Twitter-icon-24.png" alt="Follow us on Twitter" />
    </a>
    <a href="http://vimeo.com/angelwish/videos/sort:likes" target="_blank" title="Follow us on Vimeo">
        <img src="/public/images/vimeo-24x24.png" alt="Follow us on Vimeo" />
    </a>
   <!-- <div class="moreinfo">
        <i><a href="/contact">more information <small>>></small></a></i>
    </div>-->
	<div style="text-align:center"><a target="_blank" href="http://www.guidestar.org/organizations/22-3658778/angelwish.aspx">
    <img src="http://widgets.guidestar.org/gximage2?o=7089092&amp;l=v4">
</a></div>
</div>

