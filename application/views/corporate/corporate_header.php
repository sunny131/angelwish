<style>
    <?=$css?>
</style>
<?
/*Check for png, gif and then jpg*/
$logo = "";
foreach (array('png','gif','jpg','jpeg') as $ext) {
    $logo = APPPATH."../public/images/companylogos/".CompanyRouter::$company['url'].".".$ext;
    if(file_exists($logo)){
        //Actual image path should be like - /public/images/....
        $logo = "/public/images/companylogos/".CompanyRouter::$company['url'].".".$ext;
        break;
    }
}
?>
<div class="logo">
    <a href="/">
        <img src="<?=$logo?>"
             alt="<?=  CompanyRouter::$company['name']?>"/>
        <span id="powered-by">
            powered by
            <img src="/public/images/logo.png" alt="Angelwish" height="30" style="position: relative; top: 15px;"/>
        </span>
    </a>
</div>

<div class="navigation">
    <ul>
        <li id="grant_a_wish" class="first"><a class="highlight" href="/wishes"> Grant Wishes</a></li>
        <li id="living_with"><a href="/living_with" > Children With Chronic Illnesses </a></li>
        <li id="stories"><a href="/stories"> Stories </a></li>
        <li id="media"><a href="/media"> Media </a></li>
        <li id="newsletter"><a href="/newsletter"> Newsletter </a></li>
        <li id="about"><a href="/about"> About Us </a></li>
    </ul>
</div>



<div style="clear:both;margin-bottom: 5px;"></div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20484965-2']);
  _gaq.push(['_setDomainName', '<? echo CompanyRouter::$company['url']?>']);
  _gaq.push(['_trackPageview',"<? echo $pagename;?>"]);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
