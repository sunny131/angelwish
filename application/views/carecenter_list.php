<link rel="stylesheet" href="/public/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<style>
    <?=$css?>
    .plus_sign {
    font-size: 23px;
    font-weight: bold;
    cursor: pointer;
}

.listing_tr th {
    width: 20%;
}
.listing_tr th.main {
    width: 75%;
}
.listing_tr th {
    width: 75%;
}

.listing_tr th.first {
    width: 5%;
}
.listing_tr td.first {
    width: 5%;
}
.listing_tr td.main {
    width: 75%;
}
table {
    width: 100%;
}
</style>
<?php 
$this->load->library('session');
$wishss=$this->session->userdata('cart_wishes');
/*$this->load->library('pagination');
$config['base_url'] = 'http://angelwish.org/admin/lists/';
$config['total_rows'] = 200;
$config['per_page'] = 20;

$this->pagination->initialize($config);

echo $this->pagination->create_links();*/
 $page_url=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
if($page_url=='angelwish.org/admin/lists/'){
    $cid_url='angelwish.org/admin/lists/cid/asc';
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/asc';
}
elseif($page_url=='angelwish.org/admin/lists/cid'){
    $cid_url='angelwish.org/admin/lists/cid/asc';
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/';
}
elseif($page_url=='angelwish.org/admin/lists/cid/asc'){
    $cid_url='angelwish.org/admin/lists/cid/desc';
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/';
}

elseif($page_url=='angelwish.org/admin/lists/carecenter_Name/'){
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/asc';
    $cid_url='angelwish.org/admin/lists/cid';
}
elseif($page_url=='angelwish.org/admin/lists/carecenter_Name/asc'){
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/desc';
    $cid_url='angelwish.org/admin/lists/cid';
}else{
    $cid_url='angelwish.org/admin/lists/cid/asc';
    $ccc_url='angelwish.org/admin/lists/carecenter_Name/asc';
}

?>

<div id="care_center_container" class="page">
<H3>CareCenter List</H3>
<table>
    <tbody>
	<tr class="listing_tr">
	<th class="first"><a href="http://<?php echo $cid_url;?>" class="click_cid"><strong>CID <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>
	<th Class="main"><a href="http://<?php echo $ccc_url;?>" class="click_care"><strong>Care Center Name <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a> | City | State | Country </th>
	<th></th>
	</tr>
	<?php //print_r($care_centers);
	foreach($care_centers as $care_c){?>
	<tr class="listing_tr">
	    <td class="first"><?=$care_c['CID'];?></td>
	    <td class="main"><a href="http://angelwish.org/carecenter/index/<?=$care_c['CID'];?>"><?=$care_c['name'];?></a> <b>|</b> <?=$care_c['city'];?> <b>|</b> <?=$care_c['state'];?> <b>|</b> <?=$care_c['country'];?></td>
	    <td>..</td>
	</tr>
	<?php } ?>
    </tbody>
</table>
</div>

<script type="text/javascript" src="/public/js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("a#wishimage").fancybox();
    $("#wishimagelink").click(function(){
       $("a#wishimage").trigger('click');
    });
});
</script>
<script>$('.plus_sign').live('click',function(){
var id=$(this).attr('id');
$('p.description_opt_'+id).toggle();
});

$('p.plus a.active').each(function(index,anchor){
        $(anchor).bind('click',addToCart);
});
    function addToCart(){
        var wish = $(this).attr('rel');
	$(this).parents('p').hide();
	$(this).unbind('click');
        $.post("/cart/addoptions",{
            "wish":wish,
        }).done(function(data) {
	insertGoogleAnalytics('Button', 'Ad to Cart - WishId', 'Ad to Cart - WishId',wish, 2); 
        showWishInCart(data,wish);
	});
    }
    function showWishInCart(data,wish){
        if($('#cart .wish').size()==0){
            $('#emptyMessage').hide();
            $('#cart .wishes').show();
            $('#cart .allWishes').css('left','0px');
	  $('#cart .allWishes').append(data);
        }else {
	   $('#cart .allWishes').append(data);  
	}
	reCalculateTotal();
    }
	    
    function reCalculateTotal(){
        var newTotal = 0;
        $('#cart .wish').each(function(index,element){
            newTotal = newTotal+1;
        });
    $('#cart .pages .total').html(newTotal);
     var priceTotals = 0;
            $('#cart .wish').each(function(){
            priceTotal=$(this).find('.value').html();
	    priceTotals=parseFloat(priceTotal)+parseFloat(priceTotals);
        });
	    var Fifpercent=priceTotals*15/100;
	    var actual_price=parseFloat(priceTotals)+parseFloat(Fifpercent);
            actual_price=Math.round(actual_price,2)
	  $('#cartTotal').html(actual_price);  
    }
    $('.removeFromCart .active').live('click',function(){
        var wish = $(this).attr('rel');
        $.post("/cart/remove",{
            "wish":wish
        });
        $(this).parents('.wish').remove();
        $(this).unbind('click');
        $('#cart .allWishes').css('left','0px');
     reCalculateTotal();
		
       });
</script>
