<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text">
    <h1>Angelwish's Learning to Give Program</h1>
    <h2>Engaging Corporate Volunteers to Teach Philanthropy </h2>
	<p style="float: left; width: 500px; padding: 0px 20px 0px 0px;">
	  <img src="../../public/images/donor-map.png" width="480" height="243" alt="Impact Map" /> </p>
    <div class="donate" style="float:left">
      <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Help Angelwish Support More Schools!</b></div>
	<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}

		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
			$("#d_form,#black_layer").show();
				return false;
			/*document.getElementById('donation_form').submit();*/
		}
		$(document).ready(function(){
				$("#black_layer,#close_icon").click(function(){
					$("#d_form,#black_layer").hide();
				});
			});
	</script>

	<td>	<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
			  <option value="25">$25</option>
              <option value="50">$50</option>
			  <option selected="selected" value="100">$100</option>
			  <option value="500">$500</option>
              <option value="1000">$1,000</option>
              <option value="1250">$1,250</option>
              <option value="2500">$2,500</option>
              <option value="5000">$5,0000</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <td>
        <!--<form id="donation_form" action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
          <input type="hidden" name="immediateReturn" value="1" >
          <input type="hidden" name="collectShippingAddress" value="0" >
          <input type="hidden" name="signatureVersion" value="2" >
          <input type="hidden" name="signatureMethod" value="HmacSHA256" >
          <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
          <input type="hidden" name="referenceId" value="Angelwish Year End Donation" >
          <input type="hidden" name="signature" value="X+neiIUfRE0Op4qbwrDvgF92lcQGfny/HxSdpk8sRmI=" >
          <input type="hidden" name="isDonationWidget" value="1" >
          <input type="hidden" name="description" value="Angelwish Year End Donation" >
          <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
          <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks/" >
          <input type="hidden" name="processImmediate" value="1" >
          <input type="hidden" name="cobrandingStyle" value="logo" >
          <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
          <input type="hidden" id='amount' name="amount" size="8"  value="">
          <input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0">
        </form>-->
        <form method="post" id="donation_form"> 
			<style>
			.form_style{background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #000000;
    border-radius: 10px 10px 10px 10px;
    display: none;
    height: 410px;
    left: 38%;
    position: fixed;
    top: 14%;
    width: 345px;
    z-index: 99999999;}
    .logo_from{ float:left; width:100%;  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAYAAABHLFpgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHdJREFUeNqEkcsNgDAMQ632yiYswx5MywbcOwBCQjL9oBBKAj1UivWs2inGaWZAPoHlAok2Kg1Fo6NR4KpRcY7tQihPvsfmrTZ+2TRnOKTRHVJzRN/8x9GXppHP4fxa1g6eiPU9au2WpoLzCNi3JTKtKWZ9OAUYAGipcMVOJd4FAAAAAElFTkSuQmCC") repeat scroll 0 0 transparent; }
    .logo_from img{ float:left; text-align:center; padding-left:55px;}
			
			</style>
					
		<div id="d_form" class="form_style">
		<div style="float:left; width:100%; position: relative;">
			<div style="position: absolute; right: 0; top: 0;cursor: pointer; width: auto;"><img id="close_icon" src="http://angelwish.org/public/images/close_icon.png" alt="" /></div>
			<div class="logo_from"><img src="http://angelwish.org/public/images/xaw_logo.png.pagespeed.ic.WiUTy-3UkG.png" alt="anglewish"></div>
			<table style="  padding-left: 19px; padding-top: 11px;">
				<tbody><tr>
					<td>First Name</td>
					<td><input type="text" placeholder="First Name" name="fname"></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" placeholder="Last Name" name="lname"></td>
				</tr>
				<tr>
					<td>Email Address </td>
					<td><input type="text" placeholder="Email Address" name="email"></td>
				</tr>
				<tr>
					<td>Address </td>
					<td><textarea name="address"></textarea></td>
				</tr>
				<tr>
					<td>City </td>
					<td><input type="text" placeholder="City" name="city"></td>
				</tr>
				<tr>
					<td>State </td>
					<td><input type="text" placeholder="State" name="state"></td>
				</tr>
				<tr>					<td>Zip Code </td>
					<td><input type="text" placeholder="Zip Code" name="zipcode"></td> 
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				 <tr>
					<td colspan="2" align="center"><input type="submit" style="background: url('http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif') no-repeat scroll 0% 0% transparent; border: medium none; width: 134px; height: 37px; text-indent: -999px;" value="Donate" name="submit"></td>
				</tr>
			</tbody></table>
		</div></div>


			<input type="hidden" id='amount' name="amount" size="8"  value="">
			<input type="image"  onclick="return validate();"  src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
		</form>
    </td>
	</tr>
	</table>
    </div>

<p style=" padding-right: 10px; width: 930px;">The Angelwish Learning to Give Program is our way of ensuring that the impact of a donor's donation lasts more than a lifetime.<br/>
  <br/>
In order to stretch every donation, Angelwish enlists corporate volunteers to teach 3rd graders about  Philanthropy. Angelwish customizes a curriculum for each school and company and leads the volunteers through the lessons.</p>
<p style=" padding-right: 10px; width: 930px;">To find out more about what Angelwish can do for you, email <a href="mailto:shimmy@angelwish.org">Shimmy Mehta </a></p>
 
<!--<img src="/public/images/aw_giving_spree.jpg" class="kid"/>-->

    </div>
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>
