<style>
    <?=$css?>
</style>

<div id="thanks_container" class="page">

    <div class="text">
        <h1> Thank You for Making a Difference</h1>
        <p>Thank you for your donation. We will email you a receipt as soon as your donation is processed.</p>
        <p>You've just started a chain reaction. Your contribution is already hard at work helping a chlid living with a chronic illness such as asthma or diabetes</p>
        <p>1.  Young schoolchildren will be taught about giving back through our Angelwish Giving Sprees where they identify the perfect gifts that sick children their age would like.</p>
        <p>2. Those gifts will be sent to hospital social workers who will now be able to focus on their patients without having to locate appropriate toys for them</p>
        <p>3. The social worker will distribute the items to the parents of the children who often have so many challenges, that something as meaningful as a holiday or birthday gift is not a priority. </p>
      <p>4. The parent gets to give that gift to the child  and be seen as a hero the next 365 days of the year. </p>
        <p>Please help us help more children. Share this experience with your friends through email, facebook, smoke signals...<br/>
          <br/>
          Thank you,<br/>
          The Angelwish Team</p>
</div>
</div>

<script type="text/javascript">
    <?//$js?>
</script>
<script type="text/javascript">
$(function(){
	$('#thanks a').addClass('active');
});
</script>