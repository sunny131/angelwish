<style>
    <?=$css?>
    .listing_tr th {
    width: 20%;
    font-weight: bold;
}
.listing_tr th.narrow {
    width: 6%;
}
.listing_tr th {
    width: 8%;
}

.listing_tr th.first {
    width: 4%;
    vertical-align: top;
}
.listing_tr td.first {
    width: 4%;
    vertical-align: top;
}
.listing_tr td.main {
    width: 30%;
}

table {
    width: 100%;
}
</style>
<script>
    $(document).ready(function(){
    $('button.edit_button').live('click',function(){
	var share=$(this).parents('tr').find('td.share:first').text();
	if (share=='Y' || share=='N') {
	    if (share=='Y') {
		  $(this).parents('tr').find('td.share:first').html('<input type="radio" name="share_name" id="share_yes" value="Y" checked="checked" />Yes <input type="radio" name="share_name" id="share_no" value="N" />No');
	    }if (share=='N') {
		  $(this).parents('tr').find('td.share:first').html('<input type="radio" name="share_name" id="share_yes" value="Y" />Yes <input type="radio" name="share_name" id="share_no" value="N" checked="checked" />No');
	    }
	}else{
	var regain_share_value=$(this).parents('tr').find('input[name="share_name"]:checked').val();
	$(this).parents('tr').find('td.share:first').html(regain_share_value);
	}
	console.log($(this).parents('tr').find('td.share:last').text());
	var Title=$(this).parents('tr').find('td.share:last').text();
	if (Title) {
	$(this).parents('tr').find('td.share:last').html('<input type="text" name="share_title" id="share_title" value="'+Title+'" />');   
	}else {
	var regain_share_title=$(this).parents('tr').find('td.share:last input').val();
	$(this).parents('tr').find('td.share:last').html(regain_share_title);    
	}
    });
    $('button.save_button').live('click',function(){
	$('.status_msg').html('Loading...');
	    var input_name=$(this).parents('tr').find('td.share input[name="share_name"]:checked').val();
	    var input_title=$(this).parents('tr').find('td.share input[name="share_title"]').val();
	    var PK_ID=$(this).val();
	    var name=$(this).parents('tr').find('td.name').text();
	    
	    $(this).parents('tr').find('td.share:first').html(input_name);
	    $(this).parents('tr').find('td.share:last').html(input_title);
	    $.ajax({
	    type:"post",
	    url: "/admin/save_changes/",
	    data:{input_name:input_name,input_title:input_title,name:name,id:PK_ID}
	    }).done(function(data) {
		
		$('.status_msg').html('<div style="background-color:rgb(147,162,120);width:100%;padding:10px;color:white;">'+data+'</div>').show(0).delay(5000).hide(0);
	    });
	});
    });
</script>

<div id="admin" style="height: auto !important">
    <p><a href="/admin/">Go back to admin panel</a></p>
  <span style="color: #333333;font-size: 18px;font-weight: normal;width: 50%;float: left">Companies</span> <span style="color: #333333;font-weight: normal;width: 50%;float: left"><div class="status_msg"></div></span>

   <table>
    <tbody>
	<tr class="listing_tr">
	<th class="first"><a href="" class="click_cid"><strong>S.no <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>
        <th ><a class="click_care" href=""><strong>Company Name <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>

        <th><a class="click_care" href=""><strong>Sub-Domain <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>

	<th class="narrow"><a class="click_care" href=""><strong>Share <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>
        <th><a class="click_care" href=""><strong>Title <img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a></th>
        <th class="narrow"> <a href="" class="click_care"><strong>Active<img src="http://www.fatherdyer.org/admin/skins/Ascribe/icons/10x10/1downarrow.gif" /></strong></a> </th>

        <th></th>
	</tr>
   <?php $i=1;
   foreach($companies as $company):
   echo '<tr class="'.$i.'">
	<td>'.$i.'</td>
	<td class="name">'.$company['name'].'</td>
	<td>'.$company['url'].'</td>
	<td class="share">'.$company['share'].'</td>
	<td class="share">'.$company['Title'].'</td>
	<td>'. (($company['is_active']>0) ? "<b>Active</b>": "<font color='red'>In-active</font>").'</td>
	<td><button class="edit_button" type="button" name="edit" value="'.$i.'">Edit</button><button type="button" class="save_button" name="edit" value="'.$company['pk_company_id'].'">Save</button></td>
    </tr><tr><td colspan="7"></td></tr>'; $i++; endforeach?>
    </tbody>
</table>
</div>
