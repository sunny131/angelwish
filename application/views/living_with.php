<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text"><h1>Children Living With Chronic Illnesses</h1>
    <h2> Become aware </h2>
<p>Chances are that someone you know has a chronic illness such as Asthma, Diabetes, Kidney Disease, or one of the many other medical conditions that  require frequent attention, treatment, and sometimes even hospitalization..

Imagine how a child's life can be disrupted by having to prick themselves several times a day to test their blood sugar? How a child with hemophilia must abstain from recess to ensure they don't injure themselves?<br/> <br/>
Angelwish was founded in 1999 to ensure that each and every child affected by HIV/AIDS would have an enjoyable childhood - along the way, we realized how the work that we do could help even more children who are not experiencing the same happy childhood that we have.

Join us and grant a holiday or birthday wish to a child living with Chronic Illness in the United States or United Kingdom through our special Amazon.com wishlists or contribute to the life sustaining needs of children overseas.</p>
<p>&nbsp;</p>

<img src="/public/images/living_with_img.png" class="kid"/>

    </div>
    <?=modules::run('Whatwedo')?> 
    </div>
     
    

<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>