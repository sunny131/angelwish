<style>
    <?=$css?>
</style>

<?php 
	function vsort($a,$subkey,$sort) {
	foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	$sort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
	}
?>

<div id="living_with_container" class="page">
	<div class="living_with_text" style="width: 50%; padding: 10px; float: left;">
    <h1 align="left">Hurricane Sandy Relief 2012</h1>
<h2 align="left"> Helping Families Affected by the Storm</h2>

    <div class="donate">
      <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Make a Quick Donation</b></div>
		<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		<style>
			.hide{display: none;padding:5px;}
		</style>
		<script language="javascript">
			function showhide(val)
			{
			var ge1=document.getElementById('d1');
			var ge2=document.getElementById('d2');
				if(val==-1)
				{
					ge1.style.display="block";
					ge2.style.display="none";
				}
				else
				{
					ge1.style.display="none";
					ge2.style.display="block";
				}
			}

			function validate()
			{
				var valx = document.getElementById('amountbox').value;
				if (valx==-1)
				{
					var other_amount = document.getElementById('otherbox').value;
					if(other_amount > 0){
						document.getElementById('amount').value = document.getElementById('otherbox').value;
					}else{
						alert("Please add a valid amount");
						return false;
					}
				}
				else
				{
					document.getElementById('amount').value  = valx;
				}
				document.getElementById('donation_form').submit();
			}
		</script>

		<td>	<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
				  <option value="25">$25</option>
				  <option value="50">$50</option>
				  <option value="100">$100</option>
				  <option value="500">$500</option>
				  <option value="1000">$1000</option>
				  <option value="2500">$2500</option>
				  <option value="5000">$5000</option>
				  <option value="-1">Other</option>
				</select>
		</td>
		<td width="100px">
			<div id="d1" class="hide">
			<input type="text" id="otherbox" name="otherbox" size="5" value="">
			</div>
			<div id="d2"></div>
		</td>
		<td>
			<form action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
		  <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks" >
		  <input type="hidden" name="processImmediate" value="1" >
		  <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
		  <input type="hidden" name="signatureMethod" value="HmacSHA256" >
		  <input type="hidden" name="collectShippingAddress" value="0" >
		  <input type="hidden" name="isDonationWidget" value="1" >
		  <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
		  <input type="hidden" name="referenceId" value="Hurricane Sandy Relief" >
		  <input type="hidden" name="cobrandingStyle" value="logo" >
		  <input type="hidden" name="immediateReturn" value="0" >
		  <input type="hidden" name="description" value="Hurricane Sandy Relief 2012" >
		  <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
		  <input type="hidden" name="signatureVersion" value="2" >
		  <input type="hidden" name="signature" value="csulUgVE8q5wZocswS8Li6qQt4AV3LCDEdqFjT0IKOU=" >
		  <input type="hidden" id='amount' name="amount" size="8"  value="">
		<input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0">
		</form>
		</td>
		</tr>
		</table>
		    </div>
  </div>
	<div style="width:40%;float:left;margin-top:40px;">
	  <p>Hurricane Sandy has taken lives, homes, and certainly childhood memories for both young and old. Angelwish invites you to support the holiday wishes of children and families who have been affected by Hurricane Sandy as this may be the only new toy the get this holiday season.<br /><br /> 
      We are leveraging our network of major hospitals to receive and distribute these gifts as a part of their committment to their communities. As always, 100% of your donations will go to helping the underserved.<br /><br />       
      Select one of the Hospitals or Care Centers below to find out how you can make the holidays brighter or make a quick donation to your left and we'll help those who need it most.  
      </div>
	<div style="clear:both"></div>
	<h1>Select one of the Hospitals or Care Centers Below to Donate</h1>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in New York </h3>
		<ul>
			<?php $location1["New York"] = vsort($location1["New York"],'city',asort) ?>
			<?php foreach($location1["New York"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, New York</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in New Jersey </h3>
		<ul>
			<?php $location2["New Jersey"] = vsort($location2["New Jersey"],'city',asort) ?>
			<?php foreach($location2["New Jersey"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, New Jersey</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in Connecticut </h3>
		<ul>
			<?php $location3["Connecticut"] = vsort($location3["Connecticut"],'city',asort) ?>
			<?php foreach($location3["Connecticut"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, Connecticut</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in Pennsylvania </h3>
		<ul>
			<?php $location4["Pennsylvania"] = vsort($location4["Pennsylvania"],'city',asort) ?>
			<?php foreach($location4["Pennsylvania"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, Pennsylvania</div>
			</li>
			<?php } ?>
		</ul>
	</div>

	
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>

