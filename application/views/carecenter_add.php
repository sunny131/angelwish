<style>
    <?=$css?>
</style>

<div id="add-care-center">
    <h4><a href="/admin">Back to Admin Panel</a></h4>
    <h2>Add a Care Center</h2>

    <?//validation_errors(); ?>
    <form>
        <fieldset>
            <legend>General Information</legend>
            <table id="general-information">
                <tr>
                    <td class="label"><label for="name">Name&nbsp;<span class="error">*</span></label></td>
                    <td><input id="name" name="name" maxlength="100"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="description">Description&nbsp;<span class="error">*</span></label></td>
                    <td><textarea id="description" name="description"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2">Address</td>
                </tr>
                <tr>
                    <td class="label"><label for="street">Street&nbsp;<span class="error">*</span></label></td>
                    <td><input id="street" name="street" maxlength="45"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="street2">Suite/Floor</label></td>
                    <td><input id="street2" name="street2" maxlength="45"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="city">City&nbsp;<span class="error">*</span></label></td>
                    <td><input id="street2" name="city" maxlength="45"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="state">State&nbsp;<span class="error">*</span></label></td>
                    <td><input id="street2" name="state" maxlength="45"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="country">Country&nbsp;<span class="error">*</span></label></td>
                    <td><input id="street2" name="country" maxlength="45"/></td>
                </tr>
                <tr>
                    <td class="label"><label for="zipcode">Zip Code&nbsp;<span class="error">*</span></label></td>
                    <td><input id="street2" name="zipcode" maxlength="45"/></td>
                </tr>
            </table>

            
        </fieldset>
    </form>
</div>
<script type="text/javascript">
    <?//$js?>
</script>