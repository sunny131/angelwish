<style>
    <?=$css?>
</style>

<div id="Stories_container" class="page">

    <h1> Privacy Policy </h1>
   <div style="min-height: 400px;padding: 10px;">
	<p style="margin-bottom: 0.35cm;"><b>Last Updated: August 2014</b></p>
<p style="margin-bottom: 0.35cm;">Angelwish ("we," "us," or "our") is committed to preserving the privacy of all visitors to www.angelwish.org (the "Site").
Before using the Site, please read this Privacy Policy carefully to ensure that you understand the types of information we collect at the Site and how we use and protect that information.  By visiting the Site, you accept the practices and policies outlined in this Privacy Policy and consent to having your data transferred to and/or processed in the United States.  Please do not use the Site if you do not agree to this Privacy Policy.</p>

<p style="margin-bottom: 0.35cm;"><b>Information We Collect</b></p>
<p style="margin-bottom: 0.35cm;"><i>Personal Information:</i> When you visit or use the Site, we may collect personal information from or about you such as your name, email address, mailing address, telephone number(s), limited location information, usernames, or passwords. We collect this information when you provide it to us through, for example, emails you send to us, our newsletter sign-up form, surveys, applications or other online fields. We may also collect personal information about you when you use third party platforms like Facebook or Twitter to interact with us or the Site.</p>
<p style="margin-bottom: 0.35cm;"><i>Usage and Other Information:</i> As is the case with many Web sites, our servers automatically collect certain non-personal information about your interaction with the Site and the services on the Site. We receive and record information on our server logs from your browser including your browser type and your IP address when you visit the Site. We may also capture certain clickstream data, which includes, for example, the referring page that linked you to the Site, the pages you visit, any search terms you have entered on the Site or a referral site, and the next Web site you visit after you leave the Site.</p>
<p style="margin-bottom: 0.35cm;"><i>Information Obtained from Third Parties:</i> In some instances, we may supplement the information collected from you with information that is received or otherwise obtained from third parties.</p>
<p style="margin-bottom: 0.35cm;"><b>Your Payment Card and Financial Account Information</b></p>
<p style="margin-bottom: 0.35cm;">Angelwish does not collect, store or otherwise maintain any payment card or financial account information from individuals who make a donation through the Site. Donations, including information associated with your credit or debit card, are processed by our partners Amazon Payments, PayPal, and Dwolla. All of the information you provide to our payment-processing partners will be handled in accordance with the terms of their privacy policies. As with all third-party websites, we encourage you to review the applicable privacy policies carefully before submitting any personal information.</p>
<p style="margin-bottom: 0.35cm;">Amazon Payments: https://payments.amazon.com/help/Personal-Accounts/Privacy-Security/Privacy-Notice</p>
<p style="margin-bottom: 0.35cm;">PayPal: https://www.paypal.com/webapps/mpp/ua/privacy-full?country.x=US&amp;locale.x=en_US</p>
<p style="margin-bottom: 0.35cm;">Dwolla: <span style="font-family: Arial,serif;"><span style="font-size: small;"><span style="background: #ffffff;">https://www.dwolla.com/privacy</span></span></span></p>
<p style="margin-bottom: 0.35cm;"><b>Use of Information</b></p>
<p style="margin-bottom: 0.35cm;">We and our trusted partners use the information we collect in a variety of ways. For example, your information may be used in order to administer, develop, and improve the Site and the services we offer, process your donations and fulfill your donation, facilitate your participation in certain activities on the Site or related to Angelwish, prevent fraud and enhance the security of the Site, and communicate with you about Angelwish and/or how you can get more involved with our mission to help children living with chronic illness. We and our partners may also use the information we collect to assess the level of interest in the Site, our e-mails, and our other messaging campaigns. Furthermore, we reserve the right to use data that we collect on an aggregate or anonymous basis for various business purposes, where permissible under applicable laws and regulations.</p>
<p style="margin-bottom: 0.35cm;"><b>Disclosure of Information</b></p>
<p style="margin-bottom: 0.35cm;">We may share your personal information with third parties for many reasons. In addition to sharing your personal information with third parties who provide services to us or on our behalf, we may share your information:</p>

<ul style="padding:18px;">
	<li>
<p style="margin-bottom: 0.35cm;">with our partner organizations and other third parties so that they may contact you from time to time about events, products and services which may be of interest to you</p>
</li>
	<li>
<p style="margin-bottom: 0.35cm;">as required by law and when we believe in good faith that disclosure is necessary to protect our rights or those of third parties, protect your safety or the safety of others, investigate fraud, or comply with a court order or other legal process</p>
</li>
	<li>
<p style="margin-bottom: 0.35cm;">in connection with a corporate change including a merger, acquisition or sale of assets</p>
</li>
</ul>
<p style="margin-bottom: 0.35cm;">In addition, we may share aggregate Site usage information that does not identify individual users with third parties for any reason, including, for example, marketing or analytical purposes. Any or all of the above disclosures may involve overseas storage and other overseas transfer, processing and use of your personal information, including in or to jurisdictions which do not offer the same level of protection of personal information as may be enjoyed within your home country.</p>
<p style="margin-bottom: 0.35cm;"><b>Cookies and Other Tracking Technologies </b></p>
<p style="margin-bottom: 0.35cm;">We, or our service providers, and other companies we work with may deploy and use cookies, web beacons and other tracking technologies for various purposes, such as fraud prevention and monitoring our advertising and marketing campaign performance.</p>

<ul style="padding:18px;">
	<li>
<p style="margin-bottom: 0.35cm;">"Cookies" are small amounts of data a website can send to a visitor's web browser. They are often stored on the device you are using to help track your areas of interest. Cookies may also enable us or our service providers and other companies we work with to relate your use of the Site over time to customize your experience. Most web browsers allow you to adjust your browser settings to decline or delete cookies, but doing so may degrade your experience with the Site.</p>
</li>
	<li>
<p style="margin-bottom: 0.35cm;">Local shared objects, commonly called "flash cookies," may be stored on your hard drive using a media player installed on your computer or device. LSOs operate a lot like other cookies, but cannot be managed in the same way as non-Flash cookies. For more information on LSOs, <a href="http://www.adobe.com/security/flashplayer/articles/lso/">click here</a>.</p>
</li>
	<li>
<p style="margin-bottom: 0.35cm;">A pixel tag, also known as a "clear GIF" or "web beacon," is typically a one-pixel, transparent image (although it can be a visible image as well), located on a Web page or in an e-mail or other type of message, which is retrieved from a remote site on the Internet enabling the verification of an individual's viewing or receipt of a Web page or message. A pixel tag may enable us to relate your viewing or receipt of a Web page or message to other information about you, including your personal information.</p>
</li>
</ul>
<p style="margin-bottom: 0.35cm;">Other companies that are advertising on the Site may also use cookies, LSOs, or pixel tags, over which we have no control. Other companie's use of such technologies is subject to their own privacy policies. Such third party advertisers do not have access to our cookies, LSOs, or pixel tags, and we do not allow third parties to collect personal information about your online activities except as described above. The Site is not currently configured to respond to "do not track" signals or similar mechanisms.</p>
<p style="margin-bottom: 0.35cm;"><b>Security</b></p>
<p style="margin-bottom: 0.35cm;">We employ reasonable security measures to help safeguard the information we collect from you on the Site, including technical and organizational measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration or disclosure. Nevertheless, the Internet is an open system and we cannot guarantee that unauthorized third parties will never be able to access or use your personal information for improper purposes.</p>
<p style="margin-bottom: 0.35cm;"><b>Children</b></p>
<p style="margin-bottom: 0.35cm;">The Site is not intended for persons under the age of 13. We do not knowingly collect personal information from children under 13 without parental consent. If we become aware that any person submitting information to the Site is under the age of 13, we will attempt to delete the account and any related information as soon as possible.</p>
<p style="margin-bottom: 0.35cm;"><b>Users from Outside the United States </b></p>
<p style="margin-bottom: 0.35cm;">The Site is hosted in the United States and is governed by U.S. law. If you are visiting the Site from outside the United States, please be aware that your information may be transferred to, stored and processed in the United States, and other jurisdictions, where the data protection and other laws may not be as comprehensive as those in your country. By using the Site, you consent to your information being transferred to our facilities and to the facilities of those third parties with whom we share it as described in this Privacy Policy.</p>
<p style="margin-bottom: 0.35cm;"><b>Changes to this Privacy Policy</b></p>
<p style="margin-bottom: 0.35cm;">This Privacy Policy may be updated periodically and without prior notice to you to reflect changes in our online information practices. Any changes will be posted to the Site and the last updated date at the top of this policy will be revised. We also encourage users to periodically review this Privacy Policy to understand how we protect and use your information.</p>
<p style="margin-bottom: 0.35cm;"><b>How to Contact Us and Update Your Personal Information</b></p>
<p style="margin-bottom: 0.35cm;">If you have any questions or comments about this Privacy Policy or if you would like us to update information we have about you or your preferences, please contact us by sending an e-mail to <a href="mailto:privacy@angelwish.org">privacy@angelwish.org</a> with "Privacy Policy Inquiry" in the subject line.</p>
<p style="margin-bottom: 0.35cm;"><b>Links to Third-Party Sites</b></p>
<p style="margin-bottom: 0.35cm;">We may provide links to other third-party websites for your convenience and information. These sites may have their own privacy statements in place, which we encourage you to review. We are not responsible for the content or use of any linked third-party websites.</p>
<p style="margin-bottom: 0.35cm;"><b>Your California Privacy Rights</b></p>
<p style="margin-bottom: 0.35cm;">California law permits our customers who are California residents to request and obtain from us once a year, free of charge, information about the personal information (if any) we disclosed to third parties for direct marketing purposes in the preceding calendar year. If applicable, this information would include a list of the categories of personal information we shared and the names and addresses of the third parties which, in the immediately preceding calendar year, received your personal information from us for their own marketing purposes. If you are a California resident and would like to make such a request, please submit your request in writing to the below address. Please also reference the "California Shine the Light" privacy law in your request.</p>
<p style="margin-bottom: 0.35cm; line-height: 100%;"><span style="font-size: small;"><span style="font-size: small;">Angelwish
Attn: California Shine the Light Privacy Request
P.O. Box 186
Rutherford, NJ 07070</span></span></p></div>
</div>



