<?php
//  This is forcibly calling the handleCallback function which in turn calls invokePay.
//	This was originally supposed to be triggered in cart.php via the thank_you function.
//  Code igniter's URI route functionality found in route.php should handle this but it never actually triggers for thank-you
//
//	In thank_you in cart.php is where the AngelWish emails are being sent, this code is currently commented out
//	
require_once(APPPATH.'libraries/AmazonPayments.php');

$this->amazonpayments = AmazonPayments::getInstance();
$this->amazonpayments->handleCallBack();
        $host = $_SERVER['HTTP_HOST'];
        $dot_position = stripos($host,".");
        $company_url = $dot_position ? substr($host,0,$dot_position ) : false;
        if($company_url=='www'){
        $add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `id`=1") or die(mysql_error());
        $fetch_add=mysql_fetch_array($add);
        }
        else{
        $add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `company`='".$company_url."'") or die(mysql_error());   
        $num_rows=mysql_num_rows($add);
        if($num_rows>0){
        $fetch_add=mysql_fetch_array($add);
        }else{
        $fetch_add['advertisement']='';
        }
        }

?>


<style>
    <?=$css?>
</style>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=309252795879563";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<meta property="og:image" content="http://angelwish.org/public/images/companylogos/awlogored.png" />

<div id="thank_you" class="page">
    <div id="slideshow-container">
        <div class="text">
            <h1>Thank You for Your Donation!</h1>
            
            <div style="    color: #777777;
    font-family: Helvetica,Arial,sans-serif;
    font-size: 14px;
    font-weight: normal;
    line-height: 18px;
    margin: 20px 0 0 20px;
    width: 550px;">Angelwish thanks you for helping our children and families who have been affected by chronic illness. Your support during the holidays will truly have an exponential impact as the gift will be sent to the social worker who will give it to the parent to give to the child. It is an amazing chain reaction that you have started.<br/>
              <br/>
We will email you a receipt for tax purposes as soon as your donation is processed. If you need anything else, please feel free to email us at info@angelwish.org<br/>
              <br/>
Please help us help more families. Share this experience with your friends through email, facebook, smoke signals...<br/>
              <br/>
              <br/>
            <?php if($fetch_add['advertisement']!='') {?> <h3>Advertisement</h3>
              	<div style='float:left; width:560px;font-size: 13px;'>
	      <?php echo $fetch_add['advertisement'];
              } ?>
		</div>
<br /><br /><br />
           
          Thank you,
            <br />
              The Angelwish Team<br/>
              <br/>
               </div>
      </div>
        <div class="image">
            <img src="/public/images/smiling-girl.png"/>
      </div>
    </div>
    <div class="bottom">
        <h3>Other ways to help</h3>
        <p>
            Please join our groups on Facebook and Twitter and help spread the word.
        </p>
        <br class="clear"/>
        <div class="facebook">
            <div style="padding-bottom: 15px;">
                <div id="fb-root"></div>
                <script src="https://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                <fb:like href="http://www.angelwish.org" send="true" width="140" layout="button_count" show_faces="false" font="arial"></fb:like>
            </div>
            <!--<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fangelwish&amp;width=400&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=true&amp;header=true&amp;height=500" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:500px;" allowTransparency="true"></iframe>-->
        </div>
        <div class="facebook" style="margin-left: 70px;">
            <div style="padding-bottom: 15px;">
                <!--<div class="fb-share-button" data-href="http://angelwish.org" data-type="button"></div>-->
                <a class="btn" target="_blank" href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://angelwish.org/&amp;p[images][0]=http://angelwish.org/public/images/companylogos/awlogored.png">share on facebook</a>
            </div>
          
        </div>
        <div class="twitter">
            <div style="width: 220px;padding-bottom: 15px;">
                <iframe allowtransparency="true" frameborder="0" scrolling="no"
                  src="http://platform.twitter.com/widgets/follow_button.html?screen_name=angelwish"
                  style="width:220px; height:20px;"></iframe>
            </div>
            <script src="http://widgets.twimg.com/j/2/widget.js"></script>
            <script>
            new TWTR.Widget({
              version: 2,
              type: 'profile',
              rpp: 6,
              interval: 6000,
              width: 400,
              height: 400,
              theme: {
                shell: {
                  background: '#ffffff',
                  color: '#333333'
                },
                tweets: {
                  background: '#ffffff',
                  color: '#333333',
                  links: '#2898e2'
                }
              },
              features: {
                scrollbar: false,
                loop: false,
                live: true,
                hashtags: true,
                timestamp: true,
                avatars: true,
                behavior: 'all'
              }
            }).render().setUser('angelwish').start();
            </script>
        </div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
  /* $(document).ready(function () {
    // Handler for .ready() called.
    window.setTimeout(function () {
        location.href = "<?php echo 'http://'.$_SERVER['HTTP_HOST']; ?>";
    }, 5000)
});*/
</script>
