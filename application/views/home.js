$(document).ready(function() {

	$("a#video-link").fancybox({
		'hideOnContentClick': false
	});

    $("a#video-link").click(function(){
        $("#video").html('<iframe src="http://player.vimeo.com/video/15066602?title=0&amp;'+
            'byline=0&amp;portrait=0;&amp;autoplay=1;" width="620" height="370" frameborder="0"></iframe>');
    });

});

function validate_name(){
	var error=0;
	$('input[name="fname"]').attr('style','border:1px solid grey');
	$('input[name="lname"]').attr('style','border:1px solid grey');
	$('input[name="email"]').attr('style','border:1px solid grey');
	if($('input[name="fname"]').val() == '') {
		$('input[name="fname"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if($('input[name="lname"]').val() == '') {
		$('input[name="lname"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if($('input[name="email"]').val() == '') {
		$('input[name="email"]').attr('style','border:2px solid red');
		error=error+1;
	}
	if (error > 0) {
	return false;
	}else{
	return true;
	}
	
}