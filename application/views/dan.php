<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text">
    <h1 align="left"><img src="/public/images/danpirron.png" width="200" height="279" /></h1>
    <h1 align="left">In Memory of Daniel Pirron</h1>
    <h2 align="left"> A great supporter and friend</h2>

    <div class="donate">
      <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Support Children in Need</b></div>
	<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}

		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
			document.getElementById('donation_form').submit();
		}
	</script>

	<td>	<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
			  <option value="25">$25</option>
			  <option value="50">$50</option>
			  <option value="100">$100</option>
			  <option value="250">$250</option>
              <option value="500">$500</option>
              <option value="750">$750</option>
              <option value="1000">$1000</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <td>
        <form action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
	  <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks" >
	  <input type="hidden" name="processImmediate" value="1" >
	  <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
	  <input type="hidden" name="signatureMethod" value="HmacSHA256" >
	  <input type="hidden" name="collectShippingAddress" value="0" >
	  <input type="hidden" name="isDonationWidget" value="1" >
	  <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
	  <input type="hidden" name="referenceId" value="In Memory of Daniel Pirron" >
	  <input type="hidden" name="cobrandingStyle" value="logo" >
	  <input type="hidden" name="immediateReturn" value="1" >
	  <input type="hidden" name="description" value="In Memory of Daniel Pirron" >
	  <input type="hidden" name="abandonUrl" value="http://www.angelwish.org/dan" >
	  <input type="hidden" name="signatureVersion" value="2" >
	  <input type="hidden" name="signature" value="Vr50z/UpZsszhVHqrI60BxG6XgqF1QW5nv7eHa3+MEQ=" >
      <input type="hidden" id='amount' name="amount" size="8"  value="">
	<input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0"></td>
	</form>

    </td>
	</tr>
	</table>
    </div>

<p>Daniel Pirron was a major cheerleader for Angelwish personally and as a Partner at Deloitte. He truly believed in the mission of our organization and understood that chronic illnesses such as Asthma, Diabetes or Kidney Disease, required frequent attention, treatment, and sometimes even hospitalization, resulting in a disrupted childhood for many. He saw our approach to giving helped bring childhood back to those in need in a way that retained the dignity of them and their families.<br/> <br/> 

We believe that he grasped that so well because he lived those ideals every day as an advocate and supporter of so many people in life - not for recognition, but because that was who he was.<br/><br/>

Our deepest sympathies go to his family, loved ones and colleagues. In lieu of flowers, the family has requested that tax deductible donations be made to Angelwish either online or sent to the following address. 
 
 <p style="clear:both; padding-top:15px;">
 Angelwish<br/>
 48 Arthur Drive<br/>
 Rutherford, NJ 07070 
 </p>
 <p style="clear:both; padding-top:15px;">Thank You</p>   
 
  </div>
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>

