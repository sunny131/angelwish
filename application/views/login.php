<style>
    <?=$css?>
</style>

<div id="login">
    <?if($error){?>
    <h4 class="error">Email/Password combination is not correct.
        Please try again.</h4>
    <?}?>
    <h3>Please sign in</h3>

    <form action="/login/process" method="POST">
        <div class="login-box">
            <div class="inner">
                <table class="form-input">
                    <tr>
                        <td class="label">
                            <label for="email">Email:</label>
                        </td>
                        <td>
                            <input type="text" name="email" id="email" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            <label for="email">Password:</label>
                        </td>
                        <td>
                            <input type="password" name="password" id="password" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Submit" style="font-size:14px;"/></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?//$js?>
</script>