<link rel="stylesheet" href="/public/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<style>
    <?=$css?>
    .donate {
    background-color: #F4F4F4;
    border-radius: 5px 5px 5px 5px;
    margin-bottom: 15px;
    padding: 10px;
    width: 23em;
}
tr.tabletr td.first_tr{
    width: 200px;
    float: left;
    text-align: left;
    padding-top:7px;
}
</style>

<div id="home">
    <!--<h1>Helping Children &amp; Families With Chronic Illnesses
    <?php /*if(isset($total_entries)){ 
			echo  $total_entries;
		 }*/ ?>
    </h1> -->
    <div class="motto" style="width:500px;text-align:center">
		<h1 style="line-height:40px;">Helping Children &amp; Families With Chronic Illnesses </h1>
	</div>

   
			<div class="donate" style="float:right">
			<div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Support Angelwish's Programs for Kids!</b></div>
          <table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr class="tabletr">
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}

		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
			$("#d_form,#black_layer").show();
				return false;
			/*document.getElementById('donation_form').submit();*/
		}
		$(document).ready(function(){
		    $("#black_layer,#close_icon").click(function(){
			$("#d_form,#black_layer").hide();
				$('input[name="fname"]').attr('style','border:1px solid grey');
				$('input[name="lname"]').attr('style','border:1px solid grey');
				$('input[name="email"]').attr('style','border:1px solid grey');
		    });
		    $('input[name="recurring"]').click(function(){
			if($('input[name="recurring"]:checked').length>0){
			$(this).val(1);
			$('.subscription').html('<select name="subscribe_period"><option value="1 month" selected="selected">Monthly</option><option value="3 month">Quarterly</option><option value="12 month">Annual</option></select');
			$('.hidden_elm').html('<input type="hidden" name="subcribe_period" id="subcribe_period" value="1 month" />');
			}else {$(this).val(0);
			$('.subscription').html('');
			}
		    });
				
				
	$( 'select[name="subscribe_period"]').live('change',function() {
	    var period=$(this).val();
	    $("#subcribe_period").val(period);
	});
		
			});
	</script>

	<td class="first_tr">
	    <span class="subscription"></span>
	    <select id="amountbox" name="amountbox" onchange="showhide(this.value)">
			  <option value="25" selected="selected">$25</option>
              <option value="50">$50</option>
		<option  value="100">$100</option>
		<option value="500">$500</option>
              <option value="1000">$1,000</option>
              <option value="1250">$1,250</option>
              <option value="2500">$2,500</option>
              <option value="5000">$5,000</option>
			  <option value="-1">Other</option>
			</select>
	  <div>Make this recurring <input type="checkbox" name="recurring" id="recurring" title="Check to schedule payment after interval" value="0" /></div>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <td>
        <!--<form id="donation_form" action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
          <input type="hidden" name="immediateReturn" value="1" >
          <input type="hidden" name="collectShippingAddress" value="0" >
          <input type="hidden" name="signatureVersion" value="2" >
          <input type="hidden" name="signatureMethod" value="HmacSHA256" >
          <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
          <input type="hidden" name="referenceId" value="Angelwish Year End Donation" >
          <input type="hidden" name="signature" value="X+neiIUfRE0Op4qbwrDvgF92lcQGfny/HxSdpk8sRmI=" >
          <input type="hidden" name="isDonationWidget" value="1" >
          <input type="hidden" name="description" value="Angelwish Year End Donation" >
          <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
          <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks/" >
          <input type="hidden" name="processImmediate" value="1" >
          <input type="hidden" name="cobrandingStyle" value="logo" >
          <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
          <input type="hidden" id='amount' name="amount" size="8"  value="">
          <input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/golden_small_donate_withmsg_whitebg.gif" border="0">
        </form>-->
        <form method="post" id="donation_form"> 
			<style>
			.form_style{background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #000000;
    border-radius: 10px 10px 10px 10px;
    display: none;
    height: 410px;
    left: 38%;
    position: fixed;
    top: 14%;
    width: 345px;
    opacity:0.9;
    z-index: 99999999;}
    .logo_from{ float:left; width:100%;  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAYAAABHLFpgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHdJREFUeNqEkcsNgDAMQ632yiYswx5MywbcOwBCQjL9oBBKAj1UivWs2inGaWZAPoHlAok2Kg1Fo6NR4KpRcY7tQihPvsfmrTZ+2TRnOKTRHVJzRN/8x9GXppHP4fxa1g6eiPU9au2WpoLzCNi3JTKtKWZ9OAUYAGipcMVOJd4FAAAAAElFTkSuQmCC") repeat scroll 0 0 transparent; }
    .logo_from img{ float:left; text-align:center; padding-left:55px;}
			
			</style>
					
		<div id="d_form" class="form_style">
		<div style="float:left; width:100%; position: relative;">
			<div style="position: absolute; right: 0; top: 0;cursor: pointer; width: auto;"><img id="close_icon" src="http://angelwish.org/public/images/close_icon.png" alt="" /></div>
			<div class="logo_from"><img src="http://angelwish.org/public/images/xaw_logo.png.pagespeed.ic.WiUTy-3UkG.png" alt="anglewish"></div>
			<table style="  padding-left: 19px; padding-top: 11px;">
				<tbody><tr>
					<td>First Name <span class="star">*</span></td>
					<td><input type="text" placeholder="First Name" name="fname" size="15"></td>
				</tr>
				<tr>
					<td>Last Name <span class="star">*</span></td>
					<td><input type="text" placeholder="Last Name" name="lname" size="15"></td>
				</tr>
				<tr>
					<td>Email Address <span class="star">*</span></td>
					<td><input type="text" placeholder="Email Address" name="email" size="15"></td>
				</tr>
				<tr>
					<td>Address </td>
					<td><textarea name="address" cols="24"></textarea></td>
				</tr>
				<tr>
					<td>City </td>
					<td><input type="text" placeholder="City" name="city" size="15"></td>
				</tr>
				<tr>
					<td>State </td>
					<td><input type="text" placeholder="State" name="state" size="15"></td>
				</tr>
				<tr>					<td>Zip Code </td>
					<td><input type="text" placeholder="Zip Code" name="zipcode" size="15"></td> 
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				 <tr>
					<td colspan="2" align="center"><input type="submit" style="background: url('http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif') no-repeat scroll 0% 0% transparent; border: medium none; width: 134px; height: 37px; text-indent: -999px;" value="Donate" name="submit" onclick="return validate_name();"></td>
				</tr>
			</tbody></table>
		</div></div>

<div class="hidden_elm"></div>
			<input type="hidden" id='amount' name="amount" size="8"  value="">
			<input type="image"  onclick="return validate();"  src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
		</form>
    </td>
	</tr>
	</table>
	
	</div>
	 

    <div id="slideshow-container">
        <?=modules::run('Easyslideshow')?>
    </div>

    <div id="whatwedo-container">
        <div class="box first">
            <div class="image">
                <img class="icon" src="/public/images/wings_ico.png" align="absmiddle" style="width:30px"/>
            </div>
            <div class="title">
                <h3>What we do</h3>
            </div>
            <a id="video-link" href="#video">
                <img class="video" src="/public/images/intro_video.jpg"/>
            </a>
            <div style="display:none;">
                <div id="video" class="overlay">
                    <iframe height="370" frameborder="0" width="620" src="about:blank"></iframe>
                </div>
            </div>
        </div>

        <div class="box">
            <div class="image">
                <img class="icon" src="/public/images/icon2.png" align="absmiddle"/>
            </div>
            <div class="title">
                <h3>Where we work</h3>
            </div>
            <p>Angelwish works on 6 continents with over 110 Hospitals and Care Centers in the United States and an additional 15 Care Centers in 9 other countries.</p>
            <a href="/about#wherewework" class="read-more"> Read More </a>
        </div>

        <div class="box last">
            <div class="image">
                <img class="icon" src="/public/images/icon3.png" align="absmiddle"/>
            </div>
            <div class="title">
                <h3>Angelwish on Eyewitness News Sunday</h3>
            </div>
            <p align="center"><a href="http://7online.com/health/angelwish-educates-children-about-helping-others/64777/" target="_blank">Click here to see our Eyewitness News Story</a></p>
            <p align="center">&nbsp;</p>
        </div>
    </div>

    <div id="bottomrow-container">
        <div class="bottomrow-item big">
            <img class="pic" src="/public/images/kid_small.png"/>
            <div class="testimonial">
                <img class="open-quotes" alt="" src="/public/images/open_quotes.jpg">
                <p>
                    It is a real gift to be invited into a family's experience, particularly at this extremely vulnerable time. It is also very humbling to be the liaison for the community that is providing time, generosity & compassion for these often overlooked children and families. - Social Worker at the Jersey Shore Medical Center
              </p>
                <img class="close-quotes" alt="" src="/public/images/close_quotes.jpg">
            </div>
            <a href="/stories"> Read More Testimonials </a>
        </div>


        <div class="bottomrow-item small">            
            <a href="http://www.flickr.com/photos/angelwish/collections/" target="_blank" title="Angelwish Photostream">
                <img src="/public/images/collage.png" alt="Angelwish Photostream"/>
            </a>
        </div>
    </div>
</div>

<script type="text/javascript" src="/public/js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
    <?=$js?>
</script>
