<style type="text/css">
    <?=$css?>
</style>





<div id="about_container">
    <!-- the tabs -->
    <ul class="tabs">
        <li><a href="#mission"> <img src="/public/images/wings_ico.png" align="middle"/>Mission Statement</a></li>
        <li><a href="#whatwedo"><img src="/public/images/icon5.png" align="middle" />What we do </a></li>
        <li id="where_we_work"><a href="#wherewework"><img src="/public/images/icon8.png" align="middle" />Where we work</a></li>
        <li><a href="#board"><img src="/public/images/icon6.png" align="middle"/>Board members</a></li>
        <li><a href="#annualreports"><img src="/public/images/icon7.png" align="middle"/>Annual reports</a></li>
    </ul>

    <!-- tab "panes" -->
    <div class="panes">
        <div class="pane">
            <h1> About Angelwish</h1>
            <h2> Mission Statement</h2>
            <div class="left">
                <p>
                    Angelwish.org was created in 1999 with the mission to provide the public with an easy way to grant wishes to the millions of children that are living with chronic illnesses around the world. 
                  <br/>
              <br/>
                    Angelwish achieves its mission by ensuring that <b>100% of individual donations are used for program services</b> and that those funds are extended, where possible, to incorporate an educational component giving young people a hands on lesson in philanthropy.
                    <br/>
                    <br/>
                    Angelwish is  a registered 501(c)3 charitable organization in the United States and is  Registered Charity No. 1141090  in the United Kingdom. </p>
        </div>
            <div class="right">
                <img src="/public/images/aw_giving_spree.jpg"/>
            </div>
        </div>

        <div class="pane">
            <h1> About Angelwish</h1>
            <h2> What We Do</h2>

            <ol class="whatwedo">
                <li>
                    <h4>Online Birthday and Holiday Wish Granting </h4>
                    <p>
                        Angelwish pioneered the concept of digital philanthropy, engaging
                        the public to provide critical psychosocial and educational support for children and adolescents living with chronic illnesses all over the world.
                    </p>
                </li>
                <li>
                    <h4>International Giving </h4>
                    <p>
                        Angelwish offers donors the opportunity to support over 9 programs
                        overseas, where monetary contributions have the greatest impact.
                        Angelwish collects these funds from the public and redistributes 100% of the money to the partner organization to use for medical, nutritional, and educational purposes.
                    </p>
                </li>
                <li>
                    <h4>Angelwish Holiday Toy Drive </h4>
                    <p>
                        Angelwish operates the physical distribution of toys donated by the
                        Today Show Charitable Foundation. Though our roots are in online fulfillment of wishes, Angelwish is able to support our core values by reaching more children and encouraging civic engagement as a result of this drive.
                    </p>
                </li>
                <li>
                    <h4>Angelwish Learning to Give Program</h4>
                    <p>
                        Angelwish has developed a curriculum for 3rd and 4th grade school children on Angelwish Giving Sprees at
                        various toy stores or bookstores, talking to them about children living with chronic illnesses all over the country, and then asking them to select toys for those children using our money to pay for the items. Participating children go on to associate giving back with something fun.
                    </p>
                </li>
                <li>
                    <h4>Angelwish Educational Support </h4>
                    <p>
                        Angelwish believes that there is a gap in support to at risk children with chronic illnesses and that our network and services can have an even greater impact on the educational opportunities for children.
                    </p>
                </li>
            </ol>


        </div>



        <div class="pane">
            <h1> About Angelwish</h1>
            <h2> Where We Work</h2>
            
            <div id="map_canvas"></div>

            <div class="region">
                <h3> North America </h3>
                <p>
                    Together, we can make holidays and birthdays something to celebrate for children living with Chronic Illnesses all over the country. Start by selecting a state, city, and care center that means something to you.
                    You can grant a wish through the wishlist or you can make a donation and we'll take school children on an Angelwish Giving Spree to buy birthday presents for the children we support.
                </p>
            </div>

            <div class="region">
              <h3> Internationally</h3>
                <div><span id="internal-source-marker_0.23284705844707787">According to the World Health Organization, the increase in chronic diseases is a global epidemic that has mainly been either ignored or seen as less important than other health issues. This is especially true in developing countries. &nbsp;</span><br />
                  <br />
                  However, the truth is that 80% of all chronic disease deaths occur in low and middle income countries. People from developing countries who develop chronic diseases are also more likely to die from them at a younger age than people in the developed world. <br />
                  <br />
                  This is because chronic diseases and poverty tend to go hand in hand. &nbsp;Impoverished people are usually more vulnerable to these conditions, as they are exposed to more risks and have less access to health services. Chronic diseases also hurt economic development and worsen poverty as people are forced to miss work and pay for medical expenses.</div>
<p>&nbsp;</p>
            </div>

            <div class="region">                
                <h3>&nbsp;</h3>
</div>

            <div class="region">
                <h3>&nbsp;</h3>
            </div>
            <div class="region"> </div>
        </div>


        <div class="pane">
            <h1> About Angelwish</h1>
            <h2> Management </h2>

            <h3> Shimmy Mehta, founder/CEO </h3>
            <p>                Early in his career with a Big Four accounting firm, Shimmy saw the need
                for a simple and rewarding way for busy people like himself to get
                involved in the community. 
He quickly tapped into his network of contacts and mentors to pioneer the concept of digital philanthropy through Angelwish.org,
                spending the greater part of his 9 years at the Firm matching the resources
                of his peers with the needs of the many children who were living with HIV/AIDS.
                <br/>
                <br/>
            In 2006, Shimmy sent his resignation by text message, took the helm of Angelwish in the US, jumped over the pond to start up Angelwish in the UK, and co-founded another nonprofit called "Karma Geek&quot;. In 2009-2010, during what was possibly the worst economic climate to hit nonprofits, he made unorthodox decision to expand services to include children in need who were dealing with Chronic Illnesses. </p>
            <p>&nbsp;</p>
            <h3>&nbsp;</h3>
        <h2> Angelwish Board of Directors (US) </h2>

          <h3> Jim Brinksma, Director</h3>
            <p>
                Dr. Jim Brinksma is the Founding Partner of Visible Arbitrage, an investment and advisory firm that focuses on Angel Investing, Corporate and Nonprofit Mentoring, Proprietary Trading, and Research.  Jim has global experience and has provided a range of services to entrepreneurs, venture capital firms, hedge funds, institutional banks, service providers, equipment manufacturers, and non-profits.<br/>
                <br/>
            </p>

            <br/>
            <br/>
            <h3>Doug Ferrick, Director</h3>
            <p>Doug is the Vice President of Donor Relations; Vice President of Fund Development; and Senior Director of Donor Relations at the United Way for Southeastern                Michigan. Being the Vice President of Donor Relations for United Way for Southeastern Michigan allows Doug to tap into his core passion, bringing communities and people together around a common cause to create lasting change.  By working with donors, volunteers and staff he helps guide their passions by investing in quality of life issues (basic needs, education, financial stability).<br/>
                <br/> With over 20 years of comprehensive nonprofit leadership experience in small to large nonprofits, Doug recently completed his master degree at Case Western Reserve University in Nonprofit Organizations.  By serving on the board at Angelwish along with his daily work in making metro Detroit one of the top five cities to live and work by 2030, he believes that philanthropy plays a pivotal role in improving the social conditions of a neighborhood, a city and even a country.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h3>Jeffrey Zink, Director </h3>
            
            <p>Jeffrey Zink has over 30 years of experience in successfully launching and developing companies, products and services. Equally, he has a proven track record as an outstanding turnaround specialist. He currently is the Managing Director/Partner in the Westchester Business Accelerator. The WBA is a business growth/growth advisory and turnaround resource that assists companies grow to their potential.
            <br/> <br/>
            Jeffrey is responsible for the implementation of the growth plan; advising clients on developing and creating all sales, marketing and operation strategies. <br/> <br/>
            
            His previous endeavor was as a Vice President of YAHOO! Jeffrey started with YAHOO! in 1995 shortly after its IPO launch. He was solely responsible for building the East Coast and Canada operations including sales, strategic alliances, business development, business operations and web broadcasting. During his tenure revenue grew in the eastern region to over $300 million dollars.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h2>US Corporate Advisory Board<br/>
            </h2>
            <h3>Romain Kapadia</h3>
                <p>Romain Kapadia has spent his career focused on building consumer/retail and hospitality businesses. Currently, Mr. Kapadia acts as an advisor to two of India's largest conglomerates spearheading international retail partnerships for the Indian market. From 2001 – 2008, Romain was an award-winning fashion entrepreneur and the recipient of over fifteen industry-accolades and notable press mentions in leading fashion publications. Romain has a BBA in Marketing from the University of Texas at Austin and an MBA from INSEAD/Wharton Business School. He's avid runner having completed 4 marathons and is active with several charities including being on the founding team of the American India Foundation's Young Professional's Chapter.<br/>
                <br/> 
            <h3>Jeffrey Ram</h3>
            <p>Jeff is a Senior Associate at CBRE. He has previously held major positions in several commercial interior  companies. Mr. Ram has previously served on the advisory board for Pipeline  Manager, and has recently resumed his involvement with the Boy Scouts of  America. He holds a BA in English from the University of Albany.<br/>
                <br/></p>

          <p>&nbsp;</p>
          <p><br/>
            <br/>
            
          </p>
        <h2>Angelwish (UK) Board of Trustees</h2>
            <h3>Paul Caseiras</h3>
            <p>
                Born and raised in New York, Paul became interested in helping children,
                especially those from developing countries, when he started his banking
                career working with clients in Emerging Markets. Through his years of
                experience in Latin America and Eastern Europe, Paul witnessed the
                plight of underprivileged  children and has used his contacts in
                these countries to help them as much as possible
                <br/>
                <br/>
Paul&nbsp;is currently a Director of Credit Suisse in charge of Fixed Income 
Prime Brokerage. &nbsp;In this role&nbsp;Paul&nbsp;deals with international investors 
throughout the world and helps them with their fixed income investments. 
Prior to Credit Suisse&nbsp;Paul&nbsp;held positions at Lehman Brothers, Citibank and Renaissance Capital.<br />
              <br />
              Paul&nbsp;has a BS in Computer Engineering from Syracuse University and an MBA from University of Rochester. &nbsp;Paul&nbsp;is married with a daughter and a 
            son on the way. He, his wife Gisele, and daughter Beatriz, recently 
            relocated to New York from London.
            <br/>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h3>Michael Walters</h3>
<p> Born in Scotland, Michael moved to Trinidad when he was just a few days old and has been travelling ever since. After attending school in his native Scotland, Michael went on to study Artificial Intelligence and later Design and Digital Media at Edinburgh University. This equipped him well for his role as freelance web designer, &nbsp;where he used the freedom it afforded to continue travelling. Returning to the UK, Michael took up a role at the London office of Next Jump, a New York headquartered online e-commerce company, where he is currently helping grow the Marketing and Technology team. <br/>
        </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h3>Shimmy Mehta, founder</h3>
            <p>
                Early in his career with a Big Four accounting firm, Shimmy saw the need
                for a simple and rewarding way for busy people like himself to get
                involved in the community. 
He quickly tapped into his network of contacts and mentors to pioneer the concept of digital philanthropy through Angelwish.org,
                spending the greater part of his 9 years at the Firm matching the resources
                of his peers with the needs of the many children who were living with HIV/AIDS.
                  <br/>
                  <br/>
In 2006, Shimmy sent his resignation by text message, took the helm of Angelwish in the US, jumped over the pond to start up Angelwish in the UK, and co-founded another nonprofit called &quot;Karma Geek&quot;. In 2009-2010, during what was possibly the worst economic climate to hit nonprofits, he made unorthodox decision to expand services to include children in need who were dealing with Chronic Illnesses. </p>
        </div>

       <div class="pane">
            <h1> About Angelwish</h1>
            <h2> Annual Reports</h2>
                <p> 
                    Angelwish is committed to providing the public with transparent access to our reports and records. We encourage the use of <a href="http://www.guidestar.org">Guidestar.org</a> as it provides an objective source of IRS verified data. </p>
                <p>&nbsp;</p>
<p>&nbsp;</p>

                <p class="document">
                <a href="/public/images/2010angelwishannualreport.pdf" target="new">
                        2010 Angelwish Annual Report
                        <img src="/public/images/pdficon_large.gif" width="16" height="16" border="0">
                  </a>
                </p>
                <p class="document"><a href="/public/images/2011angelwish990.pdf">2011 Angelwish Form 990</a> <a href="/public/images/2010angelwish990.pdf" target="new"><img src="/public/images/pdficon_large.gif" alt="" width="16" height="16" border="0" /></a></p>
         <p class="document"><a href="/public/images/2011angelwishaudit.pdf">2011 Angelwish Audit</a> <a href="/public/images/2010angelwishaudit.pdf"><img src="/public/images/pdficon_large.gif" alt="" width="16" height="16" border="0" /></a></p>
                <p class="document"><a href="/public/images/angelwish501c3.pdf">501(c)3 Determination Letter</a> <a href="/public/images/angelwish501c3.pdf" target="new"><img src="/public/images/pdficon_large.gif" alt="" width="16" height="16" border="0" /></a></p>
                <p class="document"><a href="/public/images/angelwish1023.pdf">Form 1023 Application for tax exempt status</a> <a href="/public/images/angelwish1023.pdf" target="new"><img src="/public/images/pdficon_large.gif" alt="" width="16" height="16" border="0" /></a></p>
       </div>
        
    </div>
</div>



<script type="text/javascript"
src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript"
src="/public/js/markerclusterer_packed.js">
</script>
<script type="text/javascript">
    <?=$js?>
</script>
<script type="text/javascript">
$(function(){
	$('#about a').addClass('active');
});
</script>



