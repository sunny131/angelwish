<style>
    <?=$css?>
</style>


<div class="logo">
    <a href="/"><img src="/public/images/aw_logo.png" alt="Angelwish"/></a>
</div>

<div class="navigation">
    <ul>
        <li id="grant_a_wish" class="first"><a class="highlight" href="/wishes"> <img src="/public/images/grant_a_wish_btn.png" width="178" height="38" /></a></li>
        <li id="living_with"><a href="/living_with" > Children With Chronic Illnesses </a></li>
        <li id="stories"><a href="/stories"> Stories </a></li>
        <li id="media"><a href="/media"> Media </a></li>
        <li id="newsletter"><a href="/newsletter"> Newsletter </a></li>
        <li id="about"><a href="/about"> About Us </a></li>
    </ul>
</div>



<div style="clear:both;margin-bottom: 5px;"></div>

<script type="text/javascript">
</script>