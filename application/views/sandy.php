<style>
    <?=$css?>
</style>

<?php 
	function vsort($a,$subkey,$sort) {
	foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	$sort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
	}
?>

<div id="living_with_container" class="page">
	<div class="living_with_text" style="width: 50%; padding: 10px; float: left;">
    <h1 align="left">Hurricane Sandy Relief 2013</h1>
<h2 align="left"> It's Not Too Late to Help Families Affected by the Storm</h2>

    <div class="donate">
      <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Make a Quick Donation</b></div>
		<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		<style>
			.hide{display: none;padding:5px;}
		</style>
		<script language="javascript">
			function showhide(val)
			{
			var ge1=document.getElementById('d1');
			var ge2=document.getElementById('d2');
				if(val==-1)
				{
					ge1.style.display="block";
					ge2.style.display="none";
				}
				else
				{
					ge1.style.display="none";
					ge2.style.display="block";
				}
			}

			function validate()
			{
				var valx = document.getElementById('amountbox').value;
				if (valx==-1)
				{
					var other_amount = document.getElementById('otherbox').value;
					if(other_amount > 0){
						document.getElementById('amount').value = document.getElementById('otherbox').value;
					}else{
						alert("Please add a valid amount");
						return false;
					}
				}
				else
				{
					document.getElementById('amount').value  = valx;
				}
				
				$("#d_form,#black_layer").show();
				return false;
				/*document.getElementById('donation_form').submit();*/
			}
			$(document).ready(function(){
				$("#black_layer,#close_icon").click(function(){
					$("#d_form,#black_layer").hide();
				});
			});
		</script>

		<td>	<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
				  <option value="25">$25</option>
				  <option value="50">$50</option>
				  <option value="100">$100</option>
				  <option value="500">$500</option>
				  <option value="1000">$1000</option>
				  <option value="2500">$2500</option>
				  <option value="5000">$5000</option>
				  <option value="-1">Other</option>
				</select><br>
				 
		</td>
		<td width="100px">
			<div id="d1" class="hide">
			<input type="text" id="otherbox" name="otherbox" size="5" value="">
			</div>
			<div id="d2"></div>
		</td>
		<td>
		<!--<form action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
		  <input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks" >
		  <input type="hidden" name="ipnUrl" value="http://angelwish.org/cart/captureresponse" >
		  <input type="hidden" name="processImmediate" value="1" >
		  <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
		  <input type="hidden" name="signatureMethod" value="HmacSHA256" >
		  <input type="hidden" name="collectShippingAddress" value="0" >
		  <input type="hidden" name="isDonationWidget" value="1" >
		  <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
		  <input type="hidden" name="referenceId" value="Hurricane Sandy Relief 2013" >
		  <input type="hidden" name="cobrandingStyle" value="logo" >
		  <input type="hidden" name="immediateReturn" value="1" >
		  <input type="hidden" name="description" value="Hurricane Sandy Relief 2013" >
		  <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
		  <input type="hidden" name="signatureVersion" value="2" >
		  <input type="hidden" name="signature" value="F1A5BuhHCz143ow2L3kHzK1SbeTRsC/8GHuOyqxDjNg=" >
		  <input type="hidden" id='amount' name="amount" size="8"  value="">
		<input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
		</form>-->
		<form method="post" id="donation_form"> 
			<style>
			.form_style{background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #000000;
    border-radius: 10px 10px 10px 10px;
    display: none;
    height: 410px;
    left: 38%;
    position: fixed;
    top: 14%;
    width: 345px;
    z-index: 99999999;}
    .logo_from{ float:left; width:100%;  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAYAAABHLFpgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHdJREFUeNqEkcsNgDAMQ632yiYswx5MywbcOwBCQjL9oBBKAj1UivWs2inGaWZAPoHlAok2Kg1Fo6NR4KpRcY7tQihPvsfmrTZ+2TRnOKTRHVJzRN/8x9GXppHP4fxa1g6eiPU9au2WpoLzCNi3JTKtKWZ9OAUYAGipcMVOJd4FAAAAAElFTkSuQmCC") repeat scroll 0 0 transparent; }
    .logo_from img{ float:left; text-align:center; padding-left:55px;}
			
			</style>
					
		<div id="d_form" class="form_style">
		<div style="float:left; width:100%; position: relative;">
			<div style="position: absolute; right: 0; top: 0;cursor: pointer; width: auto;"><img id="close_icon" src="http://angelwish.org/public/images/close_icon.png" alt="" /></div>
			<div class="logo_from"><img src="http://angelwish.org/public/images/xaw_logo.png.pagespeed.ic.WiUTy-3UkG.png" alt="anglewish"></div>
			<table style="  padding-left: 19px; padding-top: 11px;">
				<tbody><tr>
					<td>First Name</td>
					<td><input type="text" placeholder="First Name" name="fname"></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" placeholder="Last Name" name="lname"></td>
				</tr>
				<tr>
					<td>Email Address </td>
					<td><input type="text" placeholder="Email Address" name="email"></td>
				</tr>
				<tr>
					<td>Address </td>
					<td><textarea name="address"></textarea></td>
				</tr>
				<tr>
					<td>City </td>
					<td><input type="text" placeholder="City" name="city"></td>
				</tr>
				<tr>
					<td>State </td>
					<td><input type="text" placeholder="State" name="state"></td>
				</tr>
				<tr>					<td>Zip Code </td>
					<td><input type="text" placeholder="Zip Code" name="zipcode"></td> 
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				 <tr>
					<td colspan="2" align="center"><input type="submit" style="background: url('http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif') no-repeat scroll 0% 0% transparent; border: medium none; width: 134px; height: 37px; text-indent: -999px;" value="Donate" name="submit"></td>
				</tr>
			</tbody></table>
		</div></div>


			<input type="hidden" id='amount' name="amount" size="8"  value="">
			<input type="image"  onclick="return validate();"  src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
		</form>
		</td>
		</tr>
		</table>
		    </div>
  </div>
	<div style="width:40%;float:left;margin-top:40px;">
	  <p>Hurricane Sandy has taken lives, homes, and certainly childhood memories for both young and old. Angelwish invites you to support the holiday wishes of children and families who have been affected by Hurricane Sandy as this may be the only new toy the get this holiday season.<br /><br /> 
      We are leveraging our network of major hospitals to receive and distribute these gifts as a part of their committment to their communities. As always, 100% of your donations will go to helping the underserved.<br /><br />       
      Select one of the Hospitals or Care Centers below to find out how you can make the holidays brighter or make a quick donation to your left and we'll help those who need it most.  
      </div>
	<div style="clear:both"></div>
	<h1>Select one of the Hospitals or Care Centers Below to Donate</h1>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in New York </h3>
		<ul>
			<?php $location1["New York"] = vsort($location1["New York"],'city',asort) ?>
			<?php foreach($location1["New York"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, New York</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in New Jersey </h3>
		<ul>
			<?php $location2["New Jersey"] = vsort($location2["New Jersey"],'city',asort) ?>
			<?php foreach($location2["New Jersey"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, New Jersey</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in Connecticut </h3>
		<ul>
			<?php $location3["Connecticut"] = vsort($location3["Connecticut"],'city',asort) ?>
			<?php foreach($location3["Connecticut"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, Connecticut</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<div id="New_York" class="sandy_state">
		<h3>Care Centers in Pennsylvania </h3>
		<ul>
			<?php $location4["Pennsylvania"] = vsort($location4["Pennsylvania"],'city',asort) ?>
			<?php foreach($location4["Pennsylvania"] as $item){ ?>
			<li>
				<div class="name"><a href="/carecenter/index/<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></a></div>
				<div class="address"><?php echo $item["city"]; ?>, Pennsylvania</div>
			</li>
			<?php } ?>
		</ul>
	</div>

	
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>

