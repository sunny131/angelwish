<style>
    <?=$css?>
</style>

<div id="nav_bar">
    <div class="navitem" id="about"> <a href="/about"> ABOUT ANGELWISH </a></div>
    <div class="navitem" id="living_with"> <a href="/living_with"> CHILDREN WITH CHRONIC ILLNESSES</a></div>
    <div class="navitem" id="media"><a href="/media">MEDIA</a></div>
    <div class="navitem" id="newsletter"><a href="/newsletter">NEWSLETTER</a></div>
    <div class="navitem" id="contact"><a href="/stories">STORIES</a></div>
    <div class="navitem orange" id="grant"><a href="/grant_a_wish">GRANT A WISH</a></div>
</div>
<div style="clear:both;margin-bottom: 10px;"></div>