<style>
    <?=$css?>
</style>

<div id="Stories_container" class="page">

    <h1> Stories </h1>
    <h2> Your act of giving has never had so much impact.  </h2>
    <p> Take a moment to read stories of  how Angelwish gifts have impacted the lives of children in need with chronic illnesses all over the country. </p>
   <?foreach($stories as $index=>$story){?>
    <div class="story" <?if($index == (count($stories)-1)){?>style="border: none;"<?}?>>
        <img class="open-quotes" alt="" src="/public/images/open_quotes.jpg">
        <div class="testimonial_content">
            <p><?=$story['content']?></p>
         </div>
        <img class="close-quotes" alt="" src="/public/images/close_quotes.jpg">
    </div>
    <?}?>    
</div>
<script type="text/javascript">
$(function(){
	$('#stories a').addClass('active');
});
</script>


