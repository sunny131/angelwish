<style>
    <?=$css?>
</style>

<div id="Stories_container" class="page">

    <h1> Terms & Conditions </h1>
   <div style="min-height: 400px;padding: 10px;">
    <p>Thank you for visiting Angelwish.org (the "Site"). The following terms and conditions (the "Terms of Use") govern your access to and use of the Site. Please read them carefully.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>BY ACCESSING THE SITE OR USING THE INFORMATION, TOOLS, FEATURES, AND FUNCTIONALITY LOCATED ON IT (COLLECTIVELY THE "SERVICE"), YOU ACCEPT AND AGREE TO BE BOUND BY THESE TERMS OF USE, AS THEY MAY BE AMENDED FROM TIME TO TIME.</p>

<p>YOU MAY NOT USE THE SERVICE AND YOU MAY NOT ACCEPT THESE TERMS OF USE IF YOU ARE NOT OF A LEGAL AGE TO FORM A BINDING CONTRACT. BY USING THE SERVICE, YOU REPRESENT THAT YOU HAVE THE CAPACITY TO BE BOUND BY THESE TERMS OF USE.</p>
<p>
&nbsp;</p>
<p>
<strong>1. Dispute Resolution.</strong> In the event of any dispute arising out of these Terms of Use, you and Angelwish each agree to use good faith efforts to resolve our differences amicably. In the event those efforts are unsuccessful, you and Angelwish agree not to commence litigation until attempting to resolve our dispute through mediation.</p>
<p>&nbsp;</p>
<ul style="padding: 10px;">
    <li>
Either you or Angelwish may initiate the mediation process with thirty (30) days' prior written notice to the other.</li>

<li>The dispute shall be submitted to mediation in Rutherford, NJ, U.S.A.</li>

<li>Costs of mediation shall be borne equally by both you and Angelwish.</li>

<li>Mediation of the dispute shall be completed within fifteen (15) days of commencement, unless you and Angelwish agree to extend the time by mutual agreement or unless the mediator declares you and Angelwish to be at an impasse.</li>

<li>Notwithstanding the above, in the event that Angelwish believes that immediate injunctive relief is required to protect its rights, Angelwish may invoke the immediate powers of the appropriate court of law without the requirement to first mediate the dispute.</li>

</ul>
<p>&nbsp;</p>

<p>
You agree that in the event any court action is necessary, these Terms of Use will be governed by the laws of the State of New Jersey, excluding any conflict of laws rules or similar principals, and the applicable laws, regulations and treaties of the United States of America. Any action arising out of any dispute with respect to these Terms of Use shall only be brought in the state or federal courts located in Bergen County, New Jersey. You agree that any claim against Angelwish shall be brought within one (1) year from when the claim arose, and any claims not brought within such period of time shall be deemed waived.</p>
<p>&nbsp;</p>
<p><strong>2. User Information.</strong> In order to access or use some parts of the Site or the Service, you may be required to submit certain personal information. Our information collection and use policies with respect to the Site are set forth in our Privacy Policy [http://www.angelwish.org/privacy], which is incorporated herein by reference for all purposes. When submitting information to us, you agree to provide only true, accurate, current and complete information, and you accept all responsibility for all activities that occur under your account. We reserve the right, in our sole discretion and without notice to you, to terminate your account or to restrict your access to all or part of the Site or the Service for any reason, including, without limitation, for extended periods of inactivity.</p>
<p>&nbsp;</p>
<p><strong>3. Your contributions.</strong> If you submit any information, material or contribution to the Site including, without limitation, any text, graphics, photo, video or audio (in each case a "Contribution"), you shall be solely responsible for your Contribution and the consequences of uploading it in each case. We do not want to receive confidential or proprietary information from you through the Site, and any Contribution sent to us via the Site shall be considered non-confidential and non-proprietary. By submitting any Contribution to the Site, you represent, warrant and undertake that such Contribution:</p>

<p><ul style="padding: 10px;">
<li>(i) is your own original work and that you have the right to make it available to us for all the purposes specified above.</li>

<li>(ii) is not defamatory</li>

<li>(iii) does not infringe any law or the intellectual property rights or other proprietary rights of any third party.
Furthermore, whilst you shall retain all of your ownership rights in any Contribution, by submitting any Contribution to the Site, you grant to us a worldwide, perpetual, royalty-free, non-exclusive, sub-licensable and transferable right and license to use, reproduce, modify, distribute and/or to incorporate such Contribution in other works for any and all commercial and/or non-commercial purposes in any media now known or hereinafter developed, subject only to the privacy restrictions in respect of personally identifiable information which are set out in our Privacy Policy. You also agree to permit any other Site user to access, view, store or reproduce your Contribution for his or her personal, non-commercial use consistent with these Terms of Use. If you cannot or do not wish to grant such rights to us or users of the Site, do not submit your Contribution to the Site.
</li></ul></p>
<p>&nbsp;</p>
<p><strong>4. Right to Monitor and Editorial Control.</strong> We reserve the right, but do not have any obligation, to monitor and/or review user Contributions. While we are not responsible for any user's Contribution, and we expressly disclaim any and all liability with respect to and in connection with any Contribution, we reserve the right to remove any Contribution uploaded by you and/or to suspend or to terminate your access to the whole or any part of the Site without notice if we believe you are in breach of these Terms of Use or applicable law, or for any other reason.</p>
<p>&nbsp;</p>
<p><strong>5. Private or Sensitive Information on Public Forums</strong>. It is important to remember that comments submitted to a forum may be recorded and stored in multiple places, both on the Site and elsewhere on the Internet, which are likely to be accessible for a long time and you have no control over who will read them eventually. It is therefore important that you are careful and selective about the personal information that you disclose about yourself and others, and, in particular, you should not disclose sensitive, proprietary or confidential information in your comments to our public forums.</p>
<p>&nbsp;</p>
<p><strong>6. Rules of Conduct.</strong> While accessing or using the Site, the Service, and the Site Content, in addition to any other supplementary terms, rules and/or guidelines that may be posted, you agree that you shall not: </p>

<p><ul style="padding: 10px;">
<li>impersonate any person or entity, whether actual or fictitious, or misrepresent your affiliation with any other person or entity.</li>

<li>reproduce, duplicate, copy, download, sell, resell, or otherwise exploit the Site, the Service or the Site Content, in whole or in part, for commercial purposes without our prior written consent.</li>

<li>contact any other user of the Site for commercial or promotional purposes, or offer to buy or sell any product or service on or through your activities on the Site without our prior written permission.</li>

<li>alter, edit, delete, remove, fail to display, otherwise change the meaning or appearance of, or repurpose any of the Site Content (or attempt to do any of the foregoing), including, without limitation, any trademarks, trade names, logos, service marks, promotional taglines, or any other proprietary content or proprietary rights notices included therein or thereon.</li>

<li>obtain or attempt to gain unauthorized access to other computer systems, materials, information or any services available on or through the Site through any means, including through means not intentionally made publicly available or provided for through the Site.</li>

<li>engage in spidering, "screen scraping," "database scraping," harvesting of e-mail addresses or other personal information, or any other automatic or unauthorized means of accessing, logging-in or registering on the Site, or obtaining lists of users or other information from or through the Site, including, without limitation, any information residing on any server or database connected to the Site.</li>

<li>use the Site or the Service in any manner that could interrupt, damage, disable, overburden or impair the Site or interfere with any other party's use and enjoyment of the Site or the Service, including, without limitation, sending mass unsolicited messages or "flooding" servers with requests;
circumvent, reverse engineer, decrypt, or otherwise alter or interfere (or attempt, encourage or support any one else's attempt to do any of the foregoing) with the Site, the Service, or any software on the Site.</li>

<li>upload, post, transmit, distribute or otherwise publish to, on or through the Site, any information, content or materials which are false, fraudulent, misleading, unlawful, threatening, abusive, harassing, hateful, racially, ethnically or otherwise objectionable, libelous, defamatory, obscene, vulgar, offensive, incendiary, pornographic, profane, sexually explicit or indecent, including without limitation, any material which encourages conduct that would constitute a criminal offense, give rise to civil liability or otherwise violate any local, state, national or international law.</li>

<li>use the Site, the Service, or the Site Content to, or in any other manner, violate, plagiarize or infringe the rights of third parties, including without limitation, copyright, trademark, trade secret, confidentiality, contract, patent, rights of privacy or rights of publicity or any other proprietary or legal right; upload, post, publish, distribute or otherwise transmit any information or material which constitutes or contains a virus, spyware, or other harmful component, or which contains any embedded links, advertising, chain letters or pyramid schemes of any kind</li>

<li>use the Site, the Service, or any Site Content, in whole or in part, in violation of any applicable law.</li></ul></p>
<p>&nbsp;</p>

<p><strong>7. Sweepstakes, Contests, Events and Other Promotions.</strong> Any sweepstakes, contest, event or other promotion in which you participate, whether made available through the Site or otherwise, by us or our partners, may be governed by specific rules that are separate from these Terms of Use. By participating in any such sweepstakes, contest, event or promotion, you agree to become subject to those rules in addition to, where applicable, these Terms of Use. We urge you to read the applicable rules, if any, which will be linked from the particular activity, and to review any applicable security and privacy statements, which, in addition to these Terms of Use, will govern any information you submit in connection with such activities.</p>
<p>&nbsp;</p>

<p><strong>8. Electronic Communications.</strong> You hereby consent to receive communications from us electronically. You agree that all disclosures, notices, agreements, and other communications you receive from us electronically satisfy any legal requirement for such communications to be in writing. We do not accept any liability or responsibility for emails or other electronic communications that are intercepted, garbled, lost or not received.</p>
<p>&nbsp;</p>

<p><strong>9. Proprietary Rights</strong>. As between you and us, and except for your Contributions (as defined in Section 3 above), we own, solely and exclusively, all rights, title and interest in and to the Site, the Service, all the content (including, but not limited to, all audio, photographs, images, illustrations, renderings, drawings, Webcasts, RSS feeds, podcasts, reports, papers, research, other services, graphics, charts, logos, widgets, gadgets, applets, other distributable applications, other visuals, video and copy), software, code, data and other materials thereon (collectively, the "Site Content"), the look and feel, design and organization of the Site, and the compilation of the Site Content, including but not limited to any copyrights, trademark rights, patent rights and other intellectual property and proprietary rights therein. Your use of the Site or the Service does not grant to you ownership or title of, in or to any Site Content or any other part of the Site or the Service.</p>
<p>&nbsp;</p>

<p><strong>10. Limited License.</strong> The Site Content is provided by Angelwish as a service to Site users. Except as specifically provided herein, none of the Site Content may be copied, reproduced, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, without our prior written permission. Subject to the terms and conditions set forth in these Terms of Use, Angelwish grants you a non-exclusive, non-transferable, limited license to access, view, use and display the Site and Site Content on your computer or other Internet-capable device, provided that you comply fully with these Terms of Use. The Site and Site Content are for your personal and noncommercial use only. Any commercial use, distribution, publishing or exploitation of the Site, or any Site Content, is not allowed unless you have received our prior written permission.</p>
<p>&nbsp;</p>

<p><strong>11. Trademarks.</strong> The trademarks, logos, service marks and trade names that may be displayed on the Site are registered and unregistered trademarks of Angelwish and other persons (collectively, the "Trademarks"), and may not be used unless authorized expressly on the Site, in these Terms of Use, or by the applicable Trademark owner. Nothing contained on the Site should be construed as granting, by implication, estoppel, or otherwise, any license or right to use any Trademark displayed on the Site without our written permission or that of the third party rights holder. Our use of third-party Trademarks does not, in itself, constitute an endorsement of any third party or its products or services.</p>
<p>&nbsp;</p>

<p><strong>12. Copyright Claims.</strong> We respect the intellectual property rights of others, and require that the people who use the Site do the same. If you believe that your work has been copied in a way that constitutes copyright infringement, please send the following information to us:</p>
<p>&nbsp;</p>

<p>(a) your address, telephone number, and email address.</p>

<p>(b) a description of the copyrighted work that you claim has been infringed.</p>

<p>(c) a description of where the alleged infringing material is located.</p>

<p>(d) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law.</p>

<p>(e) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest.</p>

<p>(f) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf.</p>
<p>&nbsp;</p>
Contact information for our Copyright Agent for notice of claims of copyright infringement is as follows:<br />
[Copyright agent]<br />
Attn: [person]<br />
address<br />
Tel:<br />
Email:<br />
<p>&nbsp;</p>
<p>It is our policy to terminate the accounts of users who repeatedly infringe third-party copyrights.</p>
<p>&nbsp;</p>

<p><strong>13. Indemnification.</strong> You agree to defend, indemnify and hold Angelwish, its directors, officers, employees, agents and affiliates harmless from any and all claims, liabilities, damages, costs and expenses, including reasonable attorneys' fees, in any way arising from, related to or in connection with your use of the Site, the Service, or the Site Content; your violation of these Terms of Use; or the posting or transmission of any Contributions on or through the Site by you, including, but not limited to, any third-party claim that any such materials you provide infringe any third party proprietary right.</p>
<p>&nbsp;</p>
<p><strong>14. Item Descriptions, Availability, and Variances.</strong> Descriptions or images of, or references to, items on the Site, including the items identified as gifts for children, do not express or imply our endorsement of such items. In addition, while we strive to accurately price, describe and display items on the Site, there may be mistakes. We cannot and do not guarantee the accuracy or completeness of the information on the Site, and we reserve the right to change or update information and to correct errors, inaccuracies or omissions at any time without prior notice.</p>

<p>A note about wish selection and identification: We learned long ago that asking children to describe their wildest wishes to us can be a recipe for disappointment (both for us and for them). Rather than ask children to identify their wishes, we rely upon our partners, including social workers around the country who work with these families daily, to identify gifts that are perfect and appropriate for children of a specific age that are impacted by chronic illnesses. As we're sure you can appreciate, for privacy reasons we don't disclose on the Site the actual names of the children whose wishes are being granted, nor do the children necessarily know what wish has been identified to them. For many of these children, the simple fact of receiving something is powerful enough.</p>

<p>Although we will make every effort to match your donation to the specific wish you choose to grant, you acknowledge and agree that all donations are made as unrestricted gifts. Specifically, we reserve the right to make substitutions, combine your donation with others, or otherwise reallocate your donation to ensure the efficiency and effectiveness of our charitable giving programs.</p>
<p>&nbsp;</p>
<p><strong>15. Donations, Payment Processing, and Refunds.</strong> You may only donate to Angelwish if, and you hereby represent and warrant that, you are at least 18 years old or the age of majority in your jurisdiction. By donating money through the site, you represent and warrant that any donation you make is legal in your jurisdiction and that you are authorized to use the payment method you have selected.<br />
Each donor agrees and acknowledges that:</p>

<p>(i) all donations are subject to the variance policy set forth in Section14.</p>

<p>(ii) designated donation amounts and all applicable taxes, shipping costs, and processing fees (if any) will be charged to the credit or debit card or other payment method you provide to one of our partners Amazon Payments, PayPal, and Dwolla.</p>

<p>(iii) all donations are final and non-refundable.</p>

<p>(iv) if payment is not received by Angelwish from your credit or debit card issuer or its agents, you agree to pay all amounts due upon demand by Angelwish.</p>
<p>&nbsp;</p>

<p><strong>16. Charities Registration.</strong> Angelwish is a registered 501(c)(3) charitable organization in the United States and is Registered Charity No. 1141090 in the United Kingdom. Donating parties may be eligible for tax advantages. You should consult your tax advisor as to the amount of your donation that is tax deductible.</p>
<p>&nbsp;</p>
<strong>17. Linking to the Site.</strong> You agree that, without our prior express permission: (a) if you include a link from any other web site to the Site, such link shall open in a new browser window and shall link to the home page of the Site; (b) you are not permitted to link directly to any image hosted on the Site, such as using an "in-line" linking method to cause the image hosted by, or on behalf of, us to be displayed on another web site; and (c) you may not link from any other web site to the Site in any manner such that the Site, or any page of the Site, is "framed," surrounded or obfuscated by any third party content, materials or branding. The posting or creation of a link to the Site signifies that you have read the restrictions described herein and agree to abide by them. We may, in our sole discretion, insist that any link to the Site be discontinued, and we may revoke your right to link to the Site from any other web site at any time upon written notice to you.
<p>&nbsp;</p>

<strong>18. Third Party Web Sites.</strong> The Site may include links to other sites that we think you might find interesting. Such other sites are not under our control and therefore we have not reviewed them and we are not responsible for their content. You click through at your own risk, and Angelwish makes no representation or warranties about the content, completeness or accuracy of the sites linked to this site. A link to the site does not imply our endorsement or approval of its content or source.
<p>&nbsp;</p>

<p><strong>19. DISCLAIMER OF WARRANTIES.</strong> THE SITE CONTENT AND ALL SERVICES AND PRODUCTS ASSOCIATED WITH THE SITE OR PROVIDED THROUGH THE SERVICE (WHETHER OR NOT SPONSORED) ARE PROVIDED TO YOU ON AN "AS IS" AND "AS AVAILABLE" BASIS. ANGELWISH MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SITE OR THE SERVICE. YOU EXPRESSLY AGREE THAT YOUR USE OF THE SERVICE IS AT YOUR SOLE RISK. <br />
ANGELWISH MAKES NO REPRESENTATIONS, WARRANTIES OR GUARANTEES, EXPRESS OR IMPLIED, REGARDING THE ACCURACY, RELIABILITY OR COMPLETENESS OF THE SITE OR THE SERVICE (WHETHER OR NOT SPONSORED), AND EXPRESSLY DISCLAIMS ANY WARRANTIES OF NON-INFRINGEMENT OR FITNESS FOR A PARTICULAR PURPOSE. ANGELWISH MAKES NO REPRESENTATION, WARRANTY OR GUARANTEE THAT THE CONTENT THAT MAY BE AVAILABLE THROUGH THE SERVICE IS FREE OF INFECTION FROM ANY VIRUSES OR OTHER CODE OR COMPUTER PROGRAMMING ROUTINES THAT CONTAIN CONTAMINATING OR DESTRUCTIVE PROPERTIES OR THAT ARE INTENDED TO DAMAGE, SURREPTITIOUSLY INTERCEPT OR EXPROPRIATE ANY SYSTEM, DATA OR PERSONAL INFORMATION. <br />
WITHOUT LIMITATION OF THE ABOVE IN THIS SECTION, WE AND OUR AFFILIATES, SUPPLIERS AND LICENSORS MAKE NO WARRANTIES OR REPRESENTATIONS REGARDING ANY PRODUCTS OR SERVICES PROVIDED VIA THE SITE, AND HEREBY DISCLAIM, AND YOU HEREBY WAIVE, ANY AND ALL WARRANTIES AND REPRESENTATIONS MADE IN PRODUCTS OR SERVICES LITERATURE, FREQUENTLY ASKED QUESTIONS DOCUMENTS AND OTHERWISE ON THE SITE OR IN CORRESPONDENCE WITH US OR OUR AGENTS. ANY PRODUCTS AND SERVICES PROVIDED VIA THE SITE ARE PROVIDED BY US (OR OUR LICENSORS OR THIRD PARTY PROVIDERS OR SUPPLIERS) "AS IS", EXCEPT TO THE EXTENT, IF AT ALL, OTHERWISE SET FORTH IN A LICENSE OR SALE AGREEMENT SEPARATELY ENTERED INTO IN WRITING BETWEEN YOU AND US OR THE LICENSOR OR SUPPLIER.</p>
<p>&nbsp;</p>

<p><strong>20. LIMITATION OF LIABILITY.</strong> IN NO EVENT, INCLUDING BUT NOT LIMITED TO NEGLIGENCE, SHALL WE, OR ANY OF OUR DIRECTORS, OFFICERS, EMPLOYEES, AGENTS OR CONTENT OR SERVICE PROVIDERS (COLLECTIVELY, THE "PROTECTED ENTITIES") BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES ARISING FROM, OR DIRECTLY OR INDIRECTLY RELATED TO, THE USE OF, OR THE INABILITY TO USE, THE SITE OR THE SERVICE, YOUR PROVISION OF INFORMATION OR CONTENT VIA THE SITE, LOST BUSINESS OR LOST SALES, EVEN IF SUCH PROTECTED ENTITY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. IN NO EVENT SHALL THE PROTECTED ENTITIES BE RESPONSIBLE OR LIABLE FOR OR IN CONNECTION WITH ANY DISPUTE BETWEEN OR AMONGST USERS OF THE SITE OR THE SERVICE, OR IN CONNECTION WITH ANY CONTENT POSTED, TRANSMITTED, EXCHANGED OR RECEIVED BY OR ON BEHALF OF ANY USER OR OTHER PERSON ON OR THROUGH THE SITE. YOU UNDERSTAND AND AGREE THAT <br />

(I) THE MUTUAL AGREEMENTS MADE IN THIS SECTION REFLECT A REASONABLE ALLOCATION OF RISK, AND<br />

(II) THE PARTIES HERETO WOULD NOT HAVE ENTERED INTO THESE TERMS OF USE WITHOUT THESE LIMITATIONS ON LIABILITY.<br />
<br />
<strong>21. Jurisdictional Issues.</strong> This site has been designed to comply with the laws of the State of New Jersey and of the United States. We do not represent that materials on the Site are appropriate for use in other locations. Persons who choose to access the Site from other locations do so on their own initiative, and are responsible for compliance with local laws, if and to the extent local laws are applicable.<br />
<strong>22. Termination.</strong> We may terminate, change, suspend or discontinue any aspect of the Site or the Service at any time. We may restrict, suspend or terminate your access to the Site, the Service, or any Site Content if we believe you are in breach of these Terms of Use or applicable law, or for any other reason without notice or liability.
<br />
<strong>23. Contacting us.</strong> You can contact us by emailing us or by writing to us at the following address:</p>
<p>&nbsp;</p>
Angelwish<br />
PO Box 186<br />
Rutherford, NJ 07070<br />
<br />
terms@angelwish.org<br />
<br /><br />
Effective Date: May 15, 2014</div>
</div>



