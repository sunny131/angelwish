<style>
    <?=$css?>
</style>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=309252795879563";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<meta property="og:image" content="http://angelwish.org/public/images/companylogos/awlogored.png" />
<div id="thank_you" class="page">
    <div id="slideshow-container">
        <div class="text">
        	<?php 
			if(isset($_GET['st']) && ($_GET['st']=='Completed' || $_GET['st']=='Pending')){
		?>
            <h1>Thank You for Your Donation!</h1>
            
            <p>Angelwish thanks you for helping our children and families who have been affected by Hurricane Sandy. We will email you a receipt for tax purposes as soon as your donation is processed.<br/>
              <br/>
It has been a difficult few weeks for many on the East Coast. Many have lost people they love, homes where they lived, their personal property, and certainly, a sense of security for them and their families. Your support today will help give them the immediate boost that will carry them as they rebuild their lives.<br/>
              <br/>
Please help us help more families. Share this experience with your friends through email, facebook, smoke signals...<br/>
              <br/>

          Thank you,
            <br />
              The Angelwish Team<br/>
              <br/>
            <?php }else if(isset($_GET['status']) && ($_GET['status']=='Completed')){?>
			<h1>Thank You for Your Donation!</h1>
            
            <p>Angelwish thanks you for helping our children and families who have been affected by Hurricane Sandy. We will email you a receipt for tax purposes as soon as your donation is processed.<br/>
              <br/>
It has been a difficult few weeks for many on the East Coast. Many have lost people they love, homes where they lived, their personal property, and certainly, a sense of security for them and their families. Your support today will help give them the immediate boost that will carry them as they rebuild their lives.<br/>
              <br/>
Please help us help more families. Share this experience with your friends through email, facebook, smoke signals...<br/>
              <br/>

          Thank you,
            <br />
              The Angelwish Team<br/>
              <br/>
	<?php	}else{
            	echo "<h1>There is some problem in the transaction.Please try again.</h1>";
            }?>
      </div>
        <div class="image">
            <img src="/public/images/smiling-girl.png"/>
      </div>
    </div>
    <div class="bottom">
        <h3>Other ways to help</h3>
        <p>
            Please join our groups on Facebook and Twitter and help spread the word.
        </p>
        <br class="clear"/>
     <br class="clear"/>
        <div class="facebook">
            <div style="padding-bottom: 15px;">
                <div id="fb-root"></div>
                <script src="https://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                <fb:like href="http://www.angelwish.org" send="true" width="140" layout="button_count" show_faces="false" font="arial"></fb:like>
            </div>
            <!--<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fangelwish&amp;width=400&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=true&amp;header=true&amp;height=500" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:500px;" allowTransparency="true"></iframe>-->
        </div>
        <div class="facebook" style="margin-left: 70px;">
            <div style="padding-bottom: 15px;">
                <!--<div class="fb-share-button" data-href="http://angelwish.org" data-type="button"></div>-->
                <a class="btn" target="_blank" href="http://www.facebook.com/sharer.php?s=100&amp;p[url]=http://angelwish.org/&amp;p[images][0]=http://angelwish.org/public/images/companylogos/awlogored.png">share on facebook</a>
            </div>
          
        </div>
        <div class="twitter">
            <div style="width: 220px;padding-bottom: 15px;">
                <iframe allowtransparency="true" frameborder="0" scrolling="no"
                  src="http://platform.twitter.com/widgets/follow_button.html?screen_name=angelwish"
                  style="width:220px; height:20px;"></iframe>
            </div>
            <script src="http://widgets.twimg.com/j/2/widget.js"></script>
            <script>
            new TWTR.Widget({
              version: 2,
              type: 'profile',
              rpp: 6,
              interval: 6000,
              width: 400,
              height: 400,
              theme: {
                shell: {
                  background: '#ffffff',
                  color: '#333333'
                },
                tweets: {
                  background: '#ffffff',
                  color: '#333333',
                  links: '#2898e2'
                }
              },
              features: {
                scrollbar: false,
                loop: false,
                live: true,
                hashtags: true,
                timestamp: true,
                avatars: true,
                behavior: 'all'
              }
            }).render().setUser('angelwish').start();
            </script>
        </div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    <?=$js?>
</script>
