<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text">
    <h1>2013 Year End Appeal</h1>
    <h2> Help Kickstart Angelwish's 2014 with a 2013 Tax Deduction</h2>
	<p style="float: left; width: 500px; padding: 0px 20px 0px 0px;">
	<iframe src="//player.vimeo.com/video/54878553" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
	</iframe>
	</p>
    <div class="donate" style="float:left">
      <div style="width: 250px; padding: 10px; float: left;color:#FCA230"><b>Make a Year End Gift for the Future!</b></div>
	<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
	<style>
		.hide{display: none;padding:5px;}
	</style>
	<script language="javascript">
		var price=10;
		function showhide(val)
		{
		
		var ge3=document.getElementById('monthbox');
		var ge4=document.getElementById('quarterlybox');
		var ge5=document.getElementById('yearbox');
            
            if(val=='month')
            {
				$("#payment_fre").val('1 month')
				$("#amount_d").val("USD 15");
                ge3.style.display="block";
                ge4.style.display="none";
                ge5.style.display="none";
            }
            else if(val=='quarterly')
            {
				$("#payment_fre").val('3 month')
				$("#amount_d").val("USD 25");
				ge4.style.display="block";
                ge3.style.display="none";                
                ge5.style.display="none";
            }
            else if(val=='year')
            {
				$("#payment_fre").val('1 year')
				$("#amount_d").val("USD 50");
                ge3.style.display="none";
                ge4.style.display="none";
                ge5.style.display="block";
            }
            else 
            {
				$("#payment_fre").val('1 month')
				$("#amount_d").val("USD 15");
				ge3.style.display="block";
                ge4.style.display="none";
                ge5.style.display="none";
		    }	
		}

		function validate()
		{
			alert(price)
			var valx=price;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount_d').value ="USD " + document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount_d').value  = "USD "+valx;
			}
			/*$("#d_form,#black_layer").show();
				
			document.getElementById('donation_form').submit();*/
			if(confirm("Sure?"))
			return true;
			else
			return false;
		}
		$(document).ready(function(){
				$("#black_layer,#close_icon").click(function(){
					$("#d_form,#black_layer").hide();
				});
			});
			
		function getvalue(val){
			var ge1=document.getElementById('d1');
			var ge2=document.getElementById('d2');
			if(val==-1)
			{
				ge1.style.display="block";
				ge2.style.display="none";
			}
			else
			{
				ge1.style.display="none";
				ge2.style.display="block";
				$("#amount_d").val("USD "+val);
				
			}
			price=val;
		}
	</script>
	<td>	<select id="recurringbox" name="recurringbox" onchange="showhide(this.value)">
			  <option value="month">monthly</option>
              <option value="quarterly">quarterly</option>
              <option value="year">annually</option> 
			</select>
	</td>

	<td>	<select id="monthbox" name="monthbox" onchange="getvalue(this.value)">
			  <option selected="selected" value="15">$15</option>
              <option value="25">$25</option>
			  <option value="50">$50</option>
			  <option value="100">$100</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td>	<select id="quarterlybox" name="quarterlyhbox" onchange="getvalue(this.value)" style="display: none;">
			   <option selected="selected"  value="25">$25</option>
              <option value="50">$50</option>
			  <option value="100">$100</option>
			  <option value="250">$250</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td>	<select id="yearbox" name="yearbox" onchange="getvalue(this.value)" style="display: none;">
			 <option selected="selected" value="50">$50</option>
              <option value="100">$100</option>
			  <option value="250">$250</option>
			  <option value="500">$500</option>
			  <option value="1000">$1000</option>
			  <option value="-1">Other</option>
			</select>
	</td>
	<td width="100px">
        <div id="d1" class="hide">
		<input type="text" id="otherbox" name="otherbox" size="5" value="">
		</div>
		<div id="d2"></div>
    </td>
    <td>
        <form action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
			<input type="hidden" name="immediateReturn" value="1" >
			<input type="hidden" name="collectShippingAddress" value="0" >
			<input type="hidden" name="signatureVersion" value="2" >
			<input type="hidden" name="signatureMethod" value="HmacSHA256" >
			<input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
			<input type="hidden" name="referenceId" value="Angelwish Year End Donation" >
			<input type="hidden" name="signature" value="X+neiIUfRE0Op4qbwrDvgF92lcQGfny/HxSdpk8sRmI=" >
			<input type="hidden" name="isDonationWidget" value="1" >
			<input type="hidden" name="description" value="Angelwish Year End Donation" >
			<input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
			<input type="hidden" name="returnUrl" value="http://www.angelwish.org/thanks/" >
			<input type="hidden" name="processImmediate" value="1" >
			<input type="hidden" name="cobrandingStyle" value="logo" >
			<input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
			<input type="hidden" name="ipnUrl" value="http://gothamangel.org/cart/captureresponse" >
			<input type="text" name="amount"  id="amount_d" value="10" >
			<input type="text" name="recurringFrequency" value="3 month" id="payment_fre" >
			<input type="hidden" name="subscriptionPeriod" value="12 month" >
			<input type="image"  onclick="return validate();"  src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_medium_paynow_withmsg_whitebg.gif" border="0">
</form>
    </td>
	</tr>
	</table>
    </div>

<p style=" padding-right: 10px; width: 930px;">Did you know that Angelwish receives 80% of our operational support in the 4th Quarter? <br/> 
  <br/>
For over 14 years, we have bootstrapped our operations, tapping our reserves in the bad times and even building up a little credit card debt when the kids and families needed more. Your support has enabled the organization to focus on the long term. <br/> 
  <br/>We have big plans ahead for 2014, including a new board of directors, a major push for our Financial Literacy course, and an even more streamlined process for wishgranting. This is a classic case of Angelwish being a sound investment that, with the proper TLC, will pay dividends in the form of healther children who will go on to do amazing things because they had positive experiences at critical points in their lives. <br/> 
  <br/>Please join us. It will be one of the most rewarding experiences of your life. <br/> 
  <br/>
  Happy New Year!</p>
 
<!--<img src="/public/images/aw_giving_spree.jpg" class="kid"/>-->

    </div>
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>
