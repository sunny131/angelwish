<style>
    <?=$css?>
</style>


<div class="footer_box">
    <h4> &#169; 2012 Angelwish</h4>
</div>

<div class="footer_box">
    <ul>
        <li><a href="/wishes"> Grant A Wish </a></li>
        <li><a href="/living_with"> Children With Chronic Illnesses </a></li>
        <li><a href="/stories"> Stories </a></li>
        <li><a href="/media"> Media </a></li>
        <li><a href="/newsletter"> Newsletter </a></li>
    </ul>
</div>

<div class="footer_box">
    <h4><a href="/index.php/about">About Us</a></h4>
    <ul>
        <li> <a href="/about#mission">Mission Statement </a> </li>
        <li> <a href="/about#whatwedo">What We Do </a> </li>
        <li> <a href="/about#wherewework">Where We Work</a> </li>
        <li> <a href="/about#board">Board Members</a> </li>
        <li> <a href="/about#annualreports">Annual Reports</a> </li>
    </ul>
</div>

<div class="footer_box">
    <h4><a href="/contact">Contact Us</a></h4>
    <a href="http://www.facebook.com/angelwish" target="_blank" title="Follow us on Facebook">
        <img src="/public/images/facebook-24x24.png" alt="Follow us on Facebook"/>
    </a>
    <a href="http://twitter.com/angelwish" target="_blank" title="Follow us on Twitter">
        <img src="/public/images/Twitter-icon-24.png" alt="Follow us on Twitter" />
    </a>
    <a href="http://vimeo.com/angelwish/videos/sort:likes" target="_blank" title="Follow us on Vimeo">
        <img src="/public/images/vimeo-24x24.png" alt="Follow us on Vimeo" />
    </a>
    <div class="moreinfo">
        <i><a href="/contact">more information <small>>></small></a></i>
    </div>
</div>
