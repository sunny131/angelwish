<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="user-scalable=no" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 400px; width: 600px;}
</style>
</head>
<body>
  <div id="map_canvas"></div>



  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"
        type="text/javascript"></script>
<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript"
    src="/public/js/markerclusterer_packed.js">
</script>
<script type="text/javascript">
  $(document).ready(function(){
    var latlng = new google.maps.LatLng(40.813817, -74.10620900000004);

    var myOptions = {
      zoom: 2,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.TERRAIN
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);

    var styles = [{
        url: '/public/images/map_cluster_icon.png',
        height: 45,
        width: 45,
        opt_anchor: [22, 22],
        opt_textColor: '#ff00ff',
        opt_textSize: 11
      }];

    var markers = [];
    $.getJSON('/googlemaps/getmapdata',function(data){
        for (var i = 0; i < data.length; i++) {
          var latLng = new google.maps.LatLng(parseFloat(data[i].latitude),
              parseFloat(data[i].longitude));

          var note = '<div style="font-family:Arial;font-size:14px;padding:5px 2px;">';
          note += '<div style="font-weight:bold;"><a href="/carecenter/index/' + data[i].id + '">' + data[i].name + '</a></div>';
          note += '<div style="font-style:italic;color:#777;font-size:12px;">' + data[i].city +', '+ data[i].country + '</div>';          
          note += '<div style="font-style:italic;font-size:12px;margin-top:5px;">' + data[i].description + '</div>';
          note += '</div>';
          var marker = add_marker(latLng,note);


          markers.push(marker);
        }
        var markerClusterer = new MarkerClusterer(map, markers,{styles:null});
    });

    var infowindow = new google.maps.InfoWindow({
        content: ''
    });

    function add_marker(latLng,note){
        var marker  = new google.maps.Marker({'position': latLng});
        marker.note = note;
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.content = marker.note;
            infowindow.open(map,marker);
        });
        return marker;
    }
  });
  
  
   
    
</script>
</body>
</html>