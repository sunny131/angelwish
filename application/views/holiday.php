<style>
    <?=$css?>
    .form_style{background: none repeat scroll 0 0 #FFFFFF;
    border: 10px solid #000000;
    border-radius: 10px 10px 10px 10px;
    display: none;
    height: 410px;
    left: 38%;
    position: fixed;
    top: 14%;
    width: 345px;
    z-index: 99999999;}
    .logo_from{ float:left; width:100%;  background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABkCAYAAABHLFpgAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHdJREFUeNqEkcsNgDAMQ632yiYswx5MywbcOwBCQjL9oBBKAj1UivWs2inGaWZAPoHlAok2Kg1Fo6NR4KpRcY7tQihPvsfmrTZ+2TRnOKTRHVJzRN/8x9GXppHP4fxa1g6eiPU9au2WpoLzCNi3JTKtKWZ9OAUYAGipcMVOJd4FAAAAAElFTkSuQmCC") repeat scroll 0 0 transparent; }
    .logo_from img{ float:left; text-align:center; padding-left:55px;}
    .hide{display: none;padding:5px;}
</style>

<div id="living_with_container" class="page">
	<div class="living_with_text">
		<h1>2013 Box of Joy Appeal</h1>
		<h2> Help Save the Holidays for Children with Chronic Illnesses</h2>
		<div style="float:left">
			<!--<iframe src="//player.vimeo.com/video/54878553" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
			</iframe>-->
			<img src="http://angelwish.org/images/wishbox.jpg" width=500>
		</div>
		<div class="donate" style="float: left; margin-left: 20px;">
			<div style="width: 250px; padding: 10px; float: left;color:#FCA230">
				<b>Please Support the Angelwish Box of Joy Appeal</b>
			</div>
			<table class="table" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				 
				<script language="javascript">
		function showhide(val)
		{
		var ge1=document.getElementById('d1');
		var ge2=document.getElementById('d2');
            if(val==-1)
            {
                ge1.style.display="block";
                ge2.style.display="none";
            }
            else
            {
                ge1.style.display="none";
                ge2.style.display="block";
            }
		}
		function validate()
		{
			var valx = document.getElementById('amountbox').value;
			if (valx==-1)
			{
                var other_amount = document.getElementById('otherbox').value;
                if(other_amount > 0){
                    document.getElementById('amount').value = document.getElementById('otherbox').value;
                }else{
                    alert("Please add a valid amount");
                    return false;
                }
			}
			else
			{
				document.getElementById('amount').value  = valx;
			}
			$("#d_form,#black_layer").show();
				return false;
			/*document.getElementById('donation_form').submit();*/
		}
		$(document).ready(function(){
				$("#black_layer,#close_icon").click(function(){
					$("#d_form,#black_layer").hide();
				});
			});
	</script>
				<td>
					<select id="amountbox" name="amountbox" onchange="showhide(this.value)">
						<option value="50">$50</option>
						<option value="100">$100</option>
						<option selected="selected" value="500">$500</option>
						<option value="1000">$1,000</option>
						<option value="1250">$1,250</option>
						<option value="2500">$2,500</option>
						<option value="-1">Other</option>
					</select>
				</td>
				<td width="100px">
					<div id="d1" class="hide">
						<input type="text" id="otherbox" name="otherbox" size="5" value="">
					</div>
					<div id="d2">
					</div>
				</td>
				<td>
					<form method="post" id="donation_form">
					 	<div id="d_form" class="form_style">
							<div style="float:left; width:100%; position: relative;">
								<div style="position: absolute; right: 0; top: 0;cursor: pointer; width: auto;">
									<img id="close_icon" src="http://angelwish.org/public/images/close_icon.png" alt=""/>
								</div>
								<div class="logo_from">
									<img src="http://angelwish.org/public/images/xaw_logo.png.pagespeed.ic.WiUTy-3UkG.png" alt="anglewish">
								</div>
								<table style=" padding-left: 19px; padding-top: 11px;">
								<tbody>
								<tr>
									<td>
										First Name
									</td>
									<td>
										<input type="text" placeholder="First Name" name="fname">
									</td>
								</tr>
								<tr>
									<td>
										Last Name
									</td>
									<td>
										<input type="text" placeholder="Last Name" name="lname">
									</td>
								</tr>
								<tr>
									<td>
										Email Address
									</td>
									<td>
										<input type="text" placeholder="Email Address" name="email">
									</td>
								</tr>
								<tr>
									<td>
										Address
									</td>
									<td>
										<textarea name="address"></textarea>
									</td>
								</tr>
								<tr>
									<td>
										City
									</td>
									<td>
										<input type="text" placeholder="City" name="city">
									</td>
								</tr>
								<tr>
									<td>
										State
									</td>
									<td>
										<input type="text" placeholder="State" name="state">
									</td>
								</tr>
								<tr>
									<td>
										Zip Code
									</td>
									<td>
										<input type="text" placeholder="Zip Code" name="zipcode">
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;
										
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<input type="submit" style="background: url('http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif') no-repeat scroll 0% 0% transparent; border: medium none; width: 134px; height: 37px; text-indent: -999px;" value="Donate" name="submit">
									</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
						<input type="hidden" id='amount' name="amount" size="8" value="">
						<input type="image" onclick="return validate();" src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_small_donate_withmsg_whitebg.gif" border="0">
					</form>
				</td>
			</tr>
			</table>
		</div>
		<p style="text-align: justify; float: right; padding-left: 10px; width:400px"><br/>
      For almost 15 years, Angelwish has been helping donors just like you stretch small contributions into amazing gifts that touch the lives of families and children who are dealing with illnesses.<br/>
		  <br/>  This year we need a Holiday Miracle to deliver over 65 Holiday boxes filled with $1,000 worth of gifts to hospitals all over the country.<br/>
		  <br/>
		  <strong>Will you help sponsor an Angelwish Box of Joy for $500?</strong><br/>
		  <br/>
		  If you or your company are interested in rolling up your sleeves and want to discuss additional ways to get involved, please contact <a href="mailto:shimmy@angelwish.org">Shimmy Mehta</a>.<br/><br/>
		  Happy Holidays!	  </p>
  </div>
	<?=modules::run('Whatwedo')?>
</div>
<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>