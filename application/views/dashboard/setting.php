<style>
tr td,te th {
		border-bottom:1px #1E4661 solid;
}
</style>
<div style="min-height:400px; margin-top:20px">
<table align="center" width ="75%" cellspacing="5" cellpadding=5>
	<tr>
		<th>Setting</th>
		<th>Value</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	<?php foreach ($settings as $setting){  ?>
	<tr>
		<td><?php echo ucfirst($setting->settingKey) ?></td>
		<td><?php echo $setting->settingValue ?></td>
		<td><?php echo ($setting->status=='Y')?'Active':'Deactive'?></td>
		<td><A href="/setting/edit/<?php echo $setting->id; ?>">Edit</A></td>
	</tr>
	<?php } ?>
</table>
</div>
