<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        ['Company Name', 'Total Transaction'],
         <?php $Angle=0;
			foreach($companies_orders as $companies_order){
			 if(!empty($companies_order['name'])){
				echo "['". addslashes($companies_order['name'])."',".$companies_order['trans']."],";
			 }else{
				$Angle=$Angle+$companies_order['trans'];
			 } 
			}
			echo "['Angel Wish',".$Angle."],"; 
			 ?> 
			
        ]);

        var options = {
          title: 'Transaction by Companies Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
      
      google.setOnLoadCallback(drawChart1);
      function drawChart1() {
        var data = google.visualization.arrayToDataTable([
        ['Care Center', 'Transactions'],
        ['US (0-499)',<?php echo $total_us_trans?>],
        ['UK (500-699)',<?php echo $total_uk_trans?>],
        ['International (700 and above)',<?php echo $total_internation_trans?>],
         
          
        ]);

        var options = {
          title: 'Breakdown by Geography'
        };

        var chart = new google.visualization.PieChart(document.getElementById('geo'));
        chart.draw(data, options);
      }
    
    </script>
    
<style>
	table{
		border:2px #000 solid;
	}
	table td,th{
		border-bottom:2px #000 solid;
		padding-bottom:10px;
		border-left:1px #000 dashed;
	}
	table td.first, th.first{
		border-left:none;
	}
	.left{
		text-align:left;
		float:none !important;
	}
	.center,th,td{
		text-align:center;
	}
	.sub_div{
		float: left; width: 250px;border:1px solid;margin:2px;padding:2px; background:white;
	}
	.sub_div1{
		float: left; width: 323px; margin:2px;padding:2px;   
	}
	#contents{
		width:1005px !important;
	}
	p,h1{ 
		text-align:center; 
	}
	h1{ 
		margin: 0 0 10px 0 !important;
	}
	p{ 
		padding-top:30px; font-size:20px;
	}
	
	table{
		width:1005px;
	}
	.form_style{
		float: left; 
		border: 1px solid rgb(0, 0, 0); 
		padding: 10px 5px; 
		margin: 10px 0px
	}
	
	</style> 
	<h1>Dashboard Summary</h1>
	<form method="post">
		<div class="form_style">
	
			<div class="sub_div1">
					Date From  : 
					<input type="text" placeholder="2013-11-17" name="date_from" value="<?php echo (isset($_POST['date_from'])?$_POST['date_from']:'') ?>"> 
			</div>
			<div class="sub_div1">
				Date To  : 
				<input type="text" placeholder="2013-12-07" name="date_to" value="<?php echo (isset($_POST['date_to'])?$_POST['date_to']:'') ?>"> 
			</div>
			<div class="sub_div1">
				<input type="submit"  name="sub" value="Get Record">
			</div>
		</div>
	</form>
<div style="float:left;  margin-left: 100px;">	
	<div class="sub_div">
		<p >Total Wishes</p><br>
		<h1><?php echo $total_wish; ?></h1>
	</div>
	
	<div class="sub_div">
		<p>Total Wish Value</p><br>
		<h1>$<?php echo number_format($total_amount,'2'); ?></h1>
	</div>
	
	<div class="sub_div">
		<p>Total Quick Donation</p><br>
		<h1>$<?php echo number_format($total_quick_donation,2); ?></h1>
	</div>
</div>
<div style="clear:both"></div>
<div style="float:left; margin-left: 100px;">
	<div class="sub_div">
		<p># of Donors</p><br>
		<h1><?php echo $totalusers ?></h1>
	</div>
	
	<div class="sub_div">
		<p> % Giving optional Donation</p><br>
		<h1>%<?php echo  number_format($percent_optional_donation,2); ?></h1>
	</div>
	
	<div class="sub_div">
		<p>Total Optional Donation</p><br>
		<h1>$<?php echo number_format($total_optional_donation,2); ; ?></h1>
	</div>
</div>
<div style="clear:both"></div>
<!--
<div style="float:left">
	<div class="sub_div" style="border: medium none; background: none repeat scroll 0% 0% transparent; width: 322px;">
		 &nbsp;
	</div>
	<div class="sub_div" >
		<p># of Center</p><br>
		<h1><?php echo $care_center['care'].'/'.$care_center['total_care'].'<br>(%'.number_format($care_center['percentage'],2).')';
		 ?></h1>
	</div>
	
</div>
<div style="clear:both"></div>
	<div style="float:left" >
		<div id="geo" style="width: 480px; height: 300px;float:right"></div>
	</div>
	<div style="float:left;">
		<div id="piechart" style="width: 480px; height: 300px; float:left"></div>
	</div>
<div style="clear:both"></div>
-->

<div style="float:; margin-left: 100px;">
	<div class="sub_div">
		<div id="geo" style="width: 320px; height: 300px;float:left"></div>
	</div>
	<div class="sub_div" style=" height: 300px; " >
		<p># of Center</p><br>
		<h1><?php echo $care_center['care'].'/'.$care_center['total_care'].'<br>(%'.number_format($care_center['percentage'],2).')';
		 ?></h1>
	</div>
	<div class="sub_div">
		<div id="piechart" style="width: 320px; height: 300px; float:right"></div>
	</div>
</div> 
<div style="clear:both"></div>




<table>
	<tr>
		<td>Select User</td>
		<td><select onchange="window.location.href='http://angelwish.org/dashboard/usertransactionbyemail/'+this.value">
			<?php foreach($users as $user){ ?>
			<option value="<?php echo base64_encode($user->email) ?>"><?php echo $user->email;?></option>
			<?php }  ?>
		</select></td>
		<td>Select Company</td>  
		<td><select onchange="window.location.href='http://angelwish.org/dashboard/getdetailofcomapny/'+this.value">
			<?php foreach($companies_name as $companies){ ?>
			<option value="<?php echo $companies['pk_company_id'] ?>"><?php echo $companies['name'];?></option>
			<?php }  ?>
		</select></td>
		
	</tr>
</table>

<!--
<table border='0' cellspacing="0" cellpadding="5" width="100%">
	
	<th class="first">Wish Id</th>
	<th>Image</th>
	<th>Book Name</th>
	<th>Price</th>
	
<?php  /*   
foreach($wishes as $wish){
 ?>
<tr>
	<td class="first"><?php echo $wish['id'];?></td>
	<td><img src="<?php echo $wish['image'];?>" width="100"></td>
	<!--<td><?php echo $wish['childName'];?></td>
	<td><?php echo $wish['age'];?></td>
	<td><?php echo $wish['state'];?></td>
	<td><?php echo $wish['disease'];?></td>
	<td><?php echo $wish['poverty'];?></td>
	<td class="left"><?php echo $wish['bookName'];?></td>
	<td>$<?php echo $wish['price'];?></td>
</tr>

 <?php } */  ?>
</table>-->


