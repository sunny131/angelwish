<style>
    <?=$css?>
</style>


<div id="living_with_container" class="page">
    <div class="living_with_text"><h1><img src="http://www.angelwish.org/public/images/angelwish-logo.png" width="117" height="40" alt="Angelwish logo" /></h1>
      <h1>2011 Holiday Appeal</h1>
    <h2> Its Never Too Late to Help Children in Need with Chronic Illnesses<img src="http://www.angelwish.org/public/images/aw_smiling_girl.jpg" class="kid"/></h2>
    <p align="left"><img src="http://www.angelwish.org/public/images/donate_now_btn.png" width="180" height="40" alt="Donate Now" /></p><br />
<br /><br />
<br />
    <div align="left">The Holidays are here, yet for 10.2 Million children living with Chronic Illnesses such as Asthma, Diabetes, HIV/AIDS, the holidays might be canceled or postponed because their families can't afford holiday presents. <br/>
    <br/>
For over 12 years, Angelwish has been helping donors just like you stretch small contributions into amazing gifts that touch the lives of families and children who are dealing with illnesses that put a great deal of stress on an economically fragile group. This year, we need your help more than ever. Please Join us and grant a holiday or birthday wish to a child living with Chronic Illness. The Power to Change a Child's Life is in Your Hands!</div>
    <p align="left">&nbsp;</p>
<p>&nbsp;</p>
<div align="right"></div>
    </div>
    <?=modules::run('Whatwedo')?>
    </div>



<script type="text/javascript">
$(function(){
	$('#living_with a').addClass('active');
});
</script>
