$(function() {
    $('#grant_a_wish a').addClass('active');
    $("ul.tabs").tabs("div.panes > div");

    drawmap('us');

    $("ul.tabs li").each(function(index,element){
        $(element).click(function(){
            if(index == 0){
                switchmap('us');
            }
            else if(index == 1){
                switchmap('uk');
            }
            else{
                switchmap('intl');
            }
        });
    });
});

function showstate(state,lat,lng){
    $('#listofstates').css('display', 'none');
    $('#'+state).css('display', 'block');

    var latlng = new google.maps.LatLng(lat, lng);
    map.panTo(latlng);
    map.setZoom(6);
}

function showlistofstates(state){
    $('#listofstates').css('display', 'block');
    $('#'+state).css('display', 'none');

    switchmap('us');
}

function showukstate(state,lat,lng){
    $('#listofukstates').css('display', 'none');
    $('#'+state).css('display', 'block');

    var latlng = new google.maps.LatLng(lat, lng);
    map.panTo(latlng);
    map.setZoom(6);
}

function showlistofukstates(state){
    $('#listofukstates').css('display', 'block');
    $('#'+state).css('display', 'none');

    switchmap('uk');
}

function showcountry(country,lat,lng){
    $('#listofcountries').css('display', 'none');
    $('#'+country).css('display', 'block');

    var latlng = new google.maps.LatLng(lat, lng);
    map.panTo(latlng);
    map.setZoom(4);
}

function showlistofcountries(country){
    $('#listofcountries').css('display', 'block');
    $('#'+country).css('display', 'none');

    switchmap('intl');
}

function drawmap(region){
    us_latlng = new google.maps.LatLng(38.0902400, -97.7128910);
    intl_latlng = new google.maps.LatLng(20.7917020, -7.0926200);
    uk_latlng = new google.maps.LatLng(53.474969999548556, -2.2412109375);

    var myOptions;
    if(region=='intl'){
        myOptions = {
          zoom: 2,
          center: intl_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }
    else if(region=='uk'){
        myOptions = {
          zoom: 6,
          center: uk_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }
    else{
        myOptions = {
          zoom: 3,
          center: us_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }

    map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);    
    var styles = [{
        url: '/public/images/map_cluster_icon.png',
        height: 45,
        width: 45,
        opt_anchor: [22, 22],
        opt_textColor: '#ff00ff',
        opt_textSize: 11
      }];

    var markers = [];
    $.getJSON('/googlemaps/getmapdata',function(data){
        for (var i = 0; i < data.length; i++) {
          var latLng = new google.maps.LatLng(parseFloat(data[i].latitude),
              parseFloat(data[i].longitude));

          var note = '<div style="font-family:Arial;font-size:14px;padding:0px 2px;">';
          note += '<div style="font-weight:bold;"><a href="/carecenter/index/' + data[i].id + '">' + data[i].name + '</a></div>';
          note += '<div style="color:#777;font-size:12px;">' + data[i].city +', '+ data[i].country + '</div>';
          note += '<div style="font-style:italic;font-size:12px;margin-top:5px;">' + data[i].description + '</div>';
          note += '</div>';
          var marker = add_marker(latLng,note);


          markers.push(marker);
        }
        markerClusterer = new MarkerClusterer(map, markers,{styles:styles});
    });

    var infowindow = new google.maps.InfoWindow({
        content: ''
    });

    function add_marker(latLng,note){
        var marker  = new google.maps.Marker({'position': latLng});
        marker.note = note;
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.content = marker.note;
            infowindow.open(map,marker);
        });
        return marker;
    }
}

function switchmap(region){
    var myOptions;
    if(region=='intl'){
        myOptions = {
          zoom: 2,
          center: intl_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }
    else if(region=='uk'){
        myOptions = {
          zoom: 6,
          center: uk_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }    
    else{
        myOptions = {
          zoom: 3,
          center: us_latlng,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          scrollwheel: false
        };
    }

    map.setOptions(myOptions);
}
