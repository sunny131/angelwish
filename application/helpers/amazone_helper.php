<?php
#       Copyright 2007 Amazon Technologies, Inc.  Licensed under the Apache License, Version 2.0 (the "License");
#       you may not use this file except in compliance with the License. You may obtain a copy of the License at:
#
#       http://aws.amazon.com/apache2.0
#
#       This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#       See the License for the specific language governing permissions and limitations under the License.
#

#To use this sample, replace the values of $accessKey and $secretKey with the values for your account

require_once 'HMAC.php'; #see http://pear.php.net/package/Crypt_HMAC
#require_once 'Request.php'; #see http://pear.php.net/package/HTTP_Request

# 	Please us PHP 5.1.1 or later to run this sample
#	1. Replace the variables $accessKey and $secreyKey with your Access Key Id and Secret Access Key
#	2. Save this file as a .php file in the htdocs folder of your Apache installation, and open it in a browser
#	3. Alternatively, you can run this sample code as a PHP application
#	4. The code sample will output a test Marketplace Widget form
#	5. You can change the parameters in the HTML Body below to get your own HTML Form
#   6. This sample, by default, generates a form which points to the Amazon Payments Sandbox.
#   To use this widget in production:
#   Replace "authorize.payments-sandbox.amazon.com" with "authorize.payments.amazon.com" in the generated HTML
#


global $accessKey,$secretKey;
$accessKey = "11SEM03K88SD016FS1G2";
$secretKey = "cIUeDnlY1cRooIA3z9SOaLWcP4/mi0/cdP8471k0";

function getMarketplaceWidgetForm($amount, 
                                $description,
                                $referenceId,
                                $immediateReturn,
                                $returnUrl,
                                $abandonUrl,
                                $processImmediate,
                                $acpaymentId,
                                $live = 'sandbox') {
    global $accessKey;
    global $secretKey;
    $formHiddenInputs['accessKey']  = $accessKey;
    $formHiddenInputs['amount']     = $amount;
    $formHiddenInputs['description'] = $description;

    $formHiddenInputs['isDonationWidget']  = '1';
    $formHiddenInputs['collectShippingAddress']     = 0;
    $formHiddenInputs['description'] = $description;

    $formHiddenInputs['signatureMethod'] = 'HmacSHA256';
    $formHiddenInputs['signatureVersion'] = '2';



    if ($referenceId) $formHiddenInputs['referenceId'] = $referenceId;
    if ($immediateReturn) $formHiddenInputs['immediateReturn'] = $immediateReturn;
    if ($returnUrl) $formHiddenInputs['returnUrl'] = $returnUrl;
    if ($abandonUrl) $formHiddenInputs['abandonUrl'] = $abandonUrl;
    if ($processImmediate) $formHiddenInputs['processImmediate'] = $processImmediate;
    if ($acpaymentId) $formHiddenInputs['amazonPaymentsAccountId'] = $acpaymentId;

    //if ($ipnUrl) $formHiddenInputs['ipnUrl'] = $ipnUrl;
    //if ($recipientEmail) $formHiddenInputs['recipientEmail'] = $recipientEmail;

    uksort($formHiddenInputs, "strnatcasecmp");
    $stringToSign = "";

    foreach ($formHiddenInputs as $formHiddenInputName => $formHiddenInputValue) {
       $stringToSign = $stringToSign . $formHiddenInputName . $formHiddenInputValue;
    }

    $formHiddenInputs['signature'] = getSignature($stringToSign, $secretKey);
    if($live == 'sandbox'){
        $form = "<form name='amazon' action=\"https://authorize.payments-sandbox.amazon.com/pba/paypipeline\" method=\"post\">\n";
    }
    else
    {
        $form = "<form name='amazon' action=\"https://authorize.payments.amazon.com/pba/paypipeline\" method=\"post\">\n";
    }
    foreach ($formHiddenInputs as $formHiddenInputName => $formHiddenInputValue) {
        $form = $form . "<input type=\"hidden\" name=\"$formHiddenInputName\" value=\"$formHiddenInputValue\" >\n";
    }
    //$form = $form . "<input type=\"image\" src=\"https://authorize.payments.amazon.com/pba/images/payNowButton.png\" border=\"0\" >\n";
    $form = $form . "</form>\n";
    return $form;

    
}

function getSignature($stringToSign) {
    global $secretKey;
    $hmac = new Crypt_HMAC($secretKey,"sha1");
    $binary_hmac = pack("H40", $hmac->hash(trim($stringToSign)));
    return base64_encode($binary_hmac);
}

 /*  <?=getMarketplaceWidgetForm("USD 1.00", "e-Card", "i123n", "1", "http://yourwebsite.com/return.html",
                           "http://yourwebsite.com/abandon.htm", "1", "http://yourwebsite.com/ipn",
                           "pba-ping-recipient@amazon.com", "USD 0.01", "4") ?>
 */