<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of get_helper
 *
 * @author ashish
 */
function initializeGet(){
    parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
}

function removeQuotes($string){
    if(($string[0] == "'" && $string[strlen($string)-1] == "'")
            || $string[0] == '"' && $string[strlen($string)-1] == '"'){
        $string = substr($string,1,-1);
    }
    return $string;
}


function shortenString($inputString, $charactersToReturn = 35){
    $ellipsis = "";
    if(strlen($inputString)>$charactersToReturn){
        $didICutAWord = substr($inputString,$charactersToReturn,1) != ' ';
        $inputString = substr($inputString, 0, $charactersToReturn);

        if($didICutAWord){
            $lastSpace = strrpos($inputString, ' ');
            $inputString = substr($inputString, 0, $lastSpace);
        }
        $ellipsis = "..";
    }
    return $inputString.$ellipsis;
}

// END Get Class

/* End of file get_helper.php */
/* Location: ./application/helpers/get_helper.php */
