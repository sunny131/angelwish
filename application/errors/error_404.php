<html>
<head>
<title>404 Page Not Found</title>
<style type="text/css">

body {
    background: url("/application/errors/bg.jpg") no-repeat fixed 100% center / cover rgba(0, 0, 0, 0);
    font-family: 'open_sanslight';
    font-size: 100%;
}


.logo {
    padding: 1% 1% 5%;
    text-align: center;
}
.logo span {
    color: #FFFFFF;
    font-size: 2em;
}
.wrap {
    margin: 5.2% auto 4%;
    width: 70%;
}
.buttom {
    background: url("/application/errors/bg2.png") no-repeat scroll 100% 0 / 100% auto rgba(0, 0, 0, 0);
    margin: 0 auto;
    text-align: center;
    vertical-align: middle;
    width: 556px;
}
.seach_bar {
    padding: 2em;
}
.seach_bar p {
    color: #FFFFFF;
    font-size: 1.5em;
    font-weight: 300;
    margin: 2.6em 0 0.9em;
}
.seach_bar span a {
    color: #FFFFFF;
    font-family: 'open_sansregular';
    font-size: 1em;
    font-weight: 300;
    text-decoration: underline;
}

</style>
</head>
<body>
	<div class="wrap">
<div class="content">
		<div class="logo">
				<h1><a href="#"><img src="/application/errors/logo.png"></a></h1>
				<span><img src="/application/errors/signal.png">Oops! The Page you requested was not found!</span>
			</div>
		
		<div class="buttom">
				<div class="seach_bar">
					<p>you can go to <span><a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>">home</a></span> page</p>
					<!-----start-sear-box--------->
					<div class="search_box">
<br /><p style="height: 10px;"></p>
					 </div>
				</div>
			</div>
		
		
</div></div>
</body>
</html>