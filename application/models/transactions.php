<?php

/**
 * Transactions Model
 *
 * @author anandwani
 */
class Transactions extends Model {

    public $URL_GENERATED = 0;
    public $TXN_INITIATED = 1;
    public $TXN_INVOKE_PAY = 2;
    public $TXN_COMPLETE = 3;

    public function saveRequest($request) {
        $sql = "INSERT INTO tbl_amazon_ipn_test (request) VALUES (?)";
        $this->db->query($sql, array(http_build_query($request)));
    }

    /**
     * Creates a new transaction and sends the caller_reference
     */
    public function createNewTransaction($userId, $amount, $reason, $companyId, $officeId =NULL, $cartId, $currencyCode,$cart_items){

        $userId = $this->db->escape($userId);
        $amount = $this->db->escape($amount);
        $reason = $this->db->escape($reason);
        $companyId = $this->db->escape($companyId);
        $officeId = $this->db->escape($officeId);
        $currencyCode = $this->db->escape($currencyCode);

        $query = "INSERT INTO tbl_transactions(fk_user_id,amount,reason,fk_company_id,currency_code,step_completed,status,created_at, previous_transaction_id, domain,json_order_items, fk_company_office_id)
        		  VALUES
				  ($userId,$amount,$reason,$companyId,$currencyCode," . $this->URL_GENERATED . ",'READY',CURRENT_TIMESTAMP,'" . $this->session->userdata('care_center_id') . "','". $_SERVER['SERVER_NAME'] ."','".$cart_items."', $officeId)";
        $this->db->query($query);

        //Retrieve caller_reference for the newly created transaction
        $transactionId = $this->db->insert_id();
        $this->db->select('caller_reference')->from('transactions')->where('pk_transaction_id', $transactionId);
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $callerReference = $row->caller_reference;
            break;
        }

        return $callerReference;
    }
    
    public function createNewTransactionwithshare($userId, $amount, $reason, $companyId, $officeId =NULL, $cartId, $currencyCode,$cart_items,$share){

        $userId = $this->db->escape($userId);
        $amount = $this->db->escape($amount);
        $reason = $this->db->escape($reason);
        $companyId = $this->db->escape($companyId);
        $officeId = $this->db->escape($officeId);
        $currencyCode = $this->db->escape($currencyCode);
        $share = $this->db->escape($share);

       $query = "INSERT INTO tbl_transactions(fk_user_id,amount,reason,fk_company_id,currency_code,step_completed,status,created_at, previous_transaction_id, domain,json_order_items, fk_company_office_id,share)
        		  VALUES
				  ($userId,$amount,$reason,$companyId,$currencyCode," . $this->URL_GENERATED . ",'READY',CURRENT_TIMESTAMP,'" . $this->session->userdata('care_center_id') . "','". $_SERVER['SERVER_NAME'] ."','".$cart_items."', $officeId,".$share.")";
        
        $this->db->query($query);

        //Retrieve caller_reference for the newly created transaction
        $transactionId = $this->db->insert_id();
        $this->db->select('caller_reference')->from('transactions')->where('pk_transaction_id', $transactionId);
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            $callerReference = $row->caller_reference;
            break;
        }

        return $callerReference;
    }

    /*
     * Step Completed values -
     * 0: URL Generated
     * 1: User checked out successfully (Ready for Pay)
     * 2: Card was charged successfully (Ready for GetStatus)
     * 3: Transaction Completed (Money received)
     *
     * Status values -
     * Completed: Txn was completed successfully
     * Failed: Txn did not complete
     * UserAction: Txn is pending on a useraction
     * Retry: Need to retry the request to update Txn
     * Review: Txn failed due to some error - needs to be reviewed
     */

    public function updateTransaction($updateData, $callerReference, $atStep) {
        $mapping = array("emailConfirmation"=>"email_confirmation","emailInitiated"=>"email_initiated","status" => "status", "description" => "status_description", "transactionId" => "fk_amazon_transaction_id",
            "tokenId" => "fk_amazon_token_id", "attempts" => "attempts", "amazonStatus" => "amazon_status",
            "amazonError" => "amazon_error", "stepCompleted" => "step_completed", "paymentMethod" => "payment_method");

        $step = self::getTransactionDetails(array("stepCompleted"), $callerReference);
        $stepCompleted = array_key_exists('stepCompleted', $step) ? $step['stepCompleted'] : 10;

        if ($atStep >= $stepCompleted) {
            $data = array();
            foreach ($updateData as $key => $value) {
                $data[$mapping[$key]] = $value;
            }

            $this->db->update('transactions', $data, array('caller_reference' => $callerReference));
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function confirmationEmailSent($callerReferences) {
        $data = array("email_confirmation"=>1);
        $this->db->where_in('caller_reference',$callerReferences)->update('transactions', $data);
    }

    public function initiatedEmailSent($callerReferences) {
        $data = array("email_initiated"=>1);
        $this->db->where_in('caller_reference',$callerReferences)->update('transactions', $data);
    }

    /*
     * Gets transaction's details
     */

    public function getTransactionDetails($selectData, $callerReference) {
	
        $mapping = array("id"=>"pk_transaction_id","emailConfirmation"=>"email_confirmation","emailInitiated"=>"email_initiated","status" => "status", "description" => "status_description", "transactionId" => "fk_amazon_transaction_id",
            "tokenId" => "fk_amazon_token_id", "attempts" => "attempts", "amazonStatus" => "amazon_status",
            "amazonError" => "amazon_error", "stepCompleted" => "step_completed", "paymentMethod" => "payment_method",
            "callerReference" => "caller_reference", "userId" => "fk_user_id", "companyId" => "fk_company_id", "amount" => "amount",
            "currencyCode" => "currency_code", "reason" => "reason", "previousTransactionId" => "previous_transaction_id");

        $columns = "";
        foreach ($selectData as $key) {
            $columns .= $mapping[$key] . " ,";
        }
        $columns = substr($columns, 0, -1);

        $this->db->select($columns)->from('transactions')->where('caller_reference', $callerReference);
        $query = $this->db->get();
        $returnArray = array();
        foreach ($query->result('array') as $row) {
            foreach ($selectData as $key) {
                $returnArray[$key] = $row[$mapping[$key]];
            }
            break;
        }

        return $returnArray;
    }

    public function getTransactionDetailsForEmail($callerReference){
        $this->db->select('fk_user_id, first_name, last_name, email, tbl_transactions.created_at, amount')
                ->from ('transactions')
                ->join('users',' tbl_users.pk_user_id = tbl_transactions.fk_user_id','inner')
                ->where('caller_reference',$callerReference);
        $query = $this->db->get();
        $row = $query->row();

        $return = array();
        $return['userId'] = $row->fk_user_id;
        $return['firstName'] = ucfirst($row->first_name);
        $return['lastName'] = ucfirst($row->last_name);
        $return['email'] = $row->email;
        $return['amount'] = $row->amount;
        date_default_timezone_set('America/New_York');
        $return['createdAt'] = date('F j, Y',strtotime($row->created_at));

        return (object)$return;

    }

    /*
     * Get transactions for cron job
     */

    public function getIncompleteTransactions($stepCompleted) {
	
        $this->db->select('caller_reference')
                ->from('transactions')
                ->where('step_completed', $stepCompleted)
                ->where("(status = 'READY' OR `status` = 'RETRY')");
		//echo $this->db->last_query();
        $query = $this->db->get();
        $returnArray = array();
        foreach ($query->result('array') as $row) {
            $returnArray[] = $row['caller_reference'];
        }
        return $returnArray;
    }

    public function getCompletedTransactions() {
        $this->db->select('caller_reference')
                ->from('transactions')
                ->where('step_completed', $this->TXN_COMPLETE)
                ->where("status","COMPLETE")
                ->where('email_confirmation',0);
        $query = $this->db->get();
        $returnArray = array();
        foreach ($query->result('array') as $row) {
            $returnArray[] = $row['caller_reference'];
        }
        return $returnArray;
    }
    public function getdata($query){
		  
		 $data=$this->db->query($query);
		 return $data->result();
		 die;
	}
 	public function updateDwollaTransaction($orderId=null,$DwollaCheckoutId=null,$DwollaClearingDate=null,$DwollaTeansactionId=null,$DwollaTestPayment=null,$status=null){
        	$updateTransaction="Update tbl_transactions set `DwollaCheckoutId`='".$DwollaCheckoutId."',
        												`status`='".$status."',
        												`DwollaClearingDate`='".$DwollaClearingDate."',
        												`d_transaction_id`='".$DwollaTeansactionId."',
        												`DwollaTestPayment`='".$DwollaTestPayment."' 
        						WHERE `DwollaOrderId`='".$orderId."'";
        	$this->db->query($updateTransaction);   
    }
    
    public function saveDwollaOrder($query=null){
    	$this->db->query($query);
    	return $this->db->insert_id();
    }

    public function savePaypalOrder($query=null){
	$this->db->query($query);
    	return $this->db->insert_id();
    }

     //send order confirmation emial
    public function sendOrderConfirmation($caller_reference=null){
		$getTransactionDetailsQuery="select trans.amount,trans.json_order_items,trans.pk_transaction_id,users.CartTotal,users.OptionalDonation,users.first_name,users.last_name,users.email from  					tbl_transactions as trans
					inner join tbl_users  as users
					ON users.pk_user_id = trans.fk_user_id
					WHERE `caller_reference`='".$caller_reference."'";
	//for adverstiment
	$host = $_SERVER['HTTP_HOST'];
	$dot_position = stripos($host,".");
	$company_url = $dot_position ? substr($host,0,$dot_position ) : false;
	if($company_url=='www'){
	$add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `id`=1") or die(mysql_error());
	$fetch_add=mysql_fetch_array($add);
	}
	else{
	$add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `company`='".$company_url."'") or die(mysql_error());   
	$num_rows=mysql_num_rows($add);
	if($num_rows>0){
	$fetch_add=mysql_fetch_array($add);
	}else{
	$fetch_add['advertisement']='';
	}
	}
	//ends
		$resultset=mysql_fetch_assoc(mysql_query($getTransactionDetailsQuery));
		if(is_array($resultset) && !empty($resultset)){
			$json_order_items=json_decode(urldecode($resultset['json_order_items']));
		}
	$CID='';
	foreach($json_order_items as $itemObject){
	$carecenterId=$itemObject->id;
	$get_Care=mysql_query("select `CID` from `tbl_wishes` where `pk_wish_id`='".$carecenterId."'") or die(mysql_error());
	$fetch_care_Center_id=mysql_fetch_array($get_Care);
	$CID=$fetch_care_Center_id['CID']; 
	break;
	}
	$Getcarecenter=mysql_query("select * from tbl_carecenters where pk_carecenter_id='".$CID."'") or die(mysql_error());
	$num_row=mysql_num_rows($Getcarecenter);
	$FetchCareCenter=mysql_fetch_array($Getcarecenter);
		    $messageBody="<div style='background-color: #FFFFFF; border: 1px solid #cccccc; border-radius: 10px 10px 10px 10px; float: left; font-family: arial; width: 600px;color:#333;'>
	<div style=' border-radius: 10px 10px 0 0; float: left; width: 100%;'><img src='http://gothamangel.org/images/sd.png'></div>
	<div style='float: left; padding-left: 20px; width: 580px;'>
		<h1 style='font-size:15px;'>Hello {$resultset['first_name']},</h1>
		<p style='float:left; width:595px;font-weight: bold;font-size: 13px;'>
			Thank you for being a digital philanthropist and improving the lives of children living with chronic
			illnesses. On ".date('F j, Y').", you contributed <Total Amount> to Angelwish for the following <br>
			items:
		</p>
			<p style='font-size: 13px;font-weight: bold;'>Donation #{$resultset['pk_transaction_id']}</p>";
     if($num_row> 0){
			$messageBody.="<p style='font-size: 15px;'>
			<div style='width:20%;float:left;'>Care Center:</div>
			<div style='width:80%;float;left;'><font style='color:#214B73;font-size: 15px;'>{$FetchCareCenter['name']}</font>
			<br />
			{$FetchCareCenter['city']},{$FetchCareCenter['state']}
			</div></p>";
		    }
			$messageBody.="<table border='1' cellpadding='0' cellspacing='0' style='float: left; font-size: 14px; width: 560px;'>
				
					<tr>
						<th width='70%' style='color:#566D7E; padding:5px;'>Gift Name</th>
						<th width='15%' style='color:#566D7E; padding:5px;'>Quantity</th>
						<th width='15%' style='color:#566D7E; padding:5px;'>Amount</th>
					</tr>";
					$i=0;
		 foreach($json_order_items as $itemObject){
			$formattedPrice=sprintf("%01.2f",$itemObject->price);
mysql_query("INSERT INTO `tbl_wish_grants` (`id` ,`wish_id` ,`name` ,`date` ,`grant`)VALUES ( NULL,'".$itemObject->id."','".$itemObject->childName."','".date('Y-m-d h:i:s')."','0')");	
		 	$messageBody.="<tr>
			<td style='color:#214B73; padding:5px;'>{$itemObject->bookName}</td>
			<td style='color:#214B73; padding:5px;text-align:center;'>1</td>
			<td style='color:#214B73; padding:5px;text-align:center;'>&#36;".$formattedPrice."</td>
			</tr>";
			$i++;
		 }
		 if($i>0){
		 $messageBody.="
			<tr>
				<td style='color:#214B73; padding:5px;'>Optional Donation to Angelwish 	</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&nbsp;</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&#36;".sprintf("%01.2f",$resultset['OptionalDonation'])."</td>
			</tr>";
		 }else {
		 		$messageBody.="	<tr>
				<td style='color:#214B73; padding:5px;'>Donation to Angelwish 	</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&nbsp;</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&#36;".sprintf("%01.2f",$resultset['amount'])."</td>
			</tr>";   
		 }
			$messageBody.="<tr>
				<td style='color:#214B73; padding:5px;'><strong>Total Amount</strong></td>
				<td style='color:#214B73; padding:5px;'>&nbsp;</td>
				<td style='color:#214B73; padding:5px;text-align:center;'><strong>&#36;".sprintf("%01.2f",$resultset['amount'])."</strong></td>
			</tr>
			</table>
		<p style='float:left; width:560px;font-size: 13px;'>
	       ".$fetch_add['advertisement']."
		</p>
				<p style='float:left; width:560px;font-size: 13px;'>
	       <b>Note:</b> To view or cancel existing payment authorizations, log into your Amazon Payments account by visiting <a href='https://payments.amazon.com/'>https://payments.amazon.com/</a>
		
		<p style='float:left; width:595px;font-size: 13px;font-weight: bold;'>
			For tax purposes, your donation is deductible to the full extent of the law and no goods or services
			were provided to you. Angelwish is a registered 501(c)3 charity in the US Tax ID# 22-3658778.</br>
			Please consider multiplying the impact of your gift by submitting this to your company matching gift
			program.
		</p>
			</div>
			<div style='float:left; width:580px; padding-left:20px;	font-size: 13px;'>
			<p style='font-size: 13px;font-weight: bold;'>Best Regards,</br>
			   Shimmy Mehta</br>
			   founder/ceo</br>
			   Angelwish
			</p>
			</div>
			</div>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		
		$headers .= 'From: Angelwish<info@angelwish.org>' . "\r\n";
		$headers .= 'Bcc:  shimmy@angelwish.org' . "\r\n";
		echo $messageBody;
	     mail($resultset['email'],'Thank You for your Donation to Angelwish',$messageBody,$headers);
	     //mail('ravinderkumar.pal@kindlebit.com','Thank You for your Donation to Angelwish',$messageBody,$headers);
	     
	     
	     //automated children script
	     
	         $grants=mysql_query("select wish_id from tbl_wish_grants where `grant`=0") or die(mysql_error());
    while($fetch=mysql_fetch_array($grants)){
	$wish_id=$fetch['wish_id'];
	mysql_query("UPDATE `tbl_wish_grants` SET `grant` = '1' WHERE `wish_id` ='$wish_id'") or die(mysql_error());
	$wishes=mysql_query("select name from tbl_wish_grants where `wish_id`='$wish_id'") or die(mysql_error());
	$output = array();
	while($fetch_wish=mysql_fetch_array($wishes)){
	$opt=$fetch_wish['name'];
	$output[]="'".$opt."'";
	}
	$all_names=implode(',', $output);
	$query_sql="SELECT tbl_children.child_name AS new_name, tbl_children.gender AS GENDER, pk_wish_id FROM tbl_wishes INNER JOIN tbl_children ON tbl_wishes.pk_wish_id ='".$wish_id."'
	AND tbl_children.child_name NOT IN ($all_names) ORDER BY RAND() LIMIT 0,1";
	$gotNrep=mysql_query($query_sql) or die(mysql_error());
	$fetch_single_name=mysql_fetch_array($gotNrep);
	$fetch_1=$fetch_single_name['new_name'];
	$GENDER=$fetch_single_name['GENDER'];
	if($GENDER=='male'){
	$GENDER='M';
	}
	if($GENDER=='female'){
	$GENDER='F';
	}
	mysql_query("UPDATE `tbl_wishes` SET `child_name` = '$fetch_1',`child_gender`='$GENDER' WHERE `pk_wish_id` ='$wish_id'") or die(mysql_error());
    }
	     
	}
	


}
