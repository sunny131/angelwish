<?php

/**
 * Wishes Model
 *
 * @author anandwani
 */
class MWishes extends Model {

    public function getWishes($carecenterId='',$type='age',$orderby='DESC', $page = 1,$limit = 10) {
        $offset = ($page-1)*$limit;

        if($type=='age'){
            $feild = 'child_age';
            $desc  = $orderby;
        }else
        {
           $feild = 'book_price';
           $desc  = $orderby;
        }

        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state,child_country, child_disease,
            child_poverty, book_name, book_image, book_final_price')
                ->where('is_active',1)
                ->where('is_granted',0)
//                ->where("CID",$carecenterId)
                ->order_by($feild,$desc)
                ->get('wishes',$limit,$offset);
        

        $wishes = array();
        foreach($query->result() as $row){
            $wish = array();
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            $wish['book_final_price'] = $row->book_final_price;
            $wish['child_country'] = $row->child_country;
            //add markup price
            //add markup price
           
            $this->load->library('session');
			if($this->session->userdata('markup')){
				$price = ($row->book_final_price*$this->session->userdata('markup'))/100;
				$wish['price']=$row->book_final_price+$price;
			}else{
				$wish['price'] = $row->book_final_price;
			}
           // $wish['price'] = $row->book_final_price;
            $inCart = $this->cart->hasWish($wish['id']);

            if(!$inCart)
                $wishes[] = $wish;
        }
        //shuffle($wishes);
        return $wishes;
    }

    public function getWish($id) {
        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_final_price')
                ->where('pk_wish_id',$id)
                ->get('wishes');

        $wish = array();
        foreach($query->result() as $row){
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            //add markup price
            
            $this->load->library('session');
			if($this->session->userdata('markup') && $this->session->userdata('markup')>0){
				$price = ($row->book_final_price*$this->session->userdata('markup'))/100;
				$wish['price']=$row->book_final_price+$price;
			}else{
				$wish['price'] = $row->book_final_price;
			}
			
           // $wish['price'] = $row->book_final_price;
            $wish['inCart'] = $this->cart->hasWish($wish['id']);
            break;
        }
        
        return $wish;
    }
    
        public function getCarecenteroptions($cidAndWishId) {
        $query = $this->db->select('carecenter_id,option_name,option_desc,amount')
                ->where('auto_id',$cidAndWishId)
                ->get('carecenter_options');

        $wish = array();
        foreach($query->result() as $row){
	$wish['id'] =$cidAndWishId;
            $wish['carecenter_id'] = $row->carecenter_id;
            $wish['bookName'] = $row->option_name;
            $wish['option_desc'] = $row->option_desc;
            $wish['price'] = $row->amount;
	    $this->load->model('Carecenters');
		$details=$this->Carecenters->details($wish['carecenter_id']);
	    $wish['childName']=$details['name'];
	    $created_at=$details['created_at'];
	    $timestamp_start = strtotime($created_at);
		$timestamp_end = strtotime(date('Y-m-d'));
		$difference = abs($timestamp_end - $timestamp_start);
	    $wish['age']=floor($difference/(60*60*24*365));
           // $wish['price'] = $row->book_final_price;
            $wish['inCart'] = $this->cart->hasWish($wish['id']);
            break;
        }
        
        return $wish;
    }

    public function saveProcessedCart($txnId){
        $wishes = $this->cart->getWishes();
        if(count($wishes)>0){
            $wishIds = array();
            $query = "INSERT INTO tbl_transaction_wishes
                (fk_transaction_id,fk_wish_id)
                VALUES";

            foreach($wishes as $wish){
                $query .= " ($txnId,".$wish['id']."),";
                $wishIds[]                = $wish['id'];
            }
            $query = substr($query, 0, -1);
            $this->db->query($query);

            $data = array('is_granted'=>1);
            $this->db->where_in('pk_wish_id',$wishIds);
            $this->db->update('wishes',$data);

            $this->cart->destroy();
            return true;
        }else{
            return false;
        }
    }


    function addMargin()
    {
        $start = 15000;
        $query = "SELECT * FROM tbl_wishes LIMIT $start, 5000";
        $values = $this->db->query($query);
        foreach ($values->result() as $value)
        {
            $id      = $value->pk_wish_id;
            $price   = $value->book_price;
            $margin  = $price*20/100;
            $final   = $price + $margin ;

            $query1 = "UPDATE tbl_wishes SET book_margin = $margin, book_final_price = $final WHERE pk_wish_id = $id";
            $this->db->query($query1);
            echo $id .'<br>';
        }


    }

    function re_export()
    {
        $start = 15000;
        $end   = $start+5000;
        $query = "SELECT * FROM tbl_wishes WHERE is_granted = '1' and pk_wish_id >= $start AND pk_wish_id < $end";
        $values = $this->db->query($query);
        foreach ($values->result() as $value)
        {
            //print_r($value);
            echo "<br>".$value->pk_wish_id;
            $a['CID']            = $value->CID;
            $a['child_name']     = $value->child_name;
            $a['child_age']      = $value->child_age;
            $a['child_gender']   = $value->child_gender;
            $a['child_city']     = $value->child_city;
            $a['child_state']    = $value->child_state;
            $a['child_country']  = $value->child_country;
            $a['child_disease']  = $value->child_disease;
            $a['child_poverty']  = $value->child_poverty;
            $a['asin']           = $value->asin;
            $a['uk_price']       = $value->uk_price;
            $a['book_name']      = $value->book_name;
            $a['book_price']     = $value->book_price;
            $a['book_image']      = $value->book_image;
            $a['book_margin']      = $value->book_margin;
            $a['book_final_price'] = $value->book_final_price;
            $a['is_granted']       = $value->is_granted;
            $a['is_active']        = $value->is_active;

            $this->db->insert('tbl_wishes',$a);
            /*$query1 = "UPDATE tbl_wishes SET book_margin = $margin, book_final_price = $final WHERE pk_wish_id = $id";
            
            echo $id .'<br>';*/
        }


    }
    function getcompanyidbyurl($url){
			$limit=1;
			$offset=0;
			 $query = $this->db->select('pk_company_id')
                ->where('url',$url)
                ->where('is_active',1)
                ->get('companies',$limit,$offset);
		//	echo 'url:'.$url.'<br />';
		//echo	$this->db->last_query();
            $row=$query->result();
            if(isset($row['0']->pk_company_id)){	
				return $row['0']->pk_company_id;
			}else{
				return 0;
			}
            die;
        
	}	
    function iscompanyactive($url){
			$limit=1;
			$offset=0;
			 $query = $this->db->select('is_active')
                ->where('url',$url)
                ->where('is_active','1')
                ->get('companies',$limit,$offset);
              //  echo ($url);
            $row=$query->result();
            if(isset($row['0']->is_active)){	
				return 1;
			}else{
				return 0;
			}
            die;
        
	}	
	function updatetracknumber($companyId){
			
			$limit=1;
			$offset=0;
			 $query = $this->db->select('trackcount')
                ->where('company_id',$companyId)
                ->get('track',$limit,$offset);
            
            $row=$query->result();
            
            if(isset($row['0']->trackcount)){
				$trackcount=$row['0']->trackcount+1;
				$query1 = "UPDATE tbl_track SET trackcount = '$trackcount' WHERE company_id = $companyId";
				$this->db->query($query1);
			}else{
				$query = $this->db->select('name')
                ->where('pk_company_id',$companyId)
                ->get('companies',$limit,$offset);
            
				$company_name=$query->result();
				 
				
				$trackcount=1;
				$a['company_id']	= $companyId;
				$a['trackcount']    = $trackcount;
				$a['trackcount']    = $trackcount;
				$a['company_name']  = $company_name['0']->name;
				$this->db->insert('tbl_track',$a);
			}
	}
	function  getmarkup($companyId){
		$limit=1;
		$offset=0;
		 $query = $this->db->select('percentage_markup')
			->where('pk_company_id',$companyId)
			->get('companies',$limit,$offset);
		
		$row=$query->result();
		//echo $row['0']->percentage_markup;
		/*die;*/
	   return $row['0']->percentage_markup; 
	}
	
	// function created by kindlebit solutions
	function update_wishes($value,$action,$mwish_id){
	if($action=='update_name'){
		$data = array('child_name'=>$value);
		$this->db->where_in('pk_wish_id',$mwish_id);
		$this->db->update('wishes',$data);
	    }
	if($action=='update_age'){
		$data = array('child_age'=>$value);
		$this->db->where_in('pk_wish_id',$mwish_id);
		$this->db->update('wishes',$data);
	    }
	    return 1;
	}
	

}
