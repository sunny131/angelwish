<?php
/**
 * Description of carecenter
 *
 * @author anandwani
 */

class Accesscontrol extends Model{

     function authenticate($email,$password){
        $query = $this->db->select('first_name, last_name, email, role')
        ->from('users')
        ->where(array("email"=>$email,"password"=>$password));

        $query = $this->db->get();
        if($query->num_rows()){
            foreach($query->result() as $row){
                $user = array();
                $user['first_name'] = $row->first_name;
                $user['last_name']  = $row->last_name;
                $user['email']      = $row->email;
                $user['role']       = $row->role;
                
                $this->CI =& get_instance();
                $this->CI->user = $user;
                return TRUE;
            }
        }
        else{
            return FALSE;
        }
    }

}
