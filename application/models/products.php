<?php

/**
 * Products Model
 *
 * @author anandwani
 */
class Products extends Model {
    const BUFFER_PERCENT = 7;
    
    private $all_products = array(
        array("id" => 1,
            "image" => "http://ecx.images-amazon.com/images/I/51sMhyLWlcL._BO2,204,203,200_PIsitb-sticker-arrow-click,TopRight,35,-76_AA300_SH20_OU01_.jpg",
            "name" => "The Way Science Works",
            "price" => "16.49"),
        array("id" => 2,
            "image" => "http://ecx.images-amazon.com/images/I/51PPzpnkl1L._BO2,204,203,200_PIsitb-sticker-arrow-click,TopRight,35,-76_AA300_SH20_OU01_.jpg",
            "name" => "First Science Encyclopedia",
            "price" => "11.55"),
        array("id" => 3,
            "image" => "http://ecx.images-amazon.com/images/I/61pWmlrdcpL._BO2,204,203,200_PIsitb-sticker-arrow-click,TopRight,35,-76_AA300_SH20_OU01_.jpg",
            "name" => "TIME For Kids Super Science Book",
            "price" => "9.59"),
        array("id" => 4,
            "image" => "http://ecx.images-amazon.com/images/I/51saDZPPHVL._BO2,204,203,200_PIsitb-sticker-arrow-click,TopRight,35,-76_AA300_SH20_OU01_.jpg",
            "name" => "TIME for Kids BIG Book of Why",
            "price" => "11.78"),
        array("id" => 5,
            "image" => "http://ecx.images-amazon.com/images/I/51-KqA7WuwL._BO2,204,203,200_PIsitb-sticker-arrow-click,TopRight,35,-76_AA300_SH20_OU01_.jpg",
            "name" => "Time For Kids: Thomas Edison: A Brilliant Inventor",
            "price" => "3.99")
    );

    public function getProduct($id) {
        foreach ($this->all_products as $product) {
            if ($product["id"] == $id) {
                $product['price'] *= (1+self::BUFFER_PERCENT/100);
                return $product;
            }
        }
    }

    public function getPrice($id) {
        foreach ($this->all_products as $product) {
            if ($product["id"] == $id) {
                $product['price'] *= (1+self::BUFFER_PERCENT/100);
                return $product['price'];
            }
        }
    }
}
