<?php

/**
 * Kids Model
 *
 * @author anandwani
 */
class Kids extends Model {

    private $all_kids = array(
        array("id" => 1,
            "name" => "Henry",
            "age" => 88,
            "gender" => "M",
            "poverty" => "High",
            "state" => "Oklahoma",
            "disease" => "HIV/AIDS"),
        array("id" => 2,
            "name" => "Martha",
            "age" => 58,
            "gender" => "F",
            "poverty" => "High",
            "state" => "New Jersey",
            "disease" => "Asthama"),
        array("id" => 3,
            "name" => "Carl",
            "age" => 74,
            "gender" => "M",
            "poverty" => "Medium",
            "state" => "Utah",
            "disease" => "Cancer"),
        array("id" => 4,
            "name" => "Alex",
            "age" => 102,
            "gender" => "M",
            "poverty" => "high",
            "state" => "California",
            "disease" => "HIV/AIDS"),
        array("id" => 5,
            "name" => "Stephanie",
            "age" => 64,
            "gender" => "F",
            "poverty" => "high",
            "state" => "Wisconsin",
            "disease" => "HIV/AIDS")
    );

    public function getKid($id) {
        foreach ($this->all_kids as $kid) {
            if ($kid["id"] == $id) {
                $kid['ageYears'] = floor($kid['age'] / 12);
                $kid['ageMonths'] = $kid['age'] % 12;
                return $kid;
            }
        }
    }

}
