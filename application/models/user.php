<?php
class User extends Model {

     public function getallsetting(){
		$query = $this->db->select('settingKey, settingValue, status,id')
				->get('setting');
		return $query->result();
	 }
     public function getsettingbyid($id){
		$query = $this->db->select('settingKey, settingValue, status,id')
				->where('id',$id)
				->get('setting');
		return $query->row();
	 }
     public function getsettingbyname($settingKey){
		$query = $this->db->select('settingKey, settingValue, status,id')
				->where('settingKey',$settingKey)
				->get('setting');
		return $query->row();
	 }
     
     public function updatesetting($settingValue,$status,$id){
		$query1 = "UPDATE tbl_setting SET settingValue ='".$settingValue."', status='".$status."' WHERE id = $id";
		$this->db->query($query1);
		
	}
     
     public function getallusers(){
		   $query = $this->db->select('pk_user_id, email, first_name')
				->order_by("email",'asc')
				->group_by("email")
                ->get('users',$limit,$offset);
         foreach($query->result() as $row){
			 $rows[]=$row;
		 }
		 return $rows;
          
	  }
	  
	  
	    public function totalusers(){
		   $query = $this->db->select('pk_user_id, email, first_name')
				->order_by("email",'asc')
				->group_by("email")
                ->get('users',$limit,$offset);
                $i=0;
         foreach($query->result() as $row){
			 $i++;
		 }
		 return $i;
          
	  }
	  
	  public function getusersbyemail($email){
		$query="select u.pk_user_id, u.email, trans.created_at, trans.pk_transaction_id,trans.payment_method,trans.amount from tbl_users u
				inner join tbl_transactions as trans on trans.fk_user_id=u.pk_user_id  and (trans.status='Completed' || trans.status='COMPLETE')
				where u.email='$email'" ;
		$dd=mysql_query($query);
		while($resultset=mysql_fetch_assoc($dd)){
			$rows[]=$resultset;
		} 
		return $rows;
	  }
	  
	   public function getcompanyname(){
		$query="select name,pk_company_id from tbl_companies";
		$dd=mysql_query($query);
		while($resultset=mysql_fetch_assoc($dd)){
			$rows[]=$resultset;
		} 
		return $rows;
	  }
	  
	  public function getcompanydetail($c_id){
		$query="select count(pk_transaction_id) as count,sum(amount) as total from  tbl_transactions where (status 
		='Completed' || status ='COMPLETE') and fk_company_id=".$c_id;
		$dd=mysql_query($query);
		$resultset=mysql_fetch_assoc($dd);
		return $resultset;
	  }

	  

	  public function gettransactionbycompany($date_from,$date_to){
		 $query="select count(*) as trans,trans.fk_company_id, comp.name from tbl_transactions trans
				left join tbl_companies as comp on trans.fk_company_id=comp.pk_company_id
				where (trans.status='Completed' || 
				trans.status='COMPLETE') ";
		if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";
		$query.=" group by trans.fk_company_id";
		
		$dd=mysql_query($query);
		while($resultset=mysql_fetch_assoc($dd)){
			$rows[]=$resultset;
		} 
		 
		return $rows;
	  }
	public function total_wish($date_from,$date_to){
		//$query='select count(pk_wish_id) as total_wish from tbl_wishes  ';
		$query='select count(tbl_cartitems.transaction_id) as total_wish from  tbl_cartitems 
				inner join tbl_transactions on 
				(tbl_transactions.status ="Completed" || tbl_transactions.status ="COMPLETE") 
				and tbl_transactions.pk_transaction_id=tbl_cartitems.transaction_id
  ';
		if(!empty($date_from) && !empty($date_to))
			$query.=" where  date(tbl_transactions.created_at)>=date('$date_from') and 
			date(tbl_transactions.created_at)<=date('$date_to') ";	 
			 
		return @mysql_result(mysql_query($query),0);
	}
	public function  total_amount($date_from,$date_to){
		//$query='select sum(book_final_price) as total_amount from tbl_wishes';
		$query='select sum(amount) as total_amount from  tbl_transactions where status ="Completed" || status ="COMPLETE"  ';
		if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";	
			//die($query); 
		return @mysql_result(mysql_query($query),0);
	}
	public function total_quick_donation($date_from,$date_to){
		$query='select sum(amount) as total_quick_donation from  tbl_transactions where (status 
		="Completed" || status ="COMPLETE") and reason  != "Granting wishes at Angelwish.org" ';
		if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";	 
			//die($query);
		return @mysql_result(mysql_query($query),0);
	}
	
	public function percent_optional_donation($date_from,$date_to){
			$query='SELECT count(`pk_user_id`) FROM `tbl_users` 
			inner join tbl_transactions on 
							(tbl_transactions.status ="Completed" || tbl_transactions.status ="COMPLETE") 
							and tbl_transactions.fk_user_id=tbl_users.pk_user_id';
			if(!empty($date_from) && !empty($date_to))
				$query.=" and  date(tbl_transactions.created_at)>=date('$date_from') and date(tbl_transactions.created_at)<=date('$date_to') ";				
			
			$query.=' WHERE `OptionalDonation` != "0"';
			$total_donation=@mysql_result(mysql_query($query),0);

			$query='select count(*) from tbl_transactions where 
			(tbl_transactions.status ="Completed" || tbl_transactions.status 
			="COMPLETE") ';
			$total_transaction=@mysql_result(mysql_query($query),0);
			return ($total_donation/$total_transaction)*100;
			
	}
	public function total_us_trans($date_from,$date_to){
		$query='select count(*) as total_us_trans from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 	AND (previous_transaction_id<500 || previous_transaction_id= "" || previous_transaction_id IS NULL)';
			if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";	 
		return @mysql_result(mysql_query($query),0);
	}
	public function total_uk_trans($date_from,$date_to){
		$query='select count(*) as total_uk_trans from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 
				AND (previous_transaction_id>=500 AND 
				previous_transaction_id<700)';
			if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";
		return @mysql_result(mysql_query($query),0);
	}
	public function total_internation_trans($date_from,$date_to){
		$query='select count(*) as total_internation_trans from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 
				And previous_transaction_id>=700';
			if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";
		return @mysql_result(mysql_query($query),0);
	}
	public function total_transaction($date_from,$date_to){
		$query='select count(*) as total_transaction from  tbl_transactions where status ="Completed" || status ="COMPLETE"';
		
		if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(created_at)>=date('$date_from') and 
			date(created_at)<=date('$date_to') ";
			
			return @mysql_result(mysql_query($query),0);
	}
	 
	
	
	public function  total_optional_donation($date_from,$date_to){
		$query='select sum(tbl_users.OptionalDonation) as total_optional_donation from  tbl_users
				inner join tbl_transactions on 
				(tbl_transactions.status ="Completed" || tbl_transactions.status ="COMPLETE") ';
		if(!empty($date_from) && !empty($date_to))
			$query.=" and  date(tbl_transactions.created_at)>=date('$date_from') and 
			date(tbl_transactions.created_at)<=date('$date_to') ";
					
		$query.=' and tbl_transactions.fk_user_id=tbl_users.pk_user_id';
		return @mysql_result(mysql_query($query),0);
	}
	public function care_center(){
		$query="select count(distinct(previous_transaction_id)) as care from tbl_transactions trans 
		where 	(trans.status='Completed' || trans.status='COMPLETE') ";
		$data['care']=@mysql_result(mysql_query($query),0);
		
		$query="select count(*) as total_care from tbl_carecenters where is_active='1'";
		$data['total_care']=@mysql_result(mysql_query($query),0);
		
		$data['percentage']=($data['care']/$data['total_care'])*100;
		/*
		echo '<pre>';
		print_r($data);
		die; */
			return $data;
	}
	 function company_data($id){
		$query='select * from tbl_transactions as tr
			left join tbl_company_offices as cmo
			on tr.fk_company_office_id=cmo.pk_company_office_id

			left join tbl_carecenters as care
			on tr.previous_transaction_id=care.pk_carecenter_id
			where tr.fk_company_id="'.$id.'" and (status="COMPLETE" || status="Completed")';
		$rows=array();
		$res=mysql_query($query);
		while($row=mysql_fetch_assoc($res)){
			$rows[]=$row;
		}
		return $rows;
	}
}
 
 


 
