<?php
/**
 * Description of carecenter
 *
 * @author anandwani
 */

class Carecenters extends Model{

    function states($country,$status){
        $sql = 'SELECT  distinct state FROM tbl_carecenters WHERE country = ? AND is_active = ?';
        $query = $this->db->query($sql,array($country,$status));
        $states = array();
        foreach($query->result() as $row){
            $states[] = $row->state;
        }
        sort($states);
        return $states;
    }

    function countries(){
        $sql = 'SELECT  distinct country FROM tbl_carecenters WHERE country != ? AND country != ? AND is_active = ?';
        $query = $this->db->query($sql,array('United States','United Kingdom',1));
        $countries = array();
        foreach($query->result() as $row){
            $countries[] = $row->country;
        }
        sort($countries);
        return $countries;
    }

    function carecentersin($region){
	$sql1='';
        $carecenters = array();
        $places = array();
        $sql = 'SELECT pk_carecenter_id, name, city, latitude, longitude ';
        $sql .= 'FROM tbl_carecenters ';
        $sql .= 'WHERE is_active = ? ';
       

        switch($region){
            case 'US': $places = self::states('United States',1);
                       $where_clause = 'state';
                       break;
            case 'UK': $places = self::states('United Kingdom',1);
                       $where_clause = 'state';
                       break;
            case 'INTL': $places = self::countries();
                       $where_clause = 'country';
                       break;
            
        }
		
		 $sql1 .= ' order by city asc , name asc';
        foreach($places as $place){
            $sql_final = $sql."AND $where_clause = ?".$sql1;
            $query = $this->db->query($sql_final,array(1,$place));
            $carecenters[$place] = array();

            foreach($query->result() as $row){
                $carecenter = array();
                $carecenter['id'] = $row->pk_carecenter_id;
                $carecenter['name'] = $row->name;
                $carecenter['city'] = $row->city;
                $carecenter['latitude'] = $row->latitude;
                $carecenter['longitude'] = $row->longitude;
                $carecenters[$place][] = $carecenter;
            }
        }

        return $carecenters;
    }
	
	function carecentersstate($region, $state){
        $carecenters = array();
        $places = array();
        $sql = 'SELECT pk_carecenter_id, name, city, latitude, longitude ';
        $sql .= 'FROM tbl_carecenters ';
        $sql .= 'WHERE is_active = ? ';

        switch($region){
            case 'US': $places = self::states('United States',1);
                       $where_clause = 'state';
                       break;
            case 'UK': $places = self::states('United Kingdom',1);
                       $where_clause = 'state';
                       break;
            case 'INTL': $places = self::countries();
                       $where_clause = 'country';
                       break;
            
        }

		$sql .= "AND state = '" . $state . "'";
		
        foreach($places as $place){
			$sql_final = $sql."AND $where_clause = ?";
			
			$query = $this->db->query($sql_final,array(1,$place));
            $carecenters[$place] = array();

            foreach($query->result() as $row){
                $carecenter = array();
                $carecenter['id'] = $row->pk_carecenter_id;
                $carecenter['name'] = $row->name;
                $carecenter['city'] = $row->city;
                $carecenter['latitude'] = $row->latitude;
                $carecenter['longitude'] = $row->longitude;
                $carecenters[$place][] = $carecenter;
            }
        }

        return $carecenters;
    }

    function details($id){
        // added by cubet technologies 1-12-2011
        $sql = "SELECT name as CName,description as CDesc,city as CCity,state as CState,Country as CCountry,'' as WishLink3,true as domestic,created_at FROM tbl_carecenters WHERE pk_carecenter_id = ? and is_active = 1";
        $query = $this->db->query($sql, array($id));
        if($query->num_rows()<1){
            $sql = "SELECT Name as CName,Description as CDesc,City as CCity,Country as CState,'' as WishLink3,
                false as domestic FROM tbl_temp_carecenter_intl WHERE CID = ?";
            $query = $this->db->query($sql, array($id));
        }
        $row = $query->row();

        $details = array();
        $details['id'] = $id;
        $details['name'] = $row->CName;
        $details['desc'] = $row->CDesc;
        $details['city'] = $row->CCity;
        $details['state'] = $row->CState;
        $details['country'] = $row->CCountry; // added by cubet technologies 1-12-2011
        $details['wishlink'] = $row->WishLink3;        
        $details['domestic'] = $row->domestic;
	$details['created_at'] = $row->created_at;
        $details['wishimage'] = null;

        if($details['domestic']){
            $sql = "SELECT ImgPath FROM tbl_wishimage WHERE CID = ?";
            $query = $this->db->query($sql, array($id));
            $row = $query->row();
            $details['wishimage'] = @$row->ImgPath;
        }
        return $details;
    }

    function getcarecentersformap($region = 'all'){
        $query = $this->db->select('pk_carecenter_id, name, description, city, country, latitude, longitude');
        switch($region){
            case 'US': $this->db->where('country','United States');
                       break;
            case 'UK': $this->db->where('country','United Kingdom');
                       break;
            case 'INTL': $this->db->where('country !=','United States');
                       break;
        }
        $this->db->where('is_active',1);
        $query = $this->db->get('carecenters');
        $carecenters = array();
        foreach($query->result() as $row){
            $carecenter = array();
            $carecenter['id'] = $row->pk_carecenter_id;            
            $carecenter['name'] = $row->name;
            $carecenter['description'] = $row->description;
            $carecenter['city'] = $row->city;            
            $carecenter['country'] = $row->country;
            $carecenter['latitude'] = $row->latitude;
            $carecenter['longitude'] = $row->longitude;

            $carecenters[] = $carecenter;
        }
        return $carecenters;
    }

    function updateLatitudeLongitude($id,$lat,$lng){
        $data = array("latitude"=>$lat,"longitude"=>$lng);
        $query = $this->db->update('carecenters',$data,array('pk_carecenter_id'=>$id));

        echo $query;
    }

    function getcarecenterAddresses(){
        $query = $this->db->select('pk_carecenter_id, address, city, state, country')
                ->where('latitude',null)
                ->where('longitude',null)
                ->get('carecenters');
        $carecenters = array();
        foreach($query->result() as $row){
            $carecenter = array();
            $carecenter['id'] = $row->pk_carecenter_id;
            $carecenter['address'] = $row->address;
            $carecenter['city'] = $row->city;
            $carecenter['state'] = $row->state;
            $carecenter['country'] = $row->country;

            $carecenters[] = $carecenter;
        }
        return $carecenters;
    }
    /**
     * Function to get the id from short code
     * @param <type> $code
     * @return <type>
     */
    function getIdCode($code)
    {        
        $sql="SELECT
                    pk_carecenter_id
              FROM
                    tbl_carecenters
              WHERE
                    shortcode='$code'";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            $row = $query->row();
            $id = $row->pk_carecenter_id;
        }
        return $id;
    }


    /**
     * function to check international care-center
     * @author jobanjohn cubet technologies
     * @since 30-11-2011
     */
    public function checkCarecenterCountry($id){
       $query = $this->db->select('pk_carecenter_id, address, city, state, country')
                ->where('is_active',1)
                ->where("pk_carecenter_id",$id)
                ->get('tbl_carecenters');
       $row = $query->row_array();

       return $row['country'];       
    }

    /**
     * function to add international care-center's donation
     * @author jobanjohn cubet technologies
     * @since 02-12-2011
     */

    public function insertInternationalDonations($data)
    {
       $this->db->insert('tbl_donation_to_intl', $data);
    }
    
    function getAllCareCenters($sort='',$order=''){
	$name='';
	if(!$sort) {
		$query = $this->db->select('*')
		    ->get('carecenters');
	}
	if($sort=='cid'){
	   $name='pk_carecenter_id';
	   	$query = $this->db->select('*')
		    ->order_by($name,$order)
			->get('carecenters');
	}
	if($sort=='carecenter_Name'){
		    $name='name';
	   	$query = $this->db->select('*')
		    ->order_by($name,$order)
			->get('carecenters');
	}
	$care_centers = array();
	foreach($query->result() as $row){
	$care_center = array();
	$care_center['CID'] = $row->pk_carecenter_id;
	$care_center['name'] = $row->name;
	$care_center['city'] = $row->city;
	$care_center['state'] = $row->state;
	$care_center['country'] = $row->country;
	$care_centers[] = $care_center;
	}
	return $care_centers;
	
    }
    
}
