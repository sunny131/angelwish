<?php

/**
 * Wishes Model
 *
 * @author anandwani
 */
class Wish extends Model {

    public function getAllwishes($carecenterId='',$type='pk_wish_id',$orderby='DESC', $page = 1,$limit = 10) {
        $offset = ($page-1)*$limit;

        if($type=='age'){
            $feild = 'child_age';
            $desc  = $orderby;
        }else
        {
           $feild = 'book_price';
           $desc  = $orderby;
        }

        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_final_price')
              //  ->where('is_active',1)
                //->where('is_granted',0)
//                ->where("CID",$carecenterId)
                ->order_by($feild,$desc)
                ->get('wishes',$limit,$offset);
        

        $wishes = array();
        foreach($query->result() as $row){
            $wish = array();
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            $wish['price'] = $row->book_final_price;
            $wishes[] = $wish;
        }
        //shuffle($wishes);
        return $wishes;
    }

    public function getWishbyid($id) {
        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_final_price')
                ->where('pk_wish_id',$id)
                ->get('wishes');

        $wish = array();
        foreach($query->result() as $row){
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
        }
        
        return $wish;
    }

    
}
