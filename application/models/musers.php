<?php
class MUsers extends Model {

     public function addOrUpdate($firstName,$lastName,$email,$role = 'donor',$cartTotal=0,$optionalDonationAmount=0,$address_a=''){
		
        $data = array(
           'first_name' => $firstName ,
           'last_name' => $lastName ,
           'email' => $email,
           'role' => $role,
           'CartTotal' => $cartTotal,
           'OptionalDonation' => $optionalDonationAmount,
           'created_by'=>'website',
           'updated_by'=>'website'
           
        );
		if(is_array($address_a)){
			$data['address']=$address_a['address'];
			$data['city']=$address_a['city'];
			$data['state']=$address_a['state'];
			$data['zipcode']=$address_a['zipcode'];
		}
        $this->db->insert('users', $data);

        return $this->db->insert_id();
    }

    public function getInvolved($firstName, $lastName, $email, $company, $phone = NULL, $message = NULL, $keepInformed = 0){
        $data = array(
           'first_name' => $firstName ,
           'last_name' => $lastName ,
           'email' => $email,
           'company' => $company,
           'phone' => $phone,
           'message' => substr($message,0,500),
           'keep_informed' => $keepInformed
        );
        $this->db->insert('tbl_get_involved', $data);
    }

    //send order confirmation emial
    public function sendOrderConfirmation($dwollaOrderId=null){
		$getTransactionDetailsQuery="select trans.amount,trans.json_order_items,trans.pk_transaction_id,users.CartTotal,users.OptionalDonation,users.first_name,users.last_name,users.email from  					tbl_transactions as trans
					inner join tbl_users  as users
					ON users.pk_user_id = trans.fk_user_id
					WHERE `DwollaOrderId`='".$dwollaOrderId."'";
	   //for adverstiment
	  $host = $_SERVER['HTTP_HOST'];
	  $dot_position = stripos($host,".");
	  $company_url = $dot_position ? substr($host,0,$dot_position ) : false;
	  if($company_url=='www'){
	  $add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `id`=1") or die(mysql_error());
	  $fetch_add=mysql_fetch_array($add);
	  }
	  else{
	  $add=mysql_query("select `advertisement` from `tbl_advertisement` where `active`=1 and `company`='".$company_url."'") or die(mysql_error());   
	  $num_rows=mysql_num_rows($add);
	  if($num_rows>0){
	  $fetch_add=mysql_fetch_array($add);
	  }else{
	  $fetch_add['advertisement']='';
	  }
	  }
   //ends		
		$resultset=mysql_fetch_assoc(mysql_query($getTransactionDetailsQuery));
		if(is_array($resultset) && !empty($resultset)){
			$json_order_items=json_decode(urldecode($resultset['json_order_items']));
		}
	$CID='';
	foreach($json_order_items as $itemObject){
	$carecenterId=$itemObject->id;
	$get_Care=mysql_query("select `CID` from `tbl_wishes` where `pk_wish_id`='".$carecenterId."'") or die(mysql_error());
	$fetch_care_Center_id=mysql_fetch_array($get_Care);
	$CID=$fetch_care_Center_id['CID']; 
	break;
	}
	$Getcarecenter=mysql_query("select * from tbl_carecenters where pk_carecenter_id='".$CID."'") or die(mysql_error());
	$FetchCareCenter=mysql_fetch_array($Getcarecenter);
		  $messageBody="<div style='background-color: #FFFFFF; border: 1px solid #cccccc; border-radius: 10px 10px 10px 10px; float: left; font-family: arial; width: 600px;color:#333;'>
	<div style=' border-radius: 10px 10px 0 0; float: left; width: 100%;'><img src='http://gothamangel.org/images/sd.png'></div>
	<div style='float: left; padding-left: 20px; width: 580px;'>
		<h1 style='font-size:15px;'>Hello {$resultset['first_name']},</h1>
		<p style='float:left; width:595px;font-weight: bold;font-size: 13px;'>
			Thank you for being a digital philanthropist and improving the lives of children living with chronic
			illnesses. On ".date('F j, Y').", you contributed <Total Amount> to Angelwish for the following <br>
			items:
		</p>
			 <p style='font-size: 13px;font-weight: bold;'>Donation #{$resultset['pk_transaction_id']}</p>
			 <p style='font-size: 15px;'>
			 <div style='width:20%;float:left;'>Care Center:</div>
			 <div style='width:80%;float;left;'><font style='color:#214B73;font-size: 15px;'>{$FetchCareCenter['name']}</font>
			 <br />
			 {$FetchCareCenter['city']},{$FetchCareCenter['state']}
			 </div></p>
			<table border='1' cellpadding='0' cellspacing='0' style='float: left; font-size: 14px; width: 560px;'>
				
					<tr>
						<th width='70%' style='color:#566D7E; padding:5px;'>Gift Name</th>
						<th width='15%' style='color:#566D7E; padding:5px;'>Quantity</th>
						<th width='15%' style='color:#566D7E; padding:5px;'>Amount</th>
					</tr>";
		 foreach($json_order_items as $itemObject){
			$formattedPrice=sprintf("%01.2f",$itemObject->price);
mysql_query("INSERT INTO `tbl_wish_grants` (`id` ,`wish_id` ,`name` ,`date` ,`grant`)VALUES ( NULL,'".$itemObject->id."','".$itemObject->childName."','".date('Y-m-d h:i:s')."','0')");				
		 	$messageBody.="<tr>
			<td style='color:#214B73; padding:5px;'>{$itemObject->bookName}</td>
			<td style='color:#214B73; padding:5px;text-align:center;'>1</td>
			<td style='color:#214B73; padding:5px;text-align:center;'>&#36;".$formattedPrice."</td>
			</tr>";
		 }
		 $messageBody.="
			<tr>
				<td style='color:#214B73; padding:5px;'>Optional Donation to Angelwish 	</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&nbsp;</td>
				<td style='color:#214B73; padding:5px;text-align:center;'>&#36;".sprintf("%01.2f",$resultset['OptionalDonation'])."</td>
			</tr>
			<tr>
				<td style='color:#214B73; padding:5px;'><strong>Total Amount</strong></td>
				<td style='color:#214B73; padding:5px;'>&nbsp;</td>
				<td style='color:#214B73; padding:5px;text-align:center;'><strong>&#36;".sprintf("%01.2f",$resultset['amount'])."</strong></td>
			</tr>
			</table>
	       <p style='float:left; width:560px;font-size: 13px;'>
	       ".$fetch_add['advertisement']."
	       </p>
		<p style='float:left; width:595px;font-size: 13px;font-weight: bold;'>
			For tax purposes, your donation is deductible to the full extent of the law and no goods or services
			were provided to you. Angelwish is a registered 501(c)3 charity in the US Tax ID# 22-3658778.</br>
			Please consider multiplying the impact of your gift by submitting this to your company matching gift
			program.
		</p>
			</div>
			<div style='float:left; width:580px; padding-left:20px;	font-size: 13px;'>
			<p style='font-size: 13px;font-weight: bold;'>Best Regards,</br>
			   Shimmy Mehta</br>
			   founder/ceo</br>
			   Angelwish
			</p>
			</div>
			</div>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		
		$headers .= 'From: Angelwish<info@angelwish.org>' . "\r\n";
		$headers .= 'Bcc:  shimmy@angelwish.org' . "\r\n";
		//echo $messageBody;
		//mail('ravinderkumar.pal@kindlebit.com','Thank You for your Donation to Angelwish',$messageBody,$headers);
		mail($resultset['email'],'Thank You for your Donation to Angelwish',$messageBody,$headers);
	  
	     
	  //automated_children_renaming() function start here
	  
	  
	  
	      $grants=mysql_query("select wish_id from tbl_wish_grants where `grant`=0") or die(mysql_error());
    while($fetch=mysql_fetch_array($grants)){
	$wish_id=$fetch['wish_id'];
	mysql_query("UPDATE `tbl_wish_grants` SET `grant` = '1' WHERE `wish_id` ='$wish_id'") or die(mysql_error());
	$wishes=mysql_query("select name from tbl_wish_grants where `wish_id`='$wish_id'") or die(mysql_error());
	$output = array();
	while($fetch_wish=mysql_fetch_array($wishes)){
	$opt=$fetch_wish['name'];
	$output[]="'".$opt."'";
	}
	$all_names=implode(',', $output);
	$query_sql="SELECT tbl_children.child_name AS new_name, tbl_children.gender AS GENDER, pk_wish_id FROM tbl_wishes INNER JOIN tbl_children ON tbl_wishes.pk_wish_id ='".$wish_id."'
	AND tbl_children.child_name NOT IN ($all_names) ORDER BY RAND() LIMIT 0,1";
	$gotNrep=mysql_query($query_sql) or die(mysql_error());
	$fetch_single_name=mysql_fetch_array($gotNrep);
	$fetch_1=$fetch_single_name['new_name'];
	$GENDER=$fetch_single_name['GENDER'];
	if($GENDER=='male'){
	$GENDER='M';
	}
	if($GENDER=='female'){
	$GENDER='F';
	}
	mysql_query("UPDATE `tbl_wishes` SET `child_name` = '$fetch_1',`child_gender`='$GENDER' WHERE `pk_wish_id` ='$wish_id'") or die(mysql_error());
    }
    
	}
	


}
