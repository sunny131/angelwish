<?php
/**
 * Save amazon ipn request
 *
 * @author anandwani
 */

class Memail extends Model{
    //TO DO: take out all these and consolidate them into config.php - allows
    // to have different configuration options for dev, test and production
    const FROM_ADDRESS = "no-reply";
    const BCC_LIST = "shimmy@angelwish.org, nandwani.ashish+angelwishdonations@gmail.com";

    public function  __construct() {
        //TO DO: set up email configuration
        $CI =& get_instance();
        $CI->load->library('email');
        parent::__construct();
    }

    public function sendDonationInitiatedConfirmation($transactionDetails){
        //builds from address like "no-reply@angelwish.org"
        $fromAddress = self::FROM_ADDRESS."@".$_SERVER['HTTP_HOST'];
        $fromName = "Angelwish";
        $bcc = NULL;//self::BCC_LIST;

        $toUserId = $transactionDetails->userId;
        $toEmailAddress = $transactionDetails->email;
        $date = $transactionDetails->createdAt;
        $amount = $transactionDetails->amount;

        $subject = $transactionDetails->firstName.', your Donation to Angelwish is Being Processed';

        $message = "Dear ".$transactionDetails->firstName.",";
        $message .= "\n\nYou Rock!";
        $message .= "\n\nThis is to acknowledge that your donation of $".$transactionDetails->amount." to Angelwish has been initiated and is currently being processed.";
        $message .= "\n\nWe will send you a confirmation email once your donation is finalized.";
        $message .= "\n\nBest Regards,\nShimmy Mehta\nfounder/ceo\nAngelwish.org";


        $altMessage = NULL;

        self::sendEmail($toUserId, $toEmailAddress, $subject, $message, $altMessage,
            $fromAddress, $fromName, $bcc);
    }

    public function sendDonationProcessedConfirmation($transactionDetails){
        $fromAddress = self::FROM_ADDRESS."@".$_SERVER['HTTP_HOST'];
        $fromName = "Angelwish";
        $bcc = self::BCC_LIST;

        $toUserId = $transactionDetails->userId;
        $toEmailAddress = $transactionDetails->email;
        $date = $transactionDetails->createdAt;
        $amount = $transactionDetails->amount;

        $subject = $transactionDetails->firstName.", your Angelwish Donation Receipt";

        $message = "Dear ".$transactionDetails->firstName.",";
        $message .= "\n\nThank you for being a digital philanthropist and improving the lives of children living with chronic illnesses.";
        $message .= "\n\nYour contribution on $date consisted of $".$amount.". Please hold onto this email for tax purposes.";
        $message .= "\n\nFor tax purposes, your donation is deductible to the full extent of the law and no goods or services were provided to you. Angelwish is a registered 501(c)3 charity. Tax ID# 22-3658778. ";
        $message .= "\n\nPlease consider multiplying the impact of your gift by submitting this to your company matching gift program.";$message .= "\n\nBest Regards,\nShimmy Mehta\nfounder/ceo\nAngelwish.org";


        $altMessage = NULL;

        self::sendEmail($toUserId, $toEmailAddress, $subject, $message, $altMessage,
            $fromAddress, $fromName, $bcc);
    }

    

    private function sendEmail($toUserId, $toEmailAddress, $subject, $message, $altMessage = NULL,
            $fromAddress = "no-reply@angelwish.org", $fromName = "Angelwish", $bcc = NULL){

        $this->email->from($fromAddress, $fromName);
        $this->email->to($toEmailAddress);
        $this->email->bcc($bcc);

        $this->email->subject($subject);
        $this->email->message($message);
        if(!empty($altMessage))
            $this->email->set_alt_message($altMessage);

        $this->email->send();

        $data = array(
           'fk_user_id' => $toUserId ,
           'email' => $toEmailAddress ,
           'subject' => $subject,
           'message' => $message,
           'from_address'=>$fromAddress,
           'from_name'=>$fromName,
           'debug_info'=>$this->email->print_debugger()
        );

        $this->db->insert('email_log', $data);
    }

}
