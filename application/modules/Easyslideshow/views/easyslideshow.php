<style>
    <?=$css?>
</style>

<div id ="shaddow">
</div>
<div id="slideshow">
    
    <!-- container for the slides -->
    <div id="slider">
        <ul>
            <!-- first slide -->
            <li class="show">
                <div class="slide">                    
                    <div class="slide-text">
                        <div class="text">
                           <span class="highlight-text">100%</span> of individual donations
    go towards program services thanks to our corporate sponsors
                        </div>
                        <div class="gaw-button">
                            <a href="/wishes" style="text-decoration: none;color:#fff;"><img src="/public/images/grant_a_wish_btn.png"/></a>
                        </div>
                    </div>
                    <div class="slide-image">
                        <img src="/public/images/slide1_img.jpg" alt="Prayer"
                             />
                    </div>                    
                </div>
            </li>


            <!-- second slide -->
            <li>
                <div class="slide">                   
                    <div class="slide-text">
                        <div class="text">
                            Let Angelwish help your company make a bigger impact in the community<span class="highlight-text"></span>
</div>
                        <div class="gaw-button">
                            <a href="/learning" style="text-decoration: none;color:#fff;"><img src="/public/images/learn_btn.png"/></a>
                        </div>
                    </div>
                    <div class="slide-image">
                        <img src="/public/images/slide4_img.jpg" alt="Learning About Philanthropy"
                             />
                    </div>                   
                </div>
            </li>

            <!-- third slide -->
            <li>
                <div class="slide">                    
                    <div class="slide-text">
                         <div class="text">
                           Angelwish works with over <span class="highlight-text">125 Hospitals</span> and Care Centers in<br/>
                            <span class="highlight-text">9 countries</span> on <br/>
                            <span class="highlight-text">6 continents</span>
                        </div>
                        <div class="gaw-button">
                            <a href="/wishes" style="text-decoration: none;color:#fff;"><img src="/public/images/grant_a_wish_btn.png"/></a>
                        </div>
                    </div>
                    <div class="slide-image">
                        <img src="/public/images/slide3_img.jpg" alt="Kids in Library"
                              />
                    </div>                    
                </div>
            </li>
        </ul>
    </div>
    <span id="prevBtn"><a href="javascript:void(0);"><img src="/public/images/next.png"/></a></span>
    <span id="nextBtn"><a href="javascript:void(0);"><img src="/public/images/prev.png"/></a></span>
</div>
<div style="clear:both;margin:30px 0;"></div>

<script type="text/javascript" src="/public/js/easySlider1.7.js"></script>
<script type="text/javascript">
    <?=$js?>
</script>
