<?php

class Easyslideshow extends MX_Controller {

	function index()
	{
            $data['js'] = $this->load->view('easyslideshow.js',NULL,TRUE);
            $data['css'] = $this->load->view('easyslideshow.css',NULL,TRUE);
            $this->load->view('easyslideshow',$data);
	}
}