<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Wishes
 *
 * @author ashish
 */
class Wishes extends MY_Controller{

    function index($id = NULL){
            $this->load->model('mwishes');
            $this->load->model('carecenters');
            $mode=$this->mwishes->getsetting('mode');
            
          
            if($this->mwishes->is_share()){
				$data['share']=1;
			}else{
				$data['share']=0;
			}
			
			$data['s_title']=$this->mwishes->gettitle();
			
            $type ='age';
            $orderby = 'ASC';
            $country = $this->carecenters->checkCarecenterCountry($id);
           // if($country == 'United States' || $country == 'United Kingdom'){
             if(true){
             
                $temp['wishes'] = $this->mwishes->getWishes_randomly($id,$type,$orderby);
                $data['hasWishes'] = $this->mwishes->getWishesCount($id);
                $temp['inCart'] = FALSE;
                $temp['cid'] = $id;
                $quantity=$this->mwishes->getsetting('quantity');
				$temp['qtyy']=$quantity;
                if($mode=='disaster')
					$data['wishes'] = $this->load->view('wishlist-select',$temp,TRUE);
                else
					$data['wishes'] = $this->load->view('wishlist-input',$temp,TRUE);
                
				
                $this->session->set_userdata('care_center_id', $id);
                $temp['wishes'] = $this->cart->getWishes();
                $temp['inCart'] = TRUE;
                $data['inCartWishes'] = $this->load->view('wishlist',$temp,TRUE);

                $data['cartHasWishes'] = !$this->cart->isEmpty();
                $data['cartTotalWishes'] =  $this->cart->count();
                $data['total'] = ($this->cart->total())*1.15;

                $data['fname'] = ($this->session->userdata('fname')) ?
                        $this->session->userdata('fname') : 'First Name';
                $data['lname'] = $this->session->userdata('lname') ?
                        $this->session->userdata('lname') : 'Last Name';
                $data['email'] = $this->session->userdata('email') ?
                        $this->session->userdata('email') : 'Email Address';

                $js['is_mobile'] = $this->agent->is_mobile() ? 'true' : 'false';
				
				
                
                if($mode=='disaster')
					$data['js'] = $this->load->view('wishes-select.js', $js, TRUE);
                //if($mode=='holiday')
                else
					$data['js'] = $this->load->view('wishes-input.js', $js, TRUE);
					
                $data['css'] = $this->load->view('wishes.css', NULL, TRUE);
				if($this->session->userdata('source')){
					$data['companyID']=$this->session->userdata('source');
					//$data['company_title']=$this->mwishes->getTitle_company($data['companyID']);
				}
				
                $this->load->view('wishes',$data);

            }
            else
            {
                $companyId = CompanyRouter::$is_corporate ? CompanyRouter::$company['id'] : NULL;
                $data['companyurl'] = CompanyRouter::$is_corporate ? CompanyRouter::$company['url'] : '';
                $data['id'] = $id;
                $key = md5(time().rand(10000,99999));
                $this->session->set_userdata('secure_key', $key);
                $data['secure_key'] = $key;
                $this->load->view('donate',$data);
            }
	}

    function more(){
		$this->load->model('mwishes');
        $mode=$this->mwishes->getsetting('mode');
        $page    = $this->input->get_post('page');
        //addd by joban john 14-11-2011
        $type    = $this->input->get_post('type');
        $orderby = $this->input->get_post('orderby');
	$page_loading = $this->input->get_post('page_loading');
        $page = is_nan($page) ? 2 : $page;
        
        $id   = $this->session->userdata('care_center_id');  
        $this->load->model('mwishes');
        
        $js['is_mobile'] = $this->agent->is_mobile() ? 'true' : 'false';
       if($page_loading==1){
	$temp['wishes'] = $this->mwishes->getWishes_randomly($id,$type,$orderby,$page);
       }else{
        $temp['wishes'] = $this->mwishes->getWishes($id,$type,$orderby,$page);
       }
        if($temp['wishes']){
            $temp['js'] = $this->load->view('wishes_new.js', $js, TRUE);
        }
        $temp['inCart'] = FALSE;
        
        $quantity=$this->mwishes->getsetting('quantity');
        $temp['qtyy']=$quantity;
        if($mode=='disaster')
			$data['wishes'] = $this->load->view('wishlist-select',$temp);
        //if($mode=='holiday')
        else
			$data['wishes'] = $this->load->view('wishlist-input',$temp);
        
    }



}
// END Wishes Class

/* End of file Wishes.php */
/* Location: ./application/libraries/Wishes.php */
