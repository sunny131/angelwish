<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of Wishes
 *
 * @author ashish
 */
class Wishes extends MY_Controller{
    
    function index($id = NULL){
            $this->load->model('mwishes');
            $type ='';
            $temp['wishes'] = $this->mwishes->getWishes($id,$type);
            $temp['inCart'] = FALSE;
            $data['wishes'] = $this->load->view('wishlist',$temp,TRUE);

            $this->session->set_userdata('care_center_id', $id);
            
            $temp['wishes'] = $this->cart->getWishes();
            $temp['inCart'] = TRUE;
            $data['inCartWishes'] = $this->load->view('wishlist',$temp,TRUE);

            $data['cartHasWishes'] = !$this->cart->isEmpty();
            $data['cartTotalWishes'] =  $this->cart->count();
            $data['total'] = ($this->cart->total())*1.15;

            $data['fname'] = ($this->session->userdata('fname')) ?
                    $this->session->userdata('fname') : 'First Name';
            $data['lname'] = $this->session->userdata('lname') ?
                    $this->session->userdata('lname') : 'Last Name';
            $data['email'] = $this->session->userdata('email') ?
                    $this->session->userdata('email') : 'Email Address';

            $js['is_mobile'] = $this->agent->is_mobile() ? 'true' : 'false';

            $data['js'] = $this->load->view('wishes.js', $js, TRUE);
            $data['css'] = $this->load->view('wishes.css', NULL, TRUE);
            $this->load->view('wishes',$data);
	}

    function more(){
        $page    = $this->input->get_post('page');
        //addd by joban john 14-11-2011
        $type    = $this->input->get_post('type');
        $orderby = $this->input->get_post('orderby');
        if(!$type){
            $type = $this->session->userdata('sort_type');
        }
        if(!$orderby){
            $orderby = $this->session->userdata('orderby');
        }
        $type = $type?$type:'age';
        $orderby = $orderby? $orderby:'DESC';
        $page = is_nan($page) ? 2 : $page;
        $id   = $this->session->userdata('care_center_id');
        $this->session->set_userdata('sort_type',$type);
        $this->session->set_userdata('orderby',$orderby);
        $this->load->model('mwishes');

        $js['is_mobile'] = $this->agent->is_mobile() ? 'true' : 'false';
        $temp['js'] = $this->load->view('wishes.js', $js, TRUE);
        $temp['wishes'] = $this->mwishes->getWishes($id,$type,$orderby,$page);
        $temp['inCart'] = FALSE;
        $data['wishes'] = $this->load->view('wishlist',$temp);
    }


    function export()
    {
        $this->mwishes->addMargin();
    }

}
// END Wishes Class

/* End of file Wishes.php */
/* Location: ./application/libraries/Wishes.php */
