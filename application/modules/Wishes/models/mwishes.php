<?php

/**
 * Wishes Model
 *
 * @author anandwani
 */
class MWishes extends Model {

	public function getsetting($key=''){
		if($key!=''){
		$query = $this->db->select('settingValue')
			->where('settingKey',$key)
			->get('setting');
			$data=$query->row();
			return $data->settingValue;
		}
		else{
			return false;
		}
	}
    public function getWishes($carecenterId='',$type='age',$orderby='DESC', $page = 1,$limit = 10) {

        $offset = ($page-1)*$limit;

        if($type=='age'){
            $feild = 'child_age';
            $desc  = $orderby;
        }else
        {
           $feild = 'book_final_price';
           $desc  = $orderby;
        }

        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_final_price')
                ->where('is_active',1)
                //->where('is_granted',0)
				//->where("(created_at) >=",'2013-11-17 17:31:54')
				->where("(created_at) >=",'2013-12-01 17:31:54')
                ->where("CID",$carecenterId)
                ->order_by($feild,$desc)
                ->get('wishes',$limit,$offset);
        
	
        $wishes = array();
        foreach($query->result() as $row){
            $wish = array();
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            //add markup price
           $this->load->library('session');
			if($this->session->userdata('markup') && $this->session->userdata('markup')>0){
				//echo $this->session->userdata('markup');
				//echo $this->session->userdata('source');
				//die;
				$price = ($row->book_final_price*$this->session->userdata('markup'))/100;
				$wish['price']=$row->book_final_price+$price;
			}else{
				$wish['price'] = $row->book_final_price;
			}
			
			$inCart = $this->cart->hasWish($wish['id']);

            if(!$inCart)
                $wishes[] = $wish;
        }
        //shuffle($wishes);
        return $wishes;
    }
    public function getWishes_randomly($carecenterId='',$type='age',$orderby='DESC', $page = 1,$limit = 10) {

        $offset = ($page-1)*$limit;

        if($type=='age'){
            $feild = 'child_age';
            $desc  = $orderby;
        }else
        {
           $feild = 'book_final_price';
           $desc  = $orderby;
        }

        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_final_price')
                ->where('is_active',1)
                //->where('is_granted',0)
				//->where("(created_at) >=",'2013-11-17 17:31:54')
				->where("(created_at) >=",'2013-12-01 17:31:54')
                ->where("CID",$carecenterId)
                ->order_by('pk_wish_id','RANDOM')
                ->get('wishes',$limit,$offset);
        
	
        $wishes = array();
        foreach($query->result() as $row){
            $wish = array();
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            //add markup price
           $this->load->library('session');
			if($this->session->userdata('markup') && $this->session->userdata('markup')>0){
				//echo $this->session->userdata('markup');
				//echo $this->session->userdata('source');
				//die;
				$price = ($row->book_final_price*$this->session->userdata('markup'))/100;
				$wish['price']=$row->book_final_price+$price;
			}else{
				$wish['price'] = $row->book_final_price;
			}
			
			$inCart = $this->cart->hasWish($wish['id']);

            if(!$inCart)
                $wishes[] = $wish;
        }
        //shuffle($wishes);
        return $wishes;
    }

    /*
     * function to check wishes exists
     * @author jobanjohn cubet technologies
     * @since 29-11-2011
     */

    public function getWishesCount($id){
       $query = $this->db->select('pk_wish_id')
                ->where('is_active',1)
                ->where('is_granted',0)
                ->where("CID",$id)
                ->get('wishes');
       
       if($query->num_rows() > 0)
       {
           return true;
       }
       else
       {
           return false;
       }
    }

    public function getWish($id) {
        $query = $this->db->select('pk_wish_id, child_name, child_age, child_state, child_disease,
            child_poverty, book_name, book_image, book_price')
                ->where('pk_wish_id',$id)
                ->get('wishes');

        $wish = array();
        foreach($query->result() as $row){
            $wish['id'] = $row->pk_wish_id;
            $wish['childName'] = $row->child_name;
            $wish['age'] = $row->child_age;
            $wish['state'] = $row->child_state;
            $wish['disease'] = $row->child_disease;
            $wish['poverty'] = $row->child_poverty;
            $wish['bookName'] = $row->book_name;
            $wish['image'] = $row->book_image;
            //add markup price
            $wish['price'] = $row->book_price+10;
            $wish['inCart'] = $this->cart->hasWish($wish['id']);

            break;
        }
        return $wish;
    }

    public function saveProcessedCart($txnId){
        $wishes = $this->cart->getWishes();
        if(count($wishes)>0){
            $wishIds = array();
            $query = "INSERT INTO tbl_transaction_wishes
                (fk_transaction_id,fk_wish_id)
                VALUES";

            foreach($wishes as $wish){
                $query .= " ($txnId,".$wish['id']."),";
                $wishIds[]                = $wish['id'];
            }
            $query = substr($query, 0, -1);
            $this->db->query($query);

            $data = array('is_granted'=>1);
            $this->db->where_in('pk_wish_id',$wishIds);
            $this->db->update('wishes',$data);

            $this->cart->destroy();
            return true;
        }else{
            return false;
        }
    }
	function is_share(){
		$this->load->library('session');
		if( $this->session->userdata('source') ){
			 $query = $this->db->select('share')
			->where('pk_company_id', $this->session->userdata('source'))
			->get('companies');
			$data=$query->result();
			// echo $data['0']->share;
			 if($data['0']->share=='Y'){
				 return true;
			 }else{
				 return false;
			 }
			 
		}else{
			return false;
		}
	}
	function gettitle(){
		$this->load->library('session');
		if( $this->session->userdata('source') ){
			$query = $this->db->select('title')
			->where('pk_company_id', $this->session->userdata('source'))
			->get('companies');
			$data=$query->result();
			 
			 return $data['0']->title;
		}
	}

    function addMargin()
    {
        /*$start = 13000;
        $query = "SELECT * FROM tbl_wishes LIMIT $start, 2000";
        $values = $this->db->query($query);
        foreach ($values->result() as $value)
        {
            $id      = $value->pk_wish_id;
            $price   = $value->book_price;
            $margin  = $price*10/100;
            $final   = $price + $margin ;

            $query1 = "UPDATE tbl_wishes SET book_margin = $margin, book_final_price = $final WHERE pk_wish_id = $id";
            $this->db->query($query1);
            echo $id .'<br>';
        }*/


    }

}
