<style>
    <?=$css?>
</style>
<script>
/**
 * Function to check the cart total. 
 */
function checkCartTotal(){
	var _CartTotal=parseInt($("#cartTotal").text());
	if(_CartTotal==0.00 || _CartTotal<=0){
		return false;
	}
	//code to update angelwish discount.
	return true;
}
</script>
<div id="wishes-container">
    <div id="wishlist" class="left">
    <?
        
        //added by jobanjohn cubet technologies
        if($hasWishes) { ?>
            <div class="tab">
				<a style="display:block" id="age" class="top" onclick="sort('age');" href="javascript:void(0);">Sort by Age</a>
                <a id="price" class="top" onclick="sort('price');" href="javascript:void(0);">Sort by Price</a>
                <input type="hidden" name="order" value="DESC" id="order">
            </div>
        <? }
         //end
    ?>
    <div class="wishes">
        <?
        //added by jobanjohn cubet technologies
        if($hasWishes) {
         echo $wishes;
         }
         else
         {
            echo "<b>There is no wishes associated with this care center</b>";
         }
         //end
        ?>
    </div>
    </div>
    <?
    //added by jobanjohn cubet technologies
    if($hasWishes) { ?>
    <div id="cart">
        <h3>Your Cart</h3>
        <form class="cart-form" method="post" onsubmit="return validate()" action="/checkout">
            <div class="cartSection" id="cart_wishes">
                <div id="emptyMessage" style="<?if($cartHasWishes){?>display:none;<?}?>">
                    Your Cart is empty
                </div>
                <div class="wishes" style="<?if(!$cartHasWishes){?>display:none;<?}?>">
                    <div class="oneWish-outer">
                        <div class="oneWish">
                            <div class="allWishes">
                                <?=$inCartWishes?>
                            </div>
                            <br class="clear"/>
                        </div>
                    </div>
                    <div style="text-align:center;">
                        <span class="leftArrow"><img src="/public/images/left-arrow.png"/></span>
                        <span class="pages">Wish <span class="current">1</span> of <span class="total"><?=$cartTotalWishes?></span></span>
                        <span class="rightArrow"><img src="/public/images/right-arrow.png"/></span>
                    </div>
                    <div style="text-align:right;margin: 5px 10px;font-size: 12px;">
                        <div style="float: right;margin-left:60px;"><i>15%</i></div>
                        <div style="width:200px;text-align: right;float: right;">
                            <input type="checkbox" checked="true" name="support" id="support"/>Optional donation to Angelwish
                        </div>
                        <br class="clear"/>
                    </div>
                </div>

                <div id="amount" style="text-align:right;margin: 5px 0px 5px 10px;font-size: 12px;">
                        <div style="float: right;">
                             $&nbsp;<input style="width:40px;font-size:12px;text-align:right;" name="cash" id="cash" value="0.00"/>
                        </div>
 	                    <div style="width:187px;text-align:left;float:right;padding-top:5px;">
 		                         Donate a specific amount
 		                </div>
 		                <br class="clear"/>
 		                </div>

                <div id="total">Your Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ <span class="value"><?=sprintf("%01.2f", $total)?></span></div>
                <br class="clear"/>
            </div>
            <div class="donorDetails">
                Angelwish is a 501(c)(3) charity - so all your donations qualify for tax exemption!
                <label style="font-size:14px;margin-top:10px;margin-bottom:5px;font-weight:bold;">Mail me my receipt at:</label>
                <input type="text" name="fname" id="fname" value="<?=$fname?>" class="cart-form" />
                <input type="text" name="lname" id="lname" value="<?=$lname?>" class="cart-form" />
                <p>
                    <input type="text" name="email" id="email" value="<?=$email?>" class="cart-form" />
                </p>
                <p>
                    <span class="error" style="display:none;">Please enter your details</span>
                </p>

                <p style="margin-top:10px;">
                    <input type="image" src="/public/images/amazon_checkout_on.png" value="Submit" alt="Submit">
                     &nbsp; &nbsp;
                  <a  id='checkoutwithdwolla' onclick='return checkCartTotal();' href='/cart/checkoutDwolla'> <img src="/public/images/btn-pay-with-dwolla.png"></a>
                   
                </p>
            </div>
        </form>
    </div>
    <br class="clear"/>
    <? } //end ?>
</div>

<script type="text/javascript">
    <?=$js?>
</script>
