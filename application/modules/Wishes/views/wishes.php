<style>
    <?=$css?>
</style>
<div id="wishes-container">
    <div id="wishlist" class="left">
    <?
        //added by jobanjohn cubet technologies
        if($hasWishes) { ?>
            <div class="tab">
				<a style="display:block" id="age" class="top" onclick="sort('age');" href="javascript:void(0);">Sort by Age</a>
                <a id="price" class="top" onclick="sort('price');" href="javascript:void(0);">Sort by Price</a>
                <input type="hidden" name="order" value="DESC" id="order">
            </div>
        <? }
         //end
    ?>
    <div class="wishes">
        <?
        //added by jobanjohn cubet technologies
        if($hasWishes) {
         echo $wishes;
         }
         else
         {
            echo "<b>Yay! There are no wishes left to grant at this care center</b>";
         }
         //end
        ?>
    </div>
    </div>
    <?
    //added by jobanjohn cubet technologies
    if($hasWishes) { ?>
    <div id="cart">
        <h3>Your Cart </h3>
        <form class="cart-form" method="post" onsubmit="return validate()" id="cartForm" action="/checkout">
            <div class="cartSection" id="cart_wishes">
                <div id="emptyMessage" style="<?if($cartHasWishes){?>display:none;<?}?>">
                    Your Cart is empty
                </div>
                <div class="wishes" style="<?if(!$cartHasWishes){?>display:none;<?}?>">
                    <div class="oneWish-outer">
                        <div class="oneWish">
                            <div class="allWishes">
                                <?=$inCartWishes?>
                            </div>
                            <br class="clear"/>
                        </div>
                    </div>
                    <div style="text-align:center;">
                        <span class="leftArrow"><img src="/public/images/left-arrow.png"/></span>
                        <span class="pages">Wish <span class="current">1</span> of <span class="total"><?=$cartTotalWishes?></span></span>
                        <span class="rightArrow"><img src="/public/images/right-arrow.png"/></span>
                    </div>
		    		    <div style="text-align:right;margin: 5px 10px;font-size: 12px;width: 100%;">

<div style="float: right; width: 245px; margin-right: 66px;"> (Sales tax of 8% has already been applied) </div>


    </div>
		    <br class="clear"/>
                    <div style="text-align:right;margin: 5px 10px;font-size: 12px;">
                        <div style="float: right;margin-left:60px;"><i>15%</i></div>
                        <div style="width:200px;text-align: right;float: right;">
                            <input type="checkbox" checked="true" name="support" id="support"/>Optional donation to Angelwish
                        </div>
                        <br class="clear"/>
                    </div>
                </div>
                <div id="amount" style="text-align:right;margin: 5px 0px 5px 10px;font-size: 12px;">
                        <div style="float: right;">
                             $&nbsp;<input style="width:40px;font-size:12px;text-align:right;" name="cash" id="cash" value="0.00"/>
                        </div>
 	                    <div style="width:187px;text-align:left;float:right;padding-top:5px;">
 		                         Add a specific donation amount
 		                </div>
 		                <br class="clear"/>
 		                </div>

                <div id="total">Your Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ <span id="cartTotal" class="value"><?=sprintf("%01.2f", $total)?></span></div>
                <br class="clear"/>
            </div>
            <div class="donorDetails">
                Angelwish is a registered charity. 100% of your donation is tax deductible!
                  <label style="font-size:14px;margin-top:10px;margin-bottom:5px;font-weight:bold;">E-mail me my receipt at:</label>
                <input type="text" name="fname" id="fname" value="<?=$fname?>" class="cart-form" />
                <input type="text" name="lname" id="lname" value="<?=$lname?>" class="cart-form" />
                <p>
                    <input type="text" name="email" id="email" value="<?=$email?>" class="cart-form" />
                </p>
                <? if(CompanyRouter::$is_corporate == TRUE && CompanyRouter::$has_corporate_offices== TRUE) {?>
                <select name="office" id="office">
                <option value="0" selected="selected"><?php echo  $s_title; ?></option>
               <?  for ($i = 0; $i < count(CompanyRouter::$offices); $i ++) {  ?>
                <option value="<?=CompanyRouter::$offices[$i]->pk_company_office_id?>"><?=CompanyRouter::$offices[$i]->description?></option>
               <? }?>
				</select>
				<? } ?>
				 <?php if(isset($share) && $share==1 && isset($companyID)) { ?>
					<input name="share" type="checkbox" id="share"  value="Y"> <?php echo $s_title;?>
				 <?php } ?>
                <p>
                    <span class="error" style="display:none;">Please enter your details</span>
                </p>

                <p style="margin-top:10px;">

          <div id="b1" style="float:left;width:159px;margin-left: 38px">   <input type="image" width="142" onclick='checkPaymentProcessor("Amazon");' src="/public/images/golden_medium_donate_withmsg_whitebg.gif" value="Submit" alt="Submit" onclick="alert('amazon');"/>
                    <input type="hidden" name="paymentProcessor" id="paymentProcessor" value="Amazon" /></div>
<div id="b2" style="float:left;width:50px;margin-top: 5px">Or</div>

                  <div id="b3" style="float:left;margin-left: -23px;margin-top: -5px">  <input type="image" width="142" onclick='checkPaymentProcessor("Paypal");' src="/public/images/donate.jpg" value="paypal" name="submit"  alt="Make payments with PayPal - it's fast, free and secure!"> </div>
                 <div style="width:159px;margin-left: 137px;margin-top:60px;"><input type="image" onclick='checkPaymentProcessor("Dwolla");'  value="Submit" alt="Submit" src="/public/images/btn-pay-with-dwolla.png"></div></p>
                 <!--http://developers.dwolla.com/assets/images/bd/button-3.png-->
</div>
          <input type="hidden" name="custom" id="custom" value="<?=$details['id']?>" /> 

        </form>


       
    </div>
    <br class="clear"/>
    <? } //end ?>
</div>
<script type="text/javascript">
    <?=$js?>
</script>
