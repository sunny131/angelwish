/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(function() {

    $('#cart').css({'top': 0,
        'left': ($('#wishlist').width())
    }).show();

    var cartTop = 0;

    $('.wish .addToCart a.active').each(function(index,anchor){
        $(anchor).bind('click',addToCart);
    });

    $('.wish .removeFromCart a.active').each(function(index,anchor){
        $(anchor).bind('click',removeFromCart);
    });


    $(window).bind('scroll resize',function(){
        stickyCart();
        moreWishes();
    });

    stickyCart();
    moreWishes();

    $('.cart-form').focus(fieldFocus);
    $('.cart-form').blur(fieldBlur);
    //$('.leftArrow').click(cartScrollLeft);
    //$('.rightArrow').click(cartScrollRight);
    //$('#support').click(reCalculateTotal);

    function stickyCart(){
        var is_mobile = <?=$is_mobile?>;
        if(!is_mobile && $(window).height() > $('#cart').height()){
            if( $(window).scrollTop() > ($('#wishes-container').offset().top - 15) ){
                $('#cart').addClass('fixed');
                $('#cart').css({'top':15});
                var cartLeft = $('#wishlist').width() + $('#wishlist').offset().left;
                $('#cart').css({'left':  cartLeft});
            }
            else{
                $('#cart').removeClass('fixed');
                $('#cart').css({'top':cartTop});
                var cartLeft = $('#wishlist').width();
                $('#cart').css({'left':  cartLeft});
            }
        }
    }

    function moreWishes(){
        var seenHeight = $(window).scrollTop() + $(window).height();
        var seenPercent = (seenHeight*100)/$(document).height();

        if(seenPercent > 95 && !loadingNewPage){
            loadingNewPage = true;
            $.post('/wishes/more',{page:++currentPage},function(data){
               $('#wishlist .wishes').append(data);
               loadingNewPage = false;
               $('.wish .addToCart a.active').each(function(index,anchor){
                    $(anchor).bind('click',addToCart);
                });
            });
        }
    }

    function reCalculateTotal(){
        var newTotal = 0;
        $('#cart .wish').each(function(index,element){
            newTotal += parseFloat($(element).find('.price .value').html());
        });
        var support = $('#support').is(':checked');
        if(support){
            newTotal *= 1.15;
        }
        newTotal = Math.round(newTotal*100)/100;
        newTotal += "";

        if(newTotal.substr(newTotal.lastIndexOf('.')+1,2).length < 2){
            newTotal += "0";
        }
        $('#total .value').html(newTotal);
    }

    function addToCart(){
        var wish = $(this).attr('rel');
        $.post("/cart/add",{
            "wish":wish
        });
        $(this).hide();
        $(this).unbind('click');

        var wishElem = "#wish" + $(this).attr('rel');

        $(wishElem).fadeTo(300,0).slideUp(600,function(){
            showWishInCart(wishElem);
        });
    }

    function showWishInCart(wishElem){
        if($('#cart .wish').size()==0){
            $('#emptyMessage').hide();
            $('#cart .wishes').show();
            $('#cart .allWishes').css('left','0px');
        }

        $(wishElem+" .addToCart").removeClass('addToCart').addClass('removeFromCart');
        $(wishElem+" .removeFromCart a.active").html('Remove')
            .bind('click',removeFromCart).show();
        if($('#cart .wish').size() != 0){
            $(wishElem).appendTo('#cart .allWishes').slideDown(600);
            cartScrollRight(false,function(){
                $(wishElem).fadeTo(300,1);
            },true);
        }else{
            $(wishElem).appendTo('#cart .allWishes').slideDown(600).fadeTo(300,1);
            var totalWishes = 1;
            $('#cart .pages .current').html(totalWishes);
            $('#cart .pages .total').html(totalWishes);
        }

        reCalculateTotal();
    }

    function showWishInList(wishElem){
        $(wishElem+" .removeFromCart").removeClass('removeFromCart').addClass('addToCart');
        $(wishElem+" .addToCart a.active").html('Grant Wish')
            .bind('click',addToCart).show();
		var lastWish = $('#cart .wish').size()==1;

        $(wishElem).prependTo('#wishlist .wishes').slideDown(600).fadeIn(300);

        if(lastWish){
            $('#cart .pages .current').html('0');
            $('#cart .wishes').slideUp(600);
            $('#emptyMessage').fadeTo(300,1);
            $('#total .value').html('0.00');
        }else{
            reCalculateTotal();
        }
    }

    function removeFromCart(){
		 
        var wish = $(this).attr('rel');
        $.post("/cart/remove",{
            "wish":wish
        });
        $(this).hide();
        $(this).unbind('click');

        var wishElem = "#wish" + $(this).attr('rel');
        $(wishElem).fadeOut(600);
        var currentWish = parseInt($('#cart .pages .current').html());
        var totalWishes = parseInt($('#cart .wish').size());
        $('#cart .pages .total').html(--totalWishes);
        if(currentWish == $('#cart .wish').size()){
				var left_s=$('#cart .allWishes').css('left');
				var f=0;
				var count_left=0;
				var total_wishes=$('.allWishes .wish').each(function(){
						count_left=parseInt(count_left)-425;
						f=parseInt(f)+1
				});
				 /*alert(f); 
				 alert(count_left);*/
    			setTimeout(function(){ 
					count_left=parseInt(count_left)+425;
					if(parseInt(left_s)==parseInt(count_left)){
						/*alert(parseInt(left_s));*/
						var total_left = parseInt(left_s)+425;
						$('#cart .allWishes').css('left',total_left+'px');
					}
				},1500);
            cartScrollLeft(false,function(){
                $('#cart .allWishes').css({'left':'-=425px'});
                showWishInList(wishElem);
            });
        }
        else{
            var originalLeft = $('#cart .allWishes').css('left');
            cartScrollRight(false,function(){
				var left_s=$('#cart .allWishes').css('left');
				var f=0;
				var count_left=0;
				var total_wishes=$('.allWishes .wish').each(function(){
						count_left=parseInt(count_left)-425;
						f=parseInt(f)+1
				});
				 /*alert(f); 
				 alert(count_left);*/

				 
				setTimeout(function(){ 
					count_left=parseInt(count_left)+425;
					if(parseInt(left_s)==parseInt(count_left)){
						/*alert(parseInt(left_s));*/
						var total_left = parseInt(left_s)+425;
						$('#cart .allWishes').css('left',total_left+'px');
					}
				},1500);
				
                showWishInList(wishElem);
                $('#cart .allWishes').css({'left': originalLeft});
            },false,true);
        }
        
         
       
    }


    function cartScrollLeft(event,callback){
        callback = callback || function(){};

        var currentWish = parseInt($('#cart .pages .current').html());
        if(currentWish <= 1){
            return callback();
        }
        $('#cart .allWishes').animate({"left": "+=425px"}, 600,callback);
        $('#cart .pages .current').html(currentWish - 1);
    }

    function cartScrollRight(event,callback,addWish,removeWish){
        callback = callback || function(){};
        addWish = addWish || false;
        removeWish = removeWish || false;

        var currentWish = parseInt($('#cart .pages .current').html());
        var totalWishes = parseInt($('#cart .wish').size());
        if(currentWish >= totalWishes){
            return;
        }
        if(addWish){
            var animateBy = (totalWishes-currentWish)*425;
            $('#cart .allWishes').animate({"left": "-="+animateBy+"px"}, 600,callback);
            $('#cart .pages .current').html(totalWishes);
            $('#cart .pages .total').html(totalWishes);
        }else{
            $('#cart .allWishes').animate({"left": "-=425px"}, 600,callback);
            if(!removeWish){
                $('#cart .pages .current').html(currentWish + 1);
            }
        }
    }

    function fieldFocus(){
        var speed = 200;
        $('#cart .error').hide();
        var noInput = $(this).val() == 'First Name' || $(this).val() == 'Last Name' ||
            $(this).val() == 'Email Address';

        if (noInput){
            $(this).animate({color: '#ffffff'}, speed, function() {
                $(this).val('');
                $(this).css('color', '#000000');
            });
        }

        var newInput = ($(this).val() != 'First Name' || $(this).val() == 'Last Name' ||
            $(this).val() != 'Email Address');

        if (newInput){
            $(this).animate({color: '#000000'}, speed);
        }
    }


    function fieldBlur(){
        var speed = 200;
        var noInput = $(this).val() == 'First Name' || $(this).val() == 'Last Name' ||
            $(this).val() == 'Email Address';

        if(noInput){
            $(this).stop();
            $(this).css('color', '#AAA');
        }
        else if ($(this).val() != ''){
            $(this).animate({
                color: '#333'
            }, speed);
        }
        else if ($(this).val() == ''){
            $(this).stop();
            $(this).css('color', '#ffffff');

            if ($(this).attr('id') == 'fname') {
                $(this).val('First Name');
            }
            if ($(this).attr('id') == 'lname') {
                $(this).val('Last Name');
            }
            if ($(this).attr('id') == 'email') {
                $(this).val('Email Address');
            }

            $(this).animate({color: '#AAA'}, speed);
        }
    }
});


function validate(){
    var error = false;
    var errorElems = new Array();
    var emailFilter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    var email = $('#email').val();

    if(!emailFilter.test(email)){
        error = true;
        $('#cart .error').html('Please enter a valid Email');
        errorElems.push('email');
    }

    if($('#lname').val() == 'Last Name'||$('#lname').val().length==0){
        error = true;
        $('#cart .error').html('Please enter your Last name');
        errorElems.push('lname');
    }

    if($('#fname').val() == 'First Name'||$('#fname').val().length==0){
        error = true;
        $('#cart .error').html('Please enter your First Name');
        errorElems.push('fname');
    }

    if($('#cart .wish').size()==0){
        error = true;
        errorElems.push('cart');
        $('#cart .error').html('Please add a wish to the cart');
    }

    if(error){
        $('#cart .error').show();
    }

    return !error;
}

