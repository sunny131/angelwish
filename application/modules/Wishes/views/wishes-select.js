
var currentPage = 1;
var loadingNewPage = false;
var sort_order= 'ASC';
var sort_type = 'age';

function checkPaymentProcessor(processor){
	var _PaymentProcessor=$("#paymentProcessor").val();
	if($("#fname").val()!='' && $("#lname").val()!='' && $("#email").val()!=''){
		$("#paymentProcessor").val(processor);
	}
}



$(function() {
 
    $('#cart').css({'top': 0,
        'left': ($('#wishlist').width())
    }).show();

    var cartTop = 0;
    
    $('.wish .addToCart a.active').each(function(index,anchor){
        $(anchor).bind('click',addToCart);
    });

    $('.wish .removeFromCart a.active').each(function(index,anchor){
        $(anchor).bind('click',removeFromCart);
    });

    
    $(window).bind('scroll resize',function(){
        stickyCart();
        moreWishes();
    });
    
    stickyCart();
    moreWishes();
    
    $('.cart-form').focus(fieldFocus);
    $('.cart-form').blur(fieldBlur);
    $('.leftArrow').click(cartScrollLeft_click);
    $('.rightArrow').click(cartScrollRight_click);
    $('#support').click(reCalculateTotal);
    $('#cash').change(reCalculateTotal);  
    
    
    
    
    function stickyCart(){
        var is_mobile = <?=$is_mobile?>;
        if(!is_mobile && $(window).height() > $('#cart').height()){
            if( $(window).scrollTop() > ($('#wishes-container').offset().top - 15) ){
                $('#cart').addClass('fixed');
                $('#cart').css({'top':15});
                var cartLeft = $('#wishlist').width() + $('#wishlist').offset().left;
                $('#cart').css({'left':  cartLeft});
            }
            else{
                $('#cart').removeClass('fixed');
                $('#cart').css({'top':cartTop});
                var cartLeft = $('#wishlist').width();
                $('#cart').css({'left':  cartLeft});
            }
        }
    }

    function moreWishes(){
        var seenHeight = $(window).scrollTop() + $(window).height();
        var seenPercent = (seenHeight*100)/$(document).height();

        if(seenPercent > 95 && !loadingNewPage){
            loadingNewPage = true;
            $.post('/wishes/more',{page:++currentPage,type:sort_type,orderby:sort_order},function(data){
               $('#wishlist .wishes').append(data);
               loadingNewPage = false;
               $('.wish .addToCart a.active').each(function(index,anchor){
                    $(anchor).bind('click',addToCart);
                });
            });
        }
    }

    function reCalculateTotal(){
        var newTotal = 0;
        $('#cart .wish').each(function(index,element){
            newTotal += parseFloat($(element).find('.price .value').html());
        });
        var support = $('#support').is(':checked');
        if(support){
            newTotal *= 1.15;
        }

        var cash = $('#cash').val();
 	if(!cash.match(/^\s*$/)) {
 	     if(cash.match(/^[0-9]+(\.[0-9]+)?$/)) {
 	         newTotal += 1*cash;
 	     }
 	}
        newTotal = Math.round(newTotal*100)/100;
        newTotal += "";

        var numDecimals = newTotal.substr(newTotal.lastIndexOf('.') +1,2).length;
 	if(newTotal.lastIndexOf('.') == -1){
 	    newTotal += ".00";
        } else if(numDecimals == 1 && newTotal.lastIndexOf('.') != -1) {
 	    newTotal += "0";
 	}
        $('#total .value').html(newTotal);
    }
    
    function addToCart(){
        var wish = $(this).attr('rel');
        var qty = $("#qty_"+wish).val();
        $.post("/cart/add",{
            "wish":wish,
            "qty":qty
        });
        $(this).hide();
        $(this).unbind('click');
        insertGoogleAnalytics('Button', 'Ad to Cart - WishId', 'Ad to Cart - WishId',wish, 2); 
        var wishElem = "#wish" + $(this).attr('rel');
        
        $(wishElem).fadeTo(300,0).slideUp(600,function(){
            showWishInCart(wishElem);
        });
    }

    function showWishInCart(wishElem){
        if($('#cart .wish').size()==0){
            $('#emptyMessage').hide();
            $('#cart .wishes').show();
            $('#cart .allWishes').css('left','0px');
        }
        
        $(wishElem+" .addToCart").removeClass('addToCart').addClass('removeFromCart');
        $(wishElem+" .padding7 select").hide();
        $(wishElem+" .padding7 span").html($(wishElem+" .padding7 select").val());
        $(wishElem+" .padding7 span").show();
        $(wishElem+" .removeFromCart a.active").html('Remove').bind('click',removeFromCart).show();
        if($('#cart .wish').size() != 0){
            $(wishElem).appendTo('#cart .allWishes').slideDown(600);
            cartScrollRight(false,function(){
                $(wishElem).fadeTo(300,1);
            },true);
        }else{
            $(wishElem).appendTo('#cart .allWishes').slideDown(600).fadeTo(300,1);
            var totalWishes = 1;
            $('#cart .pages .current').html(totalWishes);
            $('#cart .pages .total').html(totalWishes);
        }

        reCalculateTotal();
    }

    function showWishInList(wishElem){
        $(wishElem+" .removeFromCart").removeClass('removeFromCart').addClass('addToCart');
        $(wishElem+" .padding7 select").show();
        $(wishElem+" .padding7 span").hide();
        $(wishElem+" .addToCart a.active").html('Grant Wish').bind('click',addToCart).show();

        var lastWish = $('#cart .wish').size()==1;
        
        $(wishElem).prependTo('#wishlist .wishes').slideDown(600).fadeIn(300);

        if(lastWish){
            $('#cart .pages .current').html('0');
            $('#cart .wishes').slideUp(600);
            $('#emptyMessage').fadeTo(300,1);
        }
        reCalculateTotal();
    }

    function removeFromCart(){
        var wish = $(this).attr('rel');
        $.post("/cart/remove",{
            "wish":wish
        });
        $(this).hide();
        $(this).unbind('click');
        insertGoogleAnalytics('Button', 'Remove from Cart - WishId', 'Remove from Cart - WishId',wish, 3); 
        var wishElem = "#wish" + $(this).attr('rel');
        $(wishElem).fadeOut(600);
        var currentWish = parseInt($('#cart .pages .current').html());
        var totalWishes = parseInt($('#cart .wish').size());
        
        $('#cart .pages .total').html(--totalWishes);
        if(currentWish == $('#cart .wish').size()){
			/*alert($('#cart .allWishes').css('left'));*/
            cartScrollLeft(false,function(){
                $('#cart .allWishes').css({'left':'-=425px'});
                showWishInList(wishElem);
            });
        }
        else{
            var originalLeft = $('#cart .allWishes').css('left');
            cartScrollRight(false,function(){
               showWishInList(wishElem);
                $('#cart .allWishes').css({'left': originalLeft});
            },false,true);
        } 
        
        var left_s=$('#cart .allWishes').css('left');
        var f=0;
        var count_left=0;
        var total_wishes=$('.allWishes .wish').each(function(){
				count_left=parseInt(count_left)-425;
				f=parseInt(f)+1
		});
		 
		count_left=parseInt(count_left)+425;
		setTimeout(function(){ 
			if(parseInt(left_s)==parseInt(count_left)){
				var total_left = parseInt(left_s)+425;
				$('#cart .allWishes').css('left',total_left+'px');
			}
		},2000);
		
    }

    
    function cartScrollLeft(event,callback){
        callback = callback || function(){};
        
        var currentWish = parseInt($('#cart .pages .current').html());
        if(currentWish <= 1){
            return callback();
        }
        
        $('#cart .allWishes').animate({"left": "+=425px"}, 600,callback);
        $('#cart .pages .current').html(currentWish - 1);
    }

    function cartScrollLeft_click(event,callback){
        callback = callback || function(){};

        var currentWish = parseInt($('#cart .pages .current').html());
        if(currentWish <= 1){
            return callback();
        }

        $('#cart .allWishes').animate({"left": "+=425px"}, 600,callback);
        $('#cart .pages .current').html(currentWish - 1);
    }

    function cartScrollRight(event,callback,addWish,removeWish){
        callback = callback || function(){};
        addWish = addWish || false;
        removeWish = removeWish || false;

        var currentWish = parseInt($('#cart .pages .current').html());
        var totalWishes = parseInt($('#cart .wish').size());
        if(currentWish >= totalWishes){
            return;
        }
        if(addWish){
            var animateBy = (totalWishes-currentWish)*425;
            $('#cart .allWishes').animate({"left": "-="+animateBy+"px"}, 600,callback);
            $('#cart .pages .current').html(totalWishes);
            $('#cart .pages .total').html(totalWishes);
        }else{
            $('#cart .allWishes').animate({"left": "-=425px"}, 600,callback);
            if(!removeWish){
                $('#cart .pages .current').html(currentWish + 1);
            }
        }
    }

    function cartScrollRight_click(event,callback,addWish,removeWish){
        callback = callback || function(){};
        addWish = addWish || false;
        removeWish = removeWish || false;

        var currentWish = parseInt($('#cart .pages .current').html());
        var totalWishes = parseInt($('#cart .wish').size());
        if(currentWish >= totalWishes){
            return;
        }
        if(addWish){
            var animateBy = (totalWishes-currentWish)*425;
            $('#cart .allWishes').animate({"left": "-="+animateBy+"px"}, 600,callback);
            $('#cart .pages .current').html(totalWishes);
            $('#cart .pages .total').html(totalWishes);
        }else{
            $('#cart .allWishes').animate({"left": "-=425px"}, 600,callback);
            if(!removeWish){
                $('#cart .pages .current').html(currentWish + 1);
            }
        }
    }

    function fieldFocus(){
        var speed = 200;
        $('#cart .error').hide();
         if ($(this).attr('id') == 'fname') {
			if($("#fname").val()=='' || $("#fname").val()=="First Name"){
				$("#fname").val('');
				$("#fname").css('color', '#000000');
			}
		}
		if ($(this).attr('id') == 'lname') {
			if($("#lname").val()=='' || $("#lname").val()=='Last Name' ){
				$("#lname").val('');
				$("#lname").css('color', '#000000');
			}
		}
		if ($(this).attr('id') == 'email') {
			if($("#email").val()=='' || $("#email").val()=='Email Address' ){
				$("#email").val('');
				$("#email").css('color', '#000000');
			}
		}
		
       /* var noInput = $(this).val() == 'First Name' || $(this).val() == 'Last Name' ||
            $(this).val() == 'Email Address';

        if (noInput){
            $(this).animate({color: '#ffffff'}, speed, function() {
                $(this).val('');
                $(this).css('color', '#000000');
            });
        }

        var newInput = ($(this).val() != 'First Name' || $(this).val() == 'Last Name' ||
            $(this).val() != 'Email Address');

        if (newInput){
            $(this).animate({color: '#000000'}, speed);
        }*/
    }


    function fieldBlur(){
		
		if ($(this).attr('id') == 'fname') {
			if($("#fname").val()==''){
				$("#fname").val('First Name');
				$("#fname").css('color', '#ccc');
			}
		}
		if ($(this).attr('id') == 'lname') {
			if($("#lname").val()==''){
				$("#lname").val('Last Name');
				$("#lname").css('color', '#ccc');
			}
		}
		if ($(this).attr('id') == 'email') {
			if($("#email").val()==''){
				$("#email").val('Email Address');
				$("#email").css('color', '#ccc');
			}
		}
    }
});

function sort(type){
   currentPage = 1;
   var order   = '';
   order       = $('#order').val();   
   if(order == 'DESC'){
     $('#order').val('ASC');
     if(type == 'age'){
         $('#age').removeClass('top');
         $('#age').addClass('bottom');
      }
      if(type == 'price'){
         $('#price').removeClass('top');
         $('#price').addClass('bottom');
      }
   }
   else if(order == 'ASC')
   {
      $('#order').val('DESC');
      if(type == 'age'){
         $('#age').removeClass('bottom');
         $('#age').addClass('top');
      }
      if(type == 'price'){
         $('#price').removeClass('bottom');
         $('#price').addClass('top');
      }
   }
   else
   {
     $('#order').val('ASC');
     order = 'ASC';
   }
   sort_type = type;
   sort_order = order;
   $.post('/wishes/more',{page:1,type:type,orderby:order},function(data){
       $('#wishlist .wishes').html(data);
       loadingNewPage = false;
    });
}

function validate(){
    var error = false;
    var errorElems = new Array();
    var emailFilter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    var email = $('#email').val();

    if(!emailFilter.test(email)){
        error = true;
        $('#cart .error').html('Please enter a valid Email');
        errorElems.push('email');
    }

    var cash = $('#cash').val();
    if(!cash.match(/^\s*$/)) {
        if(!cash.match(/[0-9]+(\.[0-9]+)?/)) {
            error = true;
            $('#cart .error').html('Please enter a valid amount for donation.');
            errorElems.push('cash');
        }
    }

    if($('#lname').val() == 'Last Name'||$('#lname').val().length==0){
        error = true;
        $('#cart .error').html('Please enter your Last name');
        errorElems.push('lname');
    }

    if($('#fname').val() == 'First Name'||$('#fname').val().length==0){
        error = true;
        $('#cart .error').html('Please enter your First Name');
        errorElems.push('fname');
    }

    if($('#cart .wish').size()==0 && Math.round($('#cash').val()) == 0){
        error = true;
        errorElems.push('cart');
        $('#cart .error').html('Please add a wish to the cart or enter a donation amount'); 
    }
    
    if(error){
        $('#cart .error').show();
    }

    return !error;
}
