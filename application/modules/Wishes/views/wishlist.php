<style>
.leftCol{ 
	width:330px !important;
}
.rightCol{
	margin-left:0 !important;
}
.padding7{
	padding-right:7px;
}</style>

<?php 
$this->load->library('session');
$wishss=$this->session->userdata('cart_wishes');

foreach ($wishes as $wish) { 
	?>
    <div class="wish" id="wish<?=$wish['id']?>" >
        <div class="leftCol">
            <div class="productImage">
                <img src="<?=$wish['image']?>" alt="" width="80"/>
            </div>
            <div class="description">
                <div class="productDetails">
					<h4><?=  shortenString($wish['bookName'])?></h4>
                </div>
	                <?php if($cid > 700) { ?>
				<div class="child">
                </div>
                <?php } else { ?>
                <div class="child">
				<input type="hidden" name="wish_id" id="wish_id" value="<?=$wish['id']?>" />
				<input type="hidden" name="child_name" id="child_name" value="<?=trim($wish['childName'])?>" />
				<input type="hidden" name="child_age" id="child_age" value="<?=$wish['age']?>" />
                    <span><?=trim($wish['childName'])?>, </span>
					<span>
                    <?php if($wish['age'] != 0) {
                        echo $wish['age']. " years old";
                     } else { ?>
                        newborn
                    <?php } ?>
                    </span>
<!--                    <div><span class="gray">Poverty level:</span> <?=$wish['poverty']?></div>-->
                </div>
                <?php }?>
            </div>
        </div>
        <div class="rightCol padding7">
        <?php $qtyy=1; foreach($wishss as $wishs){
			if($wish['id']==$wishs['id']){
					$qtyy= $wish['qty'];
				}
			}
		?>
		<?php if(isset($inCart) && $inCart){ 
			echo '<span>'.$qtyy.'</span>';
		?>
		
			<select style="display:none;" id="qty_<?=$wish['id']?>" name="qty_<?=$wish['id']?>" onchange="price_change('<?=$wish['id']?>',this.value)">
				<?php for($i=1;$i<=10;$i++) { ?>
				<option <?php if($qtyy==$i){ echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
				<?php } ?>
			</select>
			
		<?php	}else{ 
			echo '<span style="display:none">'.$qtyy.'</span>';
			?>
			<select id="qty_<?=$wish['id']?>" name="qty_<?=$wish['id']?>" onchange="price_change('<?=$wish['id']?>',this.value)">
				<?php for($i=1;$i<=10;$i++) { ?>
				<option <?php if($qtyy==$i){ echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
				<?php } ?>
			</select>
		<?php } ?>
        </div>
        <div class="rightCol">
            <div class="price" id="price_<?=$wish['id']?>">$<span class="value"><?=sprintf("%01.2f", $wish['price'])?></span></div>
            <input type="hidden" id="or_pr_<?=$wish['id']?>" value="<?=sprintf("%01.2f", $wish['price'])?>">
            <div class="<?if($inCart){?>removeFromCart<?}else{?>addToCart<?}?>">
                <a href="javascript:void(0);" rel="<?=$wish['id']?>" class="active">
                    <?if($inCart){?>Remove<?}else{?>Grant Wish<?}?>
                </a>
            </div>
        </div>
        <br class="clear"/>
    </div>
<?}?>
<script type="text/javascript">
    <?=$js?>
    function price_change(id,qty){
		var pr=$("#or_pr_"+id).val();
		var new_pr= parseFloat(pr)*parseInt(qty);
		new_pr=new_pr.toFixed(2); 
 		$("#price_"+id+" span").html(new_pr);
	}
    $(document).ready(function(){
		
	});
</script>
    
    
