<?foreach ($wishes as $wish) {?>
    <div class="wish" id="wish<?=$wish['id']?>">
        <div class="leftCol">
            <div class="productImage">
                <img src="<?=$wish['image']?>" alt="<?=$wish['bookName']?>" width="80"/>
            </div>
            <div class="description">
                <div class="productDetails">
                    <h4><?=  shortenString($wish['bookName'])?></h4>
                </div>
                <div class="child">
                    <span><?=trim($wish['childName'])?>, </span>
                    <span>
                    <?php if($wish['age'] != 0) {
                        echo $wish['age']. " years old";
                     } else { ?>
                        newborn
                    <?php } ?>
                    </span>
<!--                    <div><span class="gray">Poverty level:</span> <?=$wish['poverty']?></div>-->
                </div>
            </div>
        </div>
        <div class="rightCol">
            <div class="price">$<span class="value"><?=sprintf("%01.2f", $wish['price'])?></span></div>
            <div class="<?if($inCart){?>removeFromCart<?}else{?>addToCart<?}?>">
                <a href="javascript:void(0);" rel="<?=$wish['id']?>" class="active">
                    <?if($inCart){?>Remove<?}else{?>Grant Wish<?}?>
                </a>
            </div>
        </div>
        <br class="clear"/>
    </div>
<?}?>
<script type="text/javascript">
    <?=$js?>
</script>
    
    