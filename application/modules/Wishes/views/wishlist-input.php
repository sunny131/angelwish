<style>
.leftCol{ 
	width:330px !important;
}
.rightCol{
	margin-left:0 !important;
}
.padding7{
	padding-right:7px;
}
</style>

<?php 
$this->load->library('session');
$wishss=$this->session->userdata('cart_wishes');

foreach ($wishes as $wish) { 
	?>
    <div class="wish" id="wish<?=$wish['id']?>" >
        <div class="leftCol">
            <div class="productImage">
                <img src="<?=$wish['image']?>" alt="<?=$wish['bookName']?>" width="80"/>
            </div>
            <div class="description">
                <div class="productDetails">
					<h4><?=  shortenString($wish['bookName'])?></h4>
                </div>
	                <?php if($cid > 700) { ?>
				<div class="child">
                </div>
                <?php } else { ?>
                <div class="child">
			
				<input type="hidden" name="wish_id" id="wish_id" value="<?=$wish['id']?>" />
				<input type="hidden" name="child_name" id="child_name" value="<?=trim($wish['childName'])?>" />
				<input type="hidden" name="child_age" id="child_age" value="<?=$wish['age']?>" />
                    <span class="childname"><?=trim($wish['childName'])?></span>,
					<span class="childage">
                    <?php if($wish['age'] != 0) {
                        echo $wish['age'];
                     } else { ?>
                        newborn
                    <?php } ?>
                    </span>years old
		<?php if ($this->session->userdata('loggedin')){ echo '<a href="javascript:void(0);" class="edit_me"><img src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-1/16/edit-icon.png" /></a>'; } ?>
    
		
                </div>
                <?php }?>
            </div>
        </div>	
        <div class="rightCol padding7">
        <?php $qtyy=1; /*if(!isset($qtyy)) $qtyy=100;*/ foreach($wishss as $wishs){
			if($wish['id']==$wishs['id']){
					$qtyy= $wish['qty'];
				}
			}
		?>
		<?php if(isset($inCart) && $inCart){ 
			echo '<span>'.$qtyy.'</span>';
		?>
		
			<input type="text" value="<?=$qtyy?>" size="2" style="display:none;" id="qty_<?=$wish['id']?>" name="qty_<?=$wish['id']?>" onblur="price_change('<?=$wish['id']?>',this.value)">
		<?php	}else{ 
			echo '<span style="display:none">'.$qtyy.'</span>';
			?>
			<input type="text" value="<?=$qtyy?>" size="2" style="display:none;" id="qty_<?=$wish['id']?>" name="qty_<?=$wish['id']?>" onkeyup="return qtty('<?=$wish['id']?>',this.value)" onblur="price_change('<?=$wish['id']?>',this.value)">
		<?php } ?>
        </div>
        <!--style="text-align:right; float:right; padding-right: 20px"-->
        <div class="rightCol" >
            <div class="price" id="price_<?=$wish['id']?>">$<span class="value"><?=sprintf("%01.2f", $wish['price']*$qtyy)?></span></div>
            <input type="hidden" id="or_pr_<?=$wish['id']?>" value="<?=sprintf("%01.2f", $wish['price'])?>">
            <div class="<?if($inCart){?>removeFromCart<?}else{?>addToCart<?}?>">
                <a href="javascript:void(0);" rel="<?=$wish['id']?>" class="active">
                    <?if($inCart){?>Remove<?}else{?>Grant Wish<?}?>
                </a>
            </div>
        </div>
        <br class="clear"/>
    </div>
<?}?>
<script type="text/javascript">
    <?=$js?>
    function price_change(id,qty){
		var pr=$("#or_pr_"+id).val();
		var q=parseInt(qty);
		if(isNaN(q)){
			$("#qty_"+id).val('1');
			qty=1;
		}
		var new_pr= parseFloat(pr)*parseInt(qty);
		new_pr=new_pr.toFixed(2); 
		
 		$("#price_"+id+" span").html(new_pr);
	}
	function qtty(id, qty){
		var pr=$("#or_pr_"+id).val();
		var q=parseInt(qty);
		if(qty!='' && isNaN(q)){
			$("#qty_"+id).val('1');
			qty=1;
		}
		var new_pr= parseFloat(pr)*parseInt(qty);
		
		if(isNaN(new_pr))
			$("#price_"+id+" span").html('0.00');
		else{
			new_pr=new_pr.toFixed(2); 
			$("#price_"+id+" span").html(new_pr);
		}
	}
	

		$('.edit_me').live('click',function(){
			var childName=$.trim($(this).closest('div').find('#child_name').val());
			var childAge=$.trim($(this).closest('div').find('#child_age').val());
				if ($.trim($(this).closest('div').find('.childname').text())!=childName) {
				$(this).closest('div').find('.childname').html(childName);
				$(this).closest('div').find('.childage').html(childAge);
				}else{
				console.log(childName);
				$(this).closest('div').find('.childname').html('<input type="text" id="child_name_input" name="child_name" value="'+childName+'" style="width:90px" /> ');
				$(this).closest('div').find('.childage').html('<input type="text" id="child_age_input" name="child_age" value="'+childAge+'" style="width:23px" /> ');
				}
		});

			
		$('#child_name_input').live('blur',function() {
			var childName=$.trim($(this).closest('div').find('#child_name').val());
			var childAge=$.trim($(this).closest('div').find('#child_age').val());
		$.post( "/wishes/update/", { name: $(this).val(), action:"update_name",mwish_id: $.trim($(this).closest('div').find('#wish_id').val()) })
		.done(function( data ) {

		});
		$(this).closest('div').find('#child_name').val($(this).val());
		
		$(this).closest('div').find('.childname').html($(this).val());

		$(this).closest('div').find('.childage').html(childAge);
		});
		
		$('#child_age_input').live('blur',function() {
			var childName=$.trim($(this).closest('div').find('#child_name').val());
			var childAge=$.trim($(this).closest('div').find('#child_age').val());
			
		$.post("/wishes/update/", { name: $(this).val(), action:"update_age",mwish_id: $.trim($(this).closest('div').find('#wish_id').val()) })
		.done(function( data ) {

		});
		$(this).closest('div').find('#child_age').val($(this).val());
				$(this).closest('div').find('.childname').html(childName);
				$(this).closest('div').find('.childage').html($(this).val());

		});

</script>
    
    
