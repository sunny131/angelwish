<?php

class Amazonipn extends MX_Controller {
    private $signature;
    private $tokenId;
    private $status;
    private $callerReference;

    function index()
    {
        parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $request);
        $temp = $this->load->model('Amazon');
        $this->Amazon->saveRequest($request);
        
        /*var_dump($_SERVER);
        $this->postbackFromAmazon();
        var_dump($this->signature,$this->tokenId,$this->status,$this->callerReference);*/
    }

    private function postbackFromAmazon(){
        parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
        $this->signature = $this->input->get('signature',true);
        $this->tokenId = $this->input->get('tokenID',true);
        $this->status= $this->input->get('status',true);
        $this->callerReference= $this->input->get('callerReference',true);
    }

}