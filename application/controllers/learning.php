<?php
require_once(APPPATH.'libraries/AmazonPayments.php');
class Learning extends MX_Controller {

	function index()
	{
            $this->load->model('Carecenters');
            $this->load->model('musers');
			$this->load->library('session');
			if($_POST){
				 
				$this->amazonpayments = AmazonPayments::getInstance();
				$total=$_POST['amount'];
				if($this->session->userdata('source')){
					$companyId=$this->session->userdata('source');
				}else{
					$companyId='';
				}
				$office='';
				$state=$_POST['state'];
				$fname=$_POST['fname'];
				$lname=$_POST['lname'];
				$city=$_POST['city'];
				$email=$_POST['email'];
				$zipcode=$_POST['zipcode'];
				$address=$_POST['address'];
				
				$address_a=array("city"=>$city,'state'=>$state,'zipcode'=>$zipcode,'address'=>$address);
				
				$userId = $this->musers->addOrUpdate($fname,$lname,$email,'donor',round($total,2,PHP_ROUND_HALF_UP),0,$address_a);
				 
				$checkoutUrl = $this->amazonpayments->generateAmazonURL($share='N',$userId,$total,
                "Angelwish Learning to Give Donation", $companyId, $office, '' );
                header('Location: '.$checkoutUrl);
                die;
			} 
            $data['css'] = $this->load->view('learning.css', NULL, TRUE);
            $this->template->view('learning',$data);
	}

}
