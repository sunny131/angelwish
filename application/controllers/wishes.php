<?php

class Wishes extends MY_Controller {

	function index(){
            $this->load->model('mwishes');
            $params=array(null,null,'http://localhost/dwollapayment/offsiteGateway.php');
            $this->load->library('DwollaRestClient', $params);
            $temp['wishes'] = $this->mwishes->getWishes();
            $temp['inCart'] = FALSE;
            $data['wishes'] = $this->load->view('wishlist',$temp,TRUE);

            $temp['wishes'] = $this->cart->getWishes();
            $temp['inCart'] = TRUE;
            $data['inCartWishes'] = $this->load->view('wishlist',$temp,TRUE);
			
            $data['cartHasWishes'] = !$this->cart->isEmpty();
            $data['cartTotalWishes'] =  $this->cart->count();
            $data['total'] = ($this->cart->total())*1.15;

            $data['fname'] = ($this->session->userdata('fname')) ?
                    $this->session->userdata('fname') : 'First Name';
            $data['lname'] = $this->session->userdata('lname') ?
                    $this->session->userdata('lname') : 'Last Name';
            $data['email'] = $this->session->userdata('email') ?
                    $this->session->userdata('email') : 'Email Address';

            $js['is_mobile'] = $this->agent->is_mobile() ? 'true' : 'false';
            
            $data['js'] = $this->load->view('wishes.js', $js, TRUE);
            $data['css'] = $this->load->view('wishes.css', NULL, TRUE);
            
            $this->template->view('wishes',$data);
	}

    function more(){
        $page = $this->input->get_post('page');
        $page = is_nan($page) ? 2 : $page;

        $this->load->model('mwishes');

        $temp['wishes'] = $this->mwishes->getWishes($page);
        $temp['inCart'] = FALSE;
        $data['wishes'] = $this->load->view('wishlist',$temp);
    }
        function update(){
	$this->load->model('mwishes');
	$name    = $this->input->get_post('name');
	$action    = $this->input->get_post('action');
	$mwish_id    = $this->input->get_post('mwish_id');
	
	$mode=$this->mwishes->update_wishes($name,$action,$mwish_id);
    }

}
