<?php

class Carecenter extends MX_Controller {

    function index($id = NULL){
        if(empty($id)){
            initializeGet();
            $id = $this->input->get('cid');
        }
        $rt = $this->load->model('Carecenters');
        $data['details']=$this->Carecenters->details($id);

        $data['wishAndBam'] = false;
        if($id == 36){
            $data['wishAndBam'] = true;
            $url = 'http://www.wishandbam.com/list/http%253A%252F%252Fwww.amazon.com%252Fexec%252Fobidos%252Fwishlist%252F2W55WWSQMA77I/20/rss';
            $data['wishlistItems'] = self::wishAndBamParser($url);
        }
        //$data['js'] = $this->load->view('carecenter.js', NULL, TRUE);
        $data['css'] = $this->load->view('carecenter.css', NULL, TRUE);
        $this->template->view('carecenter',$data);
    }

    function wishAndBamParser($url){
        $doc = new DOMDocument();
        $doc->load( $url );
        $wishlistItems = array();
        $this->load->library('amazon/cbui_singleuse_pipeline',array(self::ACCESS_KEY,self::SECRET_KEY));
        $pipeline =& $this->cbui_singleuse_pipeline;
        $items = $doc->getElementsByTagName( "item" );
        foreach($items as $item){
            $wishlistItem = array();
            $wishlistItem['title'] = $item->getElementsByTagName( "title" )->item(0)->nodeValue;

            $description = $item->getElementsByTagName( "description" )->item(0)->nodeValue;

            preg_match_all( "/\<span class=\"wlPrice\">(.*)\<\/span\>/", $description, $price );
            $wishlistItem['price'] = substr($price[1][0],1);

            preg_match_all( "/\<span class=\"authorPart\">(.*)\<\/span\>/", $description, $brand );
            $wishlistItem['brand'] = $brand[1][0];

            preg_match_all( "/\<span class=\"commentValueText\">(.*)\<\/span\>/", $description, $comment );
            $wishlistItem['comment'] = ucfirst($comment[1][0]);

            preg_match_all( "/\<img src=\"(.*)\" alt=\"Product Image\"/", $item->nodeValue, $image );
            $wishlistItem['image'] = $image[1][0];

            $pipeline->setMandatoryParameters("1",
                "http://dev2.gothamangel.org/amazontest", $wishlistItem['price']);
            $pipeline->addParameter("currencyCode", "USD");
            $pipeline->addParameter("paymentReason", $wishlistItem['title']);

            $wishlistItem['amazonUrl'] = $pipeline->getUrl();
            $wishlistItems[] = $wishlistItem;
        }
        
        return $wishlistItems;
    }

}