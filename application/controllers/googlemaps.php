<?php

class Googlemaps extends MX_Controller {

	function index()
	{
            $this->load->view('googlemaps',array());
	}

        function getmapdata(){
            $this->load->model('carecenters');
            $carecenters = $this->carecenters->getcarecentersformap();
            foreach($carecenters as &$carecenter){
                if(strlen($carecenter['description'])>100){
                    $carecenter['description'] = substr($carecenter['description'],0,100);
                    $carecenter['description'] .= "... <a href='/carecenter/index/".$carecenter['id']."'>more</a>";
                }
            }
            echo json_encode($carecenters);
        }
        function geocode(){
            $this->load->model('carecenters');
            $data['carecenters'] = $this->carecenters->getcarecenterAddresses();            
            $this->load->view('geocode',$data);
        }

        function updatelatlong(){
            $id = $this->input->get_post('id');
            $lat = $this->input->get_post('lat');
            $lng = $this->input->get_post('lng');

            $this->load->model('carecenters');
            $this->carecenters->updateLatitudeLongitude($id,$lat,$lng);
        }
}