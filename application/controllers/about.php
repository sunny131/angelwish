<?php

class About extends MX_Controller {

	function index()
	{
            $data['css'] = $this->load->view('about.css', NULL, TRUE);
            $data['js'] = $this->load->view('about.js',NULL,TRUE);
            $this->template->view('about',$data);

	}

}