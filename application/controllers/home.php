<?php
require_once(APPPATH.'libraries/AmazonPayments.php');
class Home extends MY_Controller {

    function index($source='') {
		$this->load->model('mwishes');
		$this->load->model('transactions');
		$this->load->library('session');
		$this->load->model('musers');
			
		if($_POST){
				 
				$this->amazonpayments = AmazonPayments::getInstance();
				$total=$_POST['amount'];
				$subscribe_post='';
				if(isset($_POST['subcribe_period'])){
					$subscribe_post=$_POST['subcribe_period'];
					
				}
				if($this->session->userdata('source')){
					$companyId=$this->session->userdata('source');
				}else{
					$companyId='';
				}
				$office='';
				$state=$_POST['state'];
				$fname=$_POST['fname'];
				$lname=$_POST['lname'];
				$city=$_POST['city'];
				$email=$_POST['email'];
				$zipcode=$_POST['zipcode'];
				$address=$_POST['address'];
				
				$address_a=array("city"=>$city,'state'=>$state,'zipcode'=>$zipcode,'address'=>$address);
				
				$userId = $this->musers->addOrUpdate($fname,$lname,$email,'donor',round($total,2,PHP_ROUND_HALF_UP),0,$address_a);
				
				$checkoutUrl = $this->amazonpayments->generateAmazonURL($share='N',$userId,$total,
                'Angelwish Donation', $companyId, $office, '','','',$subscribe_post);
                header('Location: '.$checkoutUrl);
                die;
			} 
		
		
		$host = $_SERVER['HTTP_HOST'];
		$dot_position = stripos($host,".");
		$company_url = $dot_position ? substr($host,0,$dot_position ) : false;
		if($company_url){   
		//echo  $source=$company_url;
		 
		if($company_url!='angelwish'){
			$ac=$this->mwishes->iscompanyactive($company_url);
			if($ac!=1){
			  //   
				
			}
		}
		 $companyIdID=$this->mwishes->getcompanyidbyurl($company_url);
		 if($companyIdID){
			 $source=$company_url;
			 }
		}
		 
		if($source!=''){
			$ac=$this->mwishes->iscompanyactive($source);
			
			 if($ac!=1){
				//header('location:http://www.sandbox.angelwish.org');
			 }else{
			$company_id=$this->mwishes->getcompanyidbyurl($source);
			
			if(!empty($company_id) || $company_id!=0 ){
				
				if($this->session->userdata('source')){
					if($this->session->userdata('source') != $company_id):
						$this->mwishes->updatetracknumber($company_id);
						$this->session->set_userdata('source',$company_id);
						$this->session->set_userdata('markup',$this->mwishes->getmarkup($company_id));
					endif;
				}else{
					$this->mwishes->updatetracknumber($company_id);
					$this->session->set_userdata('source',$company_id);
					$this->session->set_userdata('markup',$this->mwishes->getmarkup($company_id));
				}	
			
			}else{
				echo 'No Company found of this name '.$source; 
			}
			 }
		}
			
			 /*echo  $this->session->userdata('source') .'<br>';
			 echo  $this->session->userdata('markup') ;
			 die; */
		if( $this->session->userdata('source') ){
			$companyId=$this->session->userdata('source');
			$sql="select count(id) as total from tbl_comptrans where company_id='".$companyId."'";
			$res=$this->transactions->getdata($sql);
			if(isset($res['0']->total) && $res['0']->total>0){
				$data['total_entries']=$res['0']->total;
			}
		}
        $data['wishes'] = $this->mwishes->getWishes(rand(1,10),3);
        
        $data['css'] = $this->load->view('home.css', NULL, TRUE);
        $data['js'] = $this->load->view('home.js', NULL, TRUE);
        $this->template->view('home', $data);
    }

}
