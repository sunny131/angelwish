<?php

class Stories extends MX_Controller {

	function index()
	{
            $stories = array();
            $stories[] = array("content"=>'It is a real gift to be invited into a families experience, particularly at this extremely vulnerable time. It is also very humbling to be the liason for the community that is providing time, generosity & compassion for these often overlooked children and families. - <strong>Social Worker at the Jersey Shore Medical Center</strong>',"year"=>2012);
			$stories[] = array("content"=>'The movie theater gift cards have been great. one family got to see the kids\' first movie and one boy took his mother out on a "date" - <strong>Hackensack, New Jersey</strong>',"year"=>2009);
            $stories[] = array("content"=>'For many of our patients this is the only Christmas they get. This is either due to their parents being deceased or that there is no financial way to afford a holiday. These presents are anticipated by our patients yearly. - <strong>New York, New York</strong>',"year"=>2009);
            $stories[] = array("content"=>'We had a mom come and pick up toys from your program in the past and, with tears in her eyes, she thanked us for the items telling us that those were the only items she would be able to give her 5 kids that year - otherwise, they would have nothing on Christmas morning. She was so grateful for the small amount of items we were able to provide with the help from your program. - <strong>Memphis, Tennessee</strong>',"year"=>2009);
            $stories[] = array("content"=>'A single mother with HIV cried when her daughter was presented with the Angelwish gift, the Harry Potter gift set. She said that her daughter was a huge Harry Potter fan, and her daughter had wanted the books for a long time ans she was unable to provide them for her. Both mother and daughter were equally excited with the books. Another HIV Positive Patient was extremely grateful for the presents for her children, her husband was recently laid off and they were worrying about providing gifts for their young children. The entire family was elated by the presents the children received. It was exciting to see parents as excited as their children when gifts were open. - <strong>Morgantown, West Virginia</strong>',"year"=>2009);
            $stories[] = array("content"=>'Last year a generous donar donated an mp3 player to an HIV infected teen. He now listens to it whenever he has to go to the lab for blood draws (which he hates) and it relaxes him. Thank you! - <strong>Long Beach, California</strong>',"year"=>2009);
            $stories[] = array("content"=>'One of our clients said that he could not have given his kids any gifts last Christmas if not for the Angelwish program! He is a single father struggling with health issues and sincerely appreciates the help (as do we!) - <strong>Billings, Montana</strong>',"year"=>2008);
            $stories[] = array("content"=>'Angelwish provided us with a number of kits for collecting bugs as well as a butterfly farm. On one of our service evenings we held a nature themed evening with a trip to the local park. One of the children was particularly fascinated with bugs and was very intrigued with what he discovered. We are looking forward to presenting him with the kit at the Christmas party. I think we may have a future botanist on our hands! - <strong>London, United Kingdom</strong>',"year"=>2008);
            $stories[] = array("content"=>'Every gift we give makes a difference to the children in terms of self respect. Many have never been given anything of good quality before. A highlight last year is about a family we have been working with for years, some of the children in the family are positive some are not. The family are just getting out of destitution and have been living hand to mouth for years. We were able to give each child a good quality christmas gift last year. This raised their self esteem and self worth considerably as they were able to go into school after christmas and tell their classmates what they got for christmas for the first time in their lives - <strong>Manchester, United Kingdom</strong>',"year"=>2009);

            $data['stories'] = $stories;
            
            $data['css'] = $this->load->view('stories.css', NULL, TRUE);
            $this->template->view('stories',$data);
              
	}

}