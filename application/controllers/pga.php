<?php

class pga extends MX_Controller {

	function index()
	{
			$this->load->model('user');
			$setting=$this->user->getsettingbyname('pga');
			if($setting->status=='Y' && $setting->settingValue != ''){
				header('location:'.$setting->settingValue);
			}
			
            $data['css'] = $this->load->view('pga.css', NULL, TRUE);
            $this->template->view('pga',$data);
	}

}
