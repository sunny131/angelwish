<?php

class Admin extends MY_Controller {

	function  __construct() {
	parent::__construct();
	$loggedin = $this->session->userdata('loggedin');
	if(!$loggedin){
	$goto = $this->uri->uri_string();
	$this->session->set_flashdata("goto","$goto");
	header("Location: /login");
	exit;
	}
	else{
	$this->user = $this->session->userdata('user');
	}
	}

	function index(){
	$msg='';
	$data['dates']='';
	$dates='';
	ini_set('max_execution_time', 0);
	if(isset($_POST['fs'])){
	
	
	if($_FILES["wishes"]["type"]!='text/csv'){
	$msg.='Please check file, Only CSV files are allowed to upload</br>';	
	}else{
	$uploaddir = APPPATH.'uploads/';
	$uploadfile = $uploaddir.basename(time().$_FILES['wishes']['name']);
	if (move_uploaded_file($_FILES['wishes']['tmp_name'], $uploadfile)) {
	if (($handle = fopen($uploadfile, "r")) !== FALSE) {
	mysql_query("update `tbl_wishes` set `is_active`=0");
	$i=0;
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
	$data=explode(';',$data[0]);
	$data=str_replace('"', "", $data);
	
	if($i==0){
	$i++;
	}else{
	$r0 = isset($data[0]) ? $data[0] : '';
	$r1 = isset($data[1]) ? $data[1] : '';
	$r2 = isset($data[2]) ? $data[2] : '';
	$r3 = isset($data[3]) ? $data[3] : '';
	$r4 = isset($data[4]) ? $data[4] : '';
	$r5 = isset($data[5]) ? $data[5] : '';
	$r6 = isset($data[6]) ? $data[6] : '';
	$r7 = isset($data[7]) ? $data[7] : '';
	$r8 = isset($data[8]) ? $data[8] : '';
	$r9 = isset($data[9]) ? $data[9] : '';
	$r10 = isset($data[10]) ? $data[10] : '';
	$r11 = isset($data[11]) ? $data[11] : '';
	$r12 = isset($data[12]) ? $data[12] : '';
	$r13 = isset($data[13]) ? $data[13] : '';
	$r14 = isset($data[14]) ? $data[14] : '';
	$r15= isset($data[15]) ? $data[15] : '';
	$r16= isset($data[16]) ? $data[16] : '';
	
	mysql_query("INSERT INTO `tbl_wishes` (`pk_wish_id`, `CID`, `child_name`, `child_age`, `child_gender`, `child_city`, `child_state`, `child_country`, `child_disease`, `child_poverty`, `asin`, `book_name`, `book_image`, `uk_price`, `book_price`, `book_margin`, `book_final_price`, `is_granted`, `is_active`, `created_at`) VALUES (NULL, '".$r0."', '".$r1."', '".$r2."', '".$r3."', '".$r4."', '".$r5."', '".$r6."', '".$r7."', '".$r8."', '".$r9."', '".$r10."', '".$r11."', '".$r12."', '".$r13."', '".$r14."', '".$r15."', '".$r16."', 1, CURRENT_TIMESTAMP)") or die(mysql_error());
	}
	}
	fclose($handle);
	$msg.='Import Complete </br>';
	}
	} else {
	$msg.='Possible file upload attack!</br>';
	}
	}
	}
	$DistinctTime=mysql_query("select distinct DATE_FORMAT(created_at,'%Y-%m-%d %H:%i') as newdate from tbl_wishes where `is_granted`=0 and `is_active`=1") or die(mysql_error());
	$dates=array();
	while($fetch=mysql_fetch_array($DistinctTime)){
	$compare_date=date("Y-m-d", strtotime($fetch['newdate']));
	if($compare_date==date('Y-m-d')){
	$dates[]=$fetch['newdate'];
	}
	}
	
	
	$data['dates']=$dates;
	$data['msg']=$msg;
	$data['css'] = $this->load->view('admin.css', NULL, TRUE);
	$data['user'] = $this->user;
	$this->template->view('admin',$data);
	}

	function carecenter(){
	$data['css'] = $this->load->view('carecenter_add.css', NULL, TRUE);
	$data['user'] = $this->user;
	$this->template->view('carecenter_add',$data);
	}
	function lists($sort='',$order=''){
		$this->load->model('carecenters');
		$data['care_centers']=$this->carecenters->getAllCareCenters($sort,$order);
		$this->template->view('carecenter_list',$data);
	}
	function advertisment(){
		$this->load->model('advertisements');
		if(isset($_POST['fs'])){
			$this->advertisements->update($_POST['advertisement'],$_POST['active']);
		}

		$data['advertisement']=$this->advertisements->index();
		$data['active']=$this->advertisements->enable_chk();
		$this->template->view('advertisement',$data);
	}
	public function wishes(){
		$this->load->model('mwishes');
		$data['wishes']=$this->mwishes->getWishes('');
		$data['css'] = $this->load->view('admin.css', NULL, TRUE);
		$this->template->view('admin_wishes',$data);
	}
	function delete($request){
	$request=str_replace('_',' ',$request);
	$delete=mysql_query("delete from `tbl_wishes` WHERE (created_at BETWEEN '".$request.":00' AND '".$request.":59')") or die(mysql_error());
	header("Location: /admin");
	exit;
	}
	public function company(){
		$this->load->model('advertisements');
		$data['companies']=$this->advertisements->getCompanies();
		$data['css'] = $this->load->view('admin.css', NULL, TRUE);
		$this->template->view('admin_company',$data);
	}
	public function save_changes(){
		$SQL="update `tbl_companies` set `share`='".$_POST['input_name']."',`Title`='".$_POST['input_title']."' where `pk_company_id`='".$_POST['id']."' AND `name` = '".$_POST['name']."'";
	$update=mysql_query($SQL) or die(mysql_error());	
	echo 'Updated Row';
	}


}