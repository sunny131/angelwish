<?php

class Thanks extends MX_Controller {

    public function __construct() {
        parent::__construct();
        parse_str($_SERVER['REQUEST_URI'],$_GET);
    }

	function index()
	{
            $data['js'] = '';
            $data['css'] = $this->load->view('thank_you.css', NULL, TRUE);
            $this->template->view('thank_you',$data);
	}

    /**
     *
     * donate action
     * @author jobanjohn
     * @since 09-12-11
     */

    function action(){
        if($this->input->post('amount') && $this->input->post('id')){
            $id             = $this->input->post('id');
            $amount         = $this->input->post('amount');
            $description    = "2011 Angelwish Intl Donation";
            $key = md5(time().rand(10000,99999));
            $this->session->set_userdata('secure_key', $key);
            $secure_key     = $key;
            $this->load->helper('amazone');
            $companyId      = CompanyRouter::$is_corporate ? CompanyRouter::$company['id'] : NULL;
            $url            = CompanyRouter::$is_corporate ? CompanyRouter::$company['url'] : '';
            
            $url = $url?$url.".":'';
            $return_url = "http://".$url."angelwish.org/thanks/donationsucess/donationsucess/";
            $abandon_url = "http://".$url."angelwish.org";
            $acpaymentId = 'GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62';
            $data['form']  = getMarketplaceWidgetForm(
                        $amount,
                        $description,
                        "CID-$id-$secure_key",
                        1,
                        $return_url,
                        $abandon_url,
                        1,
                        "$acpaymentId",
                        'live');
             $this->load->view('donate',$data);
        }
        else{
            
        }

    }


    /**
     * function to add international care-center's donation
     * @author jobanjohn cubet technologies
     * @since 02-12-2011
     */

    function donationsucess()
    {
       
            $this->load->model('carecenters');
            $this->load->helper('url');
            $companyId = CompanyRouter::$is_corporate ? CompanyRouter::$company['id'] : NULL;
            $url       = CompanyRouter::$is_corporate ? CompanyRouter::$company['url'] : '';
            $data['fk_company_id']  = $companyId;
            $transactionId          = $data['transaction_id'] = $params["transactionId"]        = current($_GET);
            $transactionDate        = $data['transaction_date'] = $params["transactionDate"]    = $_GET['transactionDate']?$_GET['transactionDate']:'';
            $params["status"]               = $_GET['status'];
            $params["signatureMethod"]      = $_GET['signatureMethod'];
            $params["signatureVersion"]     = $_GET['signatureVersion'];
            $donatedby              = $data['donor_email'] = $params["buyerEmail"]              = $_GET['buyerEmail'];
            $params["recipientEmail"]       = $_GET['recipientEmail'];
            $params["operation"]            = $_GET['operation'];
            $transactionAmount      = $params["transactionAmount"]    = $_GET['transactionAmount'];
            $cidval                 = $params["referenceId"]          = $_GET['referenceId'];
            $buyerName              = $data['donor_name'] = $params["buyerName"]                = $_GET['buyerName'];
            $params["recipientName"]        = $_GET['recipientName'];
            $paymentMethod          = $data['payment_method'] = $params["paymentMethod"]        = $_GET['paymentMethod'];
            $params["paymentReason"]        = $_GET['paymentReason'];
            $params["certificateUrl"]       = $_GET['certificateUrl'];
            $params["signature"]            = $_GET['signature'];

            $cidarr = explode('-',$cidval);
            $cid    = $cidarr[1];
            $secure_key = $cidarr[2];
            $data['pk_care_center_id'] = $cid;

            $amtarr = explode(' ',$transactionAmount);
            $currency    = $amtarr[0];
            $amt         = $amtarr[1];
            $data['transaction_amount']   = $amt;
            $data['transaction_currency'] = $currency;
                                
            $onetime = $this->session->userdata('secure_key');
            $this->session->unset_userdata('secure_key');

            if($url){
                $redirect_url = "http://".$url.".dev.gothamangel.org/";
            }
            else{
               $redirect_url = "http://dev.gothamangel.org/";
            }
             
            if($secure_key == $onetime){
                
                $this->carecenters->insertInternationalDonations($data);
                redirect($redirect_url.'thanks/index/success');
            }
            else
            {
                redirect($redirect_url.'thanks/index/failed');
            }
    }

}
