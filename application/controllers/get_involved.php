<?php
class Get_involved extends MY_Controller {

    public function index(){        
        $data['js'] = $this->load->view('get_involved.js', NULL, TRUE);
        $data['css'] = $this->load->view('get_involved.css', NULL, TRUE);
        $this->template->view('get_involved',$data);
    }

    public function submit(){
        $firstName = $this->input->get_post('fname',true);
        $lastName = $this->input->get_post('lname',true);
        $email = $this->input->get_post('email',true);
        $company = $this->input->get_post('company',true);
        $phone = $this->input->get_post('phone',true);
        $message = $this->input->get_post('message',true);
        $keepInformed = $this->input->get_post('inform',true) == 'on';

        $this->load->model('musers');
        $this->musers->getInvolved($firstName, $lastName, $email, $company, $phone, $message, $keepInformed);
    }
}