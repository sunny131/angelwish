<?php

class Login extends MY_Controller {

	function index($error = false){
            $data['css'] = $this->load->view('login.css', NULL, TRUE);
            $redirect = $this->session->flashdata('goto') ? TRUE : FALSE;
            
            if($redirect){
                $this->session->keep_flashdata('goto');
            }
                
            $data['redirect'] = $redirect;
            $data['error'] = $error;
            $this->template->view('login',$data);
	}

    function process(){
        $redirect = $this->session->flashdata('goto') ?
                TRUE : FALSE;
        $goto = $redirect ? $this->session->flashdata('goto') : "/";

        $this->load->model('accesscontrol');

        $authenticated = $this->accesscontrol->authenticate($this->input->post('email'),
                    $this->input->post('password'));
        
        if($authenticated){
            $this->session->set_userdata("loggedin",true);
            $this->session->set_userdata("user",$this->user);
            header("Location: $goto");
            exit;
        }else{
            if($redirect){
                $this->session->keep_flashdata('goto');
            }
            header("Location: /login/index/true");
            exit;
        }
    }
        
}