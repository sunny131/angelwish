<?php
require_once(APPPATH.'libraries/AmazonPayments.php');
class Amazontest extends MY_Controller {

    function index(){
        if($this->input->get('cronjob', true)){
            $this->_cronJob();
        }
    }

    private function _cronJob() {
        $this->amazonpayments = AmazonPayments::getInstance();
        $this->load->model('transactions');

        $transactions = $this->transactions->getIncompleteTransactions($this->transactions->URL_GENERATED);
         //print_r($transactions);
        foreach ($transactions as $transaction) {
           
            print_r($this->amazonpayments->getTokenByCaller($transaction));
        }

        $transactions = $this->transactions->getIncompleteTransactions($this->transactions->TXN_INITIATED);
        foreach ($transactions as $transaction) {
            $this->amazonpayments->invokePay($transaction);
        }

        $transactions = $this->transactions->getIncompleteTransactions($this->transactions->TXN_INVOKE_PAY);
        foreach ($transactions as $transaction) {
            $this->amazonpayments->getTransactionStatus($transaction);
        }

        $this->load->model('memail');
        $transactions = $this->transactions->getCompletedTransactions();
        foreach ($transactions as $transaction) {
            $transactionDetails = $this->transactions->getTransactionDetailsForEmail($transaction);
            $this->memail->sendDonationProcessedConfirmation($transactionDetails);
        }
        $this->transactions->confirmationEmailSent($transactions);
    }

}
