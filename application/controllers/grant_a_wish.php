<?php

class Grant_a_wish extends MX_Controller {

	function index(){
           
           $rt = $this->load->model('carecenters');
          //  $this->load->model('Carecenters');
            $this->load->model('musers');
			$this->load->library('session');
            if($_POST){
				 
				$this->amazonpayments = AmazonPayments::getInstance();
				$total=$_POST['amount'];
				$subscribe_post='';
				if(isset($_POST['subcribe_period'])){
					$subscribe_post=$_POST['subcribe_period'];
				}
				if($this->session->userdata('source')){
					$companyId=$this->session->userdata('source');
				}else{
					$companyId='';
				}
				$office='';
				$state=$_POST['state'];
				$fname=$_POST['fname'];
				$lname=$_POST['lname'];
				$city=$_POST['city'];
				$email=$_POST['email'];
				$zipcode=$_POST['zipcode'];
				$address=$_POST['address'];
				
				$address_a=array("city"=>$city,'state'=>$state,'zipcode'=>$zipcode,'address'=>$address);
				
				$userId = $this->musers->addOrUpdate($fname,$lname,$email,'donor',round($total,2,PHP_ROUND_HALF_UP),0,$address_a);
				 
				$checkoutUrl = $this->amazonpayments->generateAmazonURL($share='N',$userId,$total,
                "Granting wishes for families affected by Holiday", $companyId, $office, '','','',$subscribe_post);
                header('Location: '.$checkoutUrl);
                die;
			}
            
            
            
            $data['states']=$this->carecenters->states('United States',1);
            $data['us_carecenters']=$this->carecenters->carecentersin('US');

            $data['uk_states']=$this->carecenters->states('United Kingdom',1);
            $data['uk_carecenters']=$this->carecenters->carecentersin('UK');
            
            $data['countries']=$this->carecenters->countries();
            $data['intl_carecenters']=$this->carecenters->carecentersin('INTL');

            $data['js'] = $this->load->view('grant_a_wish.js', NULL, TRUE);
            $data['css'] = $this->load->view('grant_a_wish.css', NULL, TRUE);
            $this->template->view('grant_a_wish',$data);
	}

    function more(){
        $this->load->view('more',NULL);
    }

}
