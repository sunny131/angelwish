<?php
class dashboard extends MX_Controller {
	 
	function index($date_from='',$date_to=''){
		$this->load->library('session');
		if(!$this->session->userdata('logged')){
			header("location:/setting/login");
		}
		if(isset($_POST['date_from']))
			$date_from=$_POST['date_from'];
		else
			$date_from='2013-11-17';
			
		if(isset($_POST['date_to']))
			$date_to=date('Y-m-d',strtotime($_POST['date_to']));
		else
			$date_to='2013-12-07';
		
			$this->load->model('wish');
			$this->load->model('user');
			
			$data['wishes']=$this->wish->getAllwishes();
			$data['users']=$this->user->getallusers();
			 
			$data['companies_orders']=$this->user->gettransactionbycompany($date_from,$date_to);
			
			$data['total_wish']=$this->user->total_wish($date_from,$date_to);
			$data['total_amount']=$this->user->total_amount($date_from,$date_to);
			$data['total_quick_donation']=$this->user->total_quick_donation($date_from,$date_to);
			
			$data['total_transaction']=$this->user->total_transaction($date_from,$date_to);
			$data['total_us_trans']=$this->user->total_us_trans($date_from,$date_to);
			$data['total_internation_trans']=$this->user->total_internation_trans($date_from,$date_to);
			$data['total_optional_donation']=$this->user->total_optional_donation($date_from,$date_to);
			$data['total_uk_trans']=$this->user->total_uk_trans($date_from,$date_to);
			$data['totalusers']=$this->user->totalusers();
			$data['percent_optional_donation']=$this->user->percent_optional_donation($date_from,$date_to);
			$data['care_center']=$this->user->care_center();
			$data['companies_name']=$this->user->getcompanyname();
			
			$this->template->view('dashboard/wish',$data);
	}
	
	function newdashboard($date_from='',$date_to=''){
		$this->load->library('session');
		if(!$this->session->userdata('logged')){
			header("location:/setting/login");
		}
		if(isset($_POST['date_from']))
			$date_from=$_POST['date_from'];
		else
			$date_from='2013-11-17';
			
		if(isset($_POST['date_to']))
			$date_to=date('Y-m-d',strtotime($_POST['date_to']));
		else
			$date_to='2013-12-07';
		
			$this->load->model('wish');
			$this->load->model('user');
			
			$data['wishes']=$this->wish->getAllwishes();
			$data['users']=$this->user->getallusers();
			 
			$data['companies_orders']=$this->user->gettransactionbycompany($date_from,$date_to);
			
			$data['total_wish']=1221212;
			$data['total_amount']=151242.10;
			$data['total_quick_donation']=15452.90;
			
			$data['total_transaction']=$this->user->total_transaction($date_from,$date_to);
			$data['total_us_trans']=$this->user->total_us_trans($date_from,$date_to);
			$data['total_internation_trans']=$this->user->total_internation_trans($date_from,$date_to);
			$data['total_optional_donation']=$this->user->total_optional_donation($date_from,$date_to);
			$data['total_uk_trans']=$this->user->total_uk_trans($date_from,$date_to);
			$data['totalusers']=50000;
			$data['percent_optional_donation']=33.33;
			$data['care_center']['care']=565;
			$data['care_center']['total_care']=750;
			$data['care_center']['percentage']=33;
			$data['companies_name']=$this->user->getcompanyname();
			
			$this->template->view('dashboard/dashboard2',$data);
	}
	
	function getdetailofcomapny($c_id=''){
		$this->load->model('user');
		$data['cid']=$c_id;
		$data['getcompany_detail']=$this->user->getcompanydetail($c_id);	
		
		$this->template->view('dashboard/companydetail',$data);
		
	}
	function allusers(){
		$this->load->model('user');
		$data['users']=$this->user->getallusers();
		$this->template->view('dashboard/allusers',$data);
	}
	
	function usertransactionbyemail($email=''){
		$this->load->model('user');
		
		//$email='shimmymehta@gmail.com';
		if(!empty($email)){
			$email=base64_decode($email);
			$data['userdetail']=$this->user->getusersbyemail($email);
			$data['email']=$email;
		}else{
			$data['error']='Not Valid email';
			$data['email']=''; 
		}
	 
		$data['users']=$this->user->getallusers();
		$this->template->view('dashboard/usertransactionbyemail',$data);
	}
	
	function export_reportbycompany($id){
		$this->load->model('user');
		$detail=$this->user->company_data($id);
		$filename='report.csv';
		header("Content-type: application/x-msexcel");
		header("Content-disposition: attachment; filename=".$filename);
		header("Pragma: no-cache");
		header("Expires: 0");  
		$head=1;
		foreach($detail as $row){
			foreach($row as $key=>$col){
				if($head==1)
					$heading.= $key.',';
				
				$data.=addslashes($col).','; 
			}
			$data.="\n";
			if($head==1)
				$heading.="\n";
			$head=2;
		} 
		echo $heading.$data;
		//$this->template->view('dashboard/companydata',$data);
	}	
	
	function recurringdonation(){
		echo '
<form action="https://authorize.payments.amazon.com/pba/paypipeline" method="post">
  <input type="hidden" name="ipnUrl" value="http://gothamangel.org/cart/captureresponse" >
  <input type="hidden" name="processImmediate" value="0" >
  <input type="hidden" name="signatureMethod" value="HmacSHA256" >
  <input type="hidden" name="accessKey" value="11SEM03K88SD016FS1G2" >
  <input type="hidden" name="collectShippingAddress" value="0" >
  <input type="hidden" name="amazonPaymentsAccountId" value="GMRWUGWUYGIB7KFB2QRARI8RRRN4VG26J5QN62" >
  <input type="hidden" name="cobrandingStyle" value="logo" >
  <input type="hidden" name="immediateReturn" value="1" >
  <input type="hidden" name="amount" value="USD 10" >
  <input type="hidden" name="recurringFrequency" value="1 month" >
  <input type="hidden" name="description" value="donation" >
  <input type="hidden" name="abandonUrl" value="http://www.angelwish.org" >
  <input type="hidden" name="signatureVersion" value="2" >
  <input type="hidden" name="subscriptionPeriod" value="12 month" >
  <input type="hidden" name="signature" value="5a4QaXTqkyAz6UDD+AlobhWZZ4zXtrdsG0wHYOh94sk=" >
  <input type="image" src="http://g-ecx.images-amazon.com/images/G/01/asp/beige_medium_paynow_withmsg_whitebg.gif" border="0">
</form>
';
	}
}
/*
select count(tbl_cartitems.transaction_id) as total_wishes_granted from  tbl_cartitems 
inner join tbl_transactions on 
	(tbl_transactions.status ="Completed" || tbl_transactions.status ="COMPLETE") 
	and tbl_transactions.pk_transaction_id=tbl_cartitems.transaction_id
;

select count(*) as total_transaction from  tbl_transactions where status ="Completed" || status ="COMPLETE";

select sum(amount) as total_amount from  tbl_transactions where status ="Completed" || status ="COMPLETE";

select sum(tbl_users.OptionalDonation) as total_optional_donation from  tbl_users
inner join tbl_transactions on 
	(tbl_transactions.status ="Completed" || tbl_transactions.status ="COMPLETE") 
	and tbl_transactions.fk_user_id=tbl_users.pk_user_id
;
* select count(*) as total_transaction from  tbl_transactions where status ="Completed" || status ="COMPLETE";

select count(*) as total_transaction from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 
AND (previous_transaction_id<500 || previous_transaction_id='' || `previous_transaction_id` IS NULL);

select count(*) as total_transaction from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 
AND (previous_transaction_id>=500 AND previous_transaction_id<600);

select count(*) as total_transaction from  tbl_transactions where (status ="Completed" || status ="COMPLETE") 
And previous_transaction_id>=600;



*/
