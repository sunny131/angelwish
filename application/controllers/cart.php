<?php
require_once(APPPATH.'libraries/AmazonPayments.php');
class Cart extends MY_Controller {
	public function add() {
		initializeGet();
		$cidAndWishId = $this->input->get_post('wish');
		//print_r($cidAndWishId);
		$qty = $this->input->get_post('qty');
		//$cidAndWishId = explode("_", $cidAndWishId);
		//$cId = $cidAndWishId[0];
		//$wishId = $cidAndWishId[1];
		$this->load->model('mwishes');
		$wish = $this->mwishes->getWish($cidAndWishId);
		$wish['price']=$wish['price']*$qty;
		$increased_price=$wish['price']*0.08;
		$wish['price']=$wish['price']+$increased_price;
		$wish['price']=number_format($wish['price'], 2, '.', '');
		$wish['qty']=$qty;
		$this->cart->insert($wish);
	}
        public function addoptions() {
		initializeGet();
		$cidAndWishId = $this->input->get_post('wish');
		$this->load->model('mwishes');
		$wish = $this->mwishes->getCarecenteroptions($cidAndWishId);
		$this->cart->insert($wish);
	
	echo '<div id="wish'.$cidAndWishId.'" class="wish">
		<div class="leftCol">
		<div class="productImage">
		&nbsp;
		</div>
		<div class="description">
		<div class="productDetails">
		<h4>'.$wish['bookName'].'</h4>
		</div>
		<div class="child">
		<span>'.$wish['childName'].', </span>
		<span>
		'.$wish['age'].' years old </span>
		</div>
		</div>
		</div>
		<div class="rightCol padding7">
		<span>1</span>
		</div>
		<div class="rightCol">
		<div id="price_'.$cidAndWishId.'" class="price">$<span class="value">'.$wish['price'].'</span></div>
		<input type="hidden" value="'.$wish['price'].'" id="or_pr_'.$cidAndWishId.'">
		<div class="removeFromCart">
		<a class="active2" rel="'.$cidAndWishId.'" href="javascript:void(0);">
		Remove </a>
		</div>
		</div>
		<br class="clear">
		</div>';
		die;
	}

    public function captureresponse(){
	
			
		$my_file = 'response/'.time().date('y_m_d').'_fileres.html';

		$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
		//write some data here

		foreach($_REQUEST as $k=>$v){
			if(is_array($v)){
				$data.="$k=><br>";
				foreach($v as $key=>$value){
					if(is_array($value)){
							$data.="==>$key=><br>";
						foreach($value as $key1=>$value1){
								$data.="===>$key1=>$value1<br>";
						}
					}else{
						$data.="==> $key=>$value<br>";
					}
				}
			}else{
				$data.="$k=>$v<br>";
			}
		}
		 
		fwrite($handle, $data);
		 

		fclose($handle);
		
	 if(isset($_REQUEST['operation']) && $_REQUEST['operation']=='REFUND' 	 && isset($_REQUEST['transactionStatus']) && $_REQUEST['transactionStatus']=='SUCCESS'){
		 $this->load->model('transactions');
		$query="select pk_transaction_id,fk_company_id,company_id from tbl_transactions 
				left join tbl_comptrans as track on pk_transaction_id=track.transaction_id
				where  fk_amazon_transaction_id='". $_REQUEST['parentTransactionId']."'";
		
		$data=$this->transactions->getdata($query);
		if(count($data)>0){
			
			$inserTransactionID=$data['0']->pk_transaction_id;
			
			$amount=str_replace("USD ","-",$_REQUEST['transactionAmount']);
			
			if($data['0']->fk_company_id==NULL || $data['0']->fk_company_id==''){
				if($data['0']->company_id==NULL || $data['0']->company_id==''){
					$company_id=0;
				}else{
					$company_id=$data['0']->company_id;
				}
			}else{
				$company_id=$data['0']->fk_company_id;
			}
			
			$query="update tbl_transactions set status='REFUND' where  fk_amazon_transaction_id='". $_REQUEST['parentTransactionId']."'";
			$this->transactions->getdata($query);
			$date1=date('Y-m-d h:i:s');
			$query="insert into tbl_comptrans set 
			company_id='".$company_id."', 
			amount='".$amount."', 
			transaction_id='".$inserTransactionID."',
			date1=now()
			";
			$this->transactions->saveDwollaOrder($query);
			
			
					
		}		 
	 }
	$this->amazonpayments = AmazonPayments::getInstance();
	$this->amazonpayments->handleCallBack();
	 
	}

    public function remove() {
        initializeGet();
        $id = $this->input->get_post('wish');
        $this->cart->remove($id);
    }

    

    public  function dwollacallback(){
    	$this->load->model('transactions');
		$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);
		$decoded = json_decode($jsonInput);
    		$this->transactions->updateDwollaTransaction($decoded->OrderId,
							     $decoded->CheckoutId,
							     $decoded->ClearingDate,
							     $decoded->TransactionId,
							     $decoded->TestMode,
							     $decoded->Status);
		//code to send the dwolla callback.
		if(is_object($decoded) && $decoded->Status=='Completed' ){
			$this->load->model('musers');
			$this->musers->sendOrderConfirmation($decoded->OrderId);
				
				$query="select pk_transaction_id, amount, fk_company_id from tbl_transactions where  `DwollaOrderId`='".$decoded->OrderId."' ";
				$res=$this->transactions->getdata($query);
					$trans_id=$res['0']->pk_transaction_id;
					$amount=$res['0']->amount;
					$company_id=$res['0']->fk_company_id;
					$date1=date('Y-m-d h:i:s');
					$query="insert into tbl_comptrans set 
					company_id='".$company_id."', 
					amount='".$amount."', 
					transaction_id='".$trans_id."',
					`date1`=now()
					";
					$this->transactions->saveDwollaOrder($query);
				 
			 $this->cart->destroy();
		}else{
			echo "<p>There is some problem in placing the order please try again...!!!</p>";
		}
    }

    function verifysignature(){
    	$this->load->library('DwollaRestClient');
		    // Grab Dwolla's proposed signature
		$signature = $_GET['signature'];
		
		// Grab Dwolla's checkout ID
		$checkoutId = $_GET['checkoutId'];
		
		// Grab the reported total transaction amount
		$amount = $_GET['amount'];
		
		// Verify the proposed signature
		$didVerify = $this->dwollarestclient->verifyGatewaySignature($signature, $checkoutId, $amount);
		
		if($didVerify) { 
			echo "<p>Order Placed successfully!!</p>";
		} else {
			echo "<p>There is some problem in placing the order please try again!!</p>";
		}
     }
	public function checkoutDwolla($optionalDonationAmount=0,$share,$donation){
		
		 $this->optionalDonationAmount=0.00;
		 $this->load->model('transactions');
    	
    	 $this->load->library('DwollaRestClient');

    	// die($optionalDonationAmount.'asd');

	
                 $this->items[0]=array('bookName'=>"Donation", 'price'=> $donation);
                 $optionalDonationAmount=0;

    	 if(!empty($this->items))
         {
    	 	//$this->dwollarestclient->setMode('test');
			
			$this->dwollarestclient->startGatewaySession();
    	 	foreach($this->items as $item)
               {
         //print_r($item);
        

 //die("hiii");    	 	
	$this->dwollarestclient->addGatewayProduct($item['bookName'], $item['price'], 1, $item['bookName']);
    	 	}
    	 	
    	 	if($optionalDonationAmount>0)
                 {
    	 		
    	 		$this->dwollarestclient->addGatewayProduct('Optional donation to Angelwish',$optionalDonationAmount, 1,'Optional donation to Angelwish');
    	 	}
		
    	 	//Original account 812-460-4040
    	 	//Test account 812-713-9234
    	 	$this->DwollaOrderId=strtotime(date('Y-m-d H:i:s'));
    	 	$this->checkoutUrl = $this->dwollarestclient->getGatewayURL('812-460-4040',$this->DwollaOrderId, 0, 0, 0, 'Granting wishes at Angelwish.org', $_SERVER['SERVER_NAME'].'/cart/dwollacallback');
		
			if(!$this->checkoutUrl) 
                       { 
				echo $this->dwollarestclient->getError(); 
			} 
			else { 
				//before forwarding the site to the offsite gateway please save the transaction first.
				 $query = "INSERT INTO tbl_transactions(fk_user_id,
									amount,
									reason,
									fk_company_id,
									status,
									created_at,
									domain,
									payment_method,
									DwollaOrderId,
									json_order_items,
									previous_transaction_id,
									fk_company_office_id,
									share
									)VALUES(
 									'".$this->fk_user_id."',
 									'".$this->dwollaTotal."',
 									'Granting wishes at Angelwish.org',
 									'".$this->dwollaCompanyId."',
 									'',
 									CURRENT_TIMESTAMP,
 									'". $_SERVER['SERVER_NAME'] ."',
									'Dwolla',
 									$this->DwollaOrderId,
									'".urlencode(json_encode($this->items))."',
									'".$this->careCenterId."',
									'".$this->officeId."',
								'".$share."')";
					 									   
				$inserTransactionID=$this->transactions->saveDwollaOrder($query);	 									  
				// Forward the user to the offsite gateway
				
				if($inserTransactionID){
					header('Location: '.$this->checkoutUrl);


				}
				else{

					/*	echo $_SERVER['HTTP_REFERER'];
					die; */
					header('Location: '.$_SERVER['HTTP_REFERER'].'?TransSave=Failed');


				}
			} 
    	}

    	exit;
    }
    
    public function checkout(){
		
	$this->load->library('session');
    	$this->paymentProcessor=$this->input->get_post('paymentProcessor',true);
    	$this->optionalDonation=0;
    	$this->optionalDonationAmount=0.00;
    	if($this->paymentProcessor=='Amazon'){
        	$this->amazonpayments = AmazonPayments::getInstance();
    	}

        $this->load->model('musers');
        $fname   = ucfirst($this->input->get_post('fname',true));
        $lname   = ucfirst($this->input->get_post('lname',true));
        $email   = $this->input->get_post('email',true);
        $support = $this->input->get_post('support',true);
        $donation  = $this->input->get_post('cash',true);
	
        $office = NULL;
        if($this->input->get_post('office',true) == true){
			$office  = $this->input->get_post('office',true);
        }
        if($this->input->get_post('share',true) == true){
			$share  = $this->input->get_post('share',true);
        }else{
			$share='N';
		}
	
    	$this->items=$this->session->userdata('cart_wishes');
        //TODO: Add Server side validations
//print_r($this->items);die;
        $this->session->set_userdata('fname',$fname);
        $this->session->set_userdata('lname',$lname);
        $this->session->set_userdata('email',$email);
        
        if($support == 'on' || $support>0){
        	if($support == 'on'){
    	 			$this->optionalDonationAmount=(($this->cart->total())*15/100);
    	 	}
    	 		$this->optionalDonationAmount=round($this->optionalDonationAmount+$donation,2,PHP_ROUND_HALF_UP);
        }
       
        $subtotal = $this->cart->total();
        $total    = ($support == 'on')? $subtotal*1.15 : $subtotal;
		$total 	  = $total + $donation;
        $total    = round($total,2,PHP_ROUND_HALF_UP);
        
        $userId = $this->musers->addOrUpdate($fname,$lname,$email,'donor',round($this->cart->total(),2,PHP_ROUND_HALF_UP),$this->optionalDonationAmount);
        $this->careCenterId=$this->session->userdata('care_center_id');
      
        $companyId = CompanyRouter::$is_corporate ? CompanyRouter::$company['id'] : NULL;
        if(empty($companyId) || $companyId=='') {
			$companyId=$this->session->userdata('source');
		}

        if($this->paymentProcessor=='Amazon'){
        		$checkoutUrl = $this->amazonpayments->generateAmazonURL($share,$userId,$total,
                "Granting wishes at Angelwish.org", $companyId, $office, urlencode(json_encode($this->items)),$currencyCode = "USD",'','');
				 
        		header('Location: '.$checkoutUrl);
        }elseif($this->paymentProcessor=='Paypal'){
			$this->load->model('transactions');
			$this->PaypalOrderId=strtotime(date('Y-m-d H:i:s'));
			$query = "INSERT INTO tbl_transactions(fk_user_id,
								amount,
								reason,
								fk_company_id,
								status,
								created_at,
								domain,
								payment_method,
								paypal_order_id,
								json_order_items,
								previous_transaction_id,
								fk_company_office_id,
								`share`
								)VALUES(
								'".$userId."',
								'".$total."',
								'Granting wishes at Angelwish.org',
								'".$companyId."',
								'',
								CURRENT_TIMESTAMP,
								'". $_SERVER['SERVER_NAME'] ."',
								'Paypal',
								$this->PaypalOrderId,
								'".urlencode(json_encode($this->items))."',
								'".$this->careCenterId."',
								'".$office."',
								'".$share."'
 								)";
			//code to save paypal transaction.
			$inserTransactionID=$this->transactions->savePaypalOrder($query);	
			if($inserTransactionID){
				$data['css'] = $this->load->view('thank_you.css', NULL, TRUE);
				$data['optionalDonationAmount'] = $this->optionalDonationAmount;
				$data['orderid'] = $this->PaypalOrderId;
				$this->load->library('session');
				if($this->session->userdata('source')){
					$data['company_id']= $this->session->userdata('source');
				}
				
				$data['cartTotal'] = $total;
			}
	
			$this->template->view('placepaypalorder',$data);
		}else{
			
			if($support == 'on'){
				$this->optionalDonation=1;
			}
			$this->dwollaCompanyId=$companyId;
			$this->fk_user_id=$userId;
			$this->dwollaTotal=$total;
			$this->officeId = $office;
			
			$this->checkoutDwolla($this->optionalDonationAmount,$share,$donation);
			



        	}
        
    } 
    	
	public function Paymentconfirmation(){

	  	$data['css'] = $this->load->view('thank_you.css', NULL, TRUE);
        	 
			if(isset($_GET['postback']) && $_GET['postback']=='success'){
				 $this->cart->destroy();
				 $this->load->library('session');
				 $array_items = array('fname' => '', 'lname' => '', 'email' => '');
				 $this->session->unset_userdata($array_items);
				// $this->session->sess_destroy();
 			}
 			
			$this->template->view('PaymentConfirmation',$data);
	}
	
	
    public function thank_you(){
        /*$this->amazonpayments = AmazonPayments::getInstance();
        if($this->amazonpayments){
            $callerRefernce = $this->amazonpayments->handleCallback();
        
            $this->load->model('transactions');
            $txnId = $this->transactions->getTransactionDetails(array('id'),$callerRefernce);

            $this->load->model('memail');
            $this->load->model('mwishes');
            $this->mwishes->saveProcessedCart($txnId['id']);
            $emailed = $this->transactions->getTransactionDetails(array('emailInitiated'),$callerRefernce);
            if(!empty($emailed) && $emailed['emailInitiated'] == 0){
                $transactionDetails = $this->transactions->getTransactionDetailsForEmail($callerRefernce);
                $this->memail->sendDonationInitiatedConfirmation($transactionDetails);
            }
        }*/
        $data['css'] = $this->load->view('thank_you.css', NULL, TRUE);
        $this->template->view('thank_you',$data);
    }

    public function checkServerStatus(){
	$serverStatus=array('working'=>'Fine');
	print_r(json_encode($serverStatus));
    }
    
    public  function dwollacallback1(){
				$this->load->model('transactions');
		 
				   $decoded->OrderId='c50999aa-576f-11e3-afcf-008cfa12d494';
					 $query="select pk_transaction_id, amount, fk_company_id from tbl_transactions where  `caller_reference`='".$decoded->OrderId."' ";
					$res=$this->transactions->getdata($query);
					//print_r($res);
					$trans_id=$res['0']->pk_transaction_id;
					$amount=$res['0']->amount;
					$company_id=$res['0']->fk_company_id;
					$date1=date('Y-m-d h:i:s');
					echo $query="insert into tbl_comptrans set 
					company_id='".$company_id."', 
					amount='".$amount."', 
					transaction_id='".$trans_id."',
					`date1`=now()
					";
					 $this->transactions->saveDwollaOrder($query);
		 
    }

    public function test(){
		 $this->load->model('transactions');
		 $company_id='11111';
		 $inserTransactionID='111111111';
		 $amount='11111';
		//$this->transactions->getdata($query);
			$date1=date('Y-m-d h:i:s');
			echo $query="insert into tbl_comptrans set 
			company_id='".$company_id."', 
			amount='".$amount."', 
			transaction_id='".$inserTransactionID."',
			`date1`='".$date1."'
			";
			$this->transactions->saveDwollaOrder($query);
			
	}
}
