<?php

class Setting extends MX_Controller {

	function index()
	{
			$this->load->library('session');
			if(!$this->session->userdata('logged')){
				header("location:/setting/login");
			}
            $this->load->model('user');
            $data['settings']=$this->user->getallsetting();
            $this->template->view('dashboard/setting',$data);
	}
	function edit($id=''){
			$this->load->library('session');
			$this->load->model('user');
			if(!$this->session->userdata('logged')){
				header("location:/setting/login");
			}
			if($_POST){
				
				if(!isset($_POST['status'])){
					$_POST['status']='Y';
				}
				$this->user->updatesetting($_POST['settingValue'],$_POST['status'],$id);
				header("location:/setting");
			}
			if($id==''){
				$data['error']="Wrong setting  ID";
			}else{
				$data['setting']=$this->user->getsettingbyid($id);
			}
			$this->template->view('dashboard/edit',$data);
	}
	function login(){
		$this->load->library('session');
		if($this->session->userdata('logged')){
			header("location:/setting");
		}
		$err='';
		if($_POST){
			if(isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['password']) && !empty($_POST['password'])){
				if($_POST['username']=='admin' && $_POST['password']=='123456')
					$this->session->set_userdata('logged','1');
				else
					$err='1';
			}else{
				$err='1';
			}
		}
		if($err!=''){
			$data['error']="Wrong user name or password";
		}
		$this->template->view('dashboard/login',$data);
		
	}
	function logout(){
		$this->load->library('session');
		$this->session->set_userdata('logged','0');
		header("location:/setting");
	}

}
