<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Template
 *
 * @author ashish
 */
class Template {
    var $template_data = array();

    private static $default_template = 'template';
    private $CI;

    function __construct(){
        $this->CI =& get_instance();
    }
    function set($name, $value){
        $this->template_data[$name] = $value;
    }

    function load($template = '', $view = '' , $view_data = array(), $return = FALSE){            
            $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
            return $this->CI->load->view($template, $this->template_data, $return);
    }

    function view($view = '' , $view_data = array(), $return = FALSE) {            
            $this->set('templatecss', $this->CI->load->view(self::$default_template.".css", NULL, TRUE));
            $this->set('templatejs', $this->CI->load->view(self::$default_template.".js", NULL, TRUE));

            $headerdata = array();
            $headerdata['css'] = $this->CI->load->view('header.css', NULL, TRUE);
            $headerdata['pagename'] = $view;
            $this->set('header', $this->CI->load->view('header', $headerdata, TRUE));


            $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));

            $footerdata = array();
            $footerdata['css'] = $this->CI->load->view('footer.css', NULL, TRUE);
            $this->set('footer', $this->CI->load->view('footer', $footerdata, TRUE));

            return $this->CI->load->view(self::$default_template, $this->template_data, $return);
    }

    static function getDefaultTemplate(){
        return self::$default_template;
    }

    static function setDefaultTemplate($template){
        self::$default_template = $template;
    }
}
// END Template Class

/* End of file Template.php */
/* Location: ./application/libraries/Template.php */