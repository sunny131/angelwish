<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Cart{
    private $wishes;
    private $CI;
    
    public function  __construct(){
        $this->CI =& get_instance();

        // Load the Sessions class
		$this->CI->load->library('session', array());
			
		// Grab the shopping cart array from the session table, if it exists
		if ($this->CI->session->userdata('cart_wishes') !== FALSE){
			$this->wishes = $this->CI->session->userdata('cart_wishes');
		}
		else{
            $this->wishes = array();
        }
        
    }

    public function hasWish($id) {
        return array_key_exists($id, $this->wishes);
    }

    public function getWishes() {
        return $this->contents();
    }

    public function contents() {
        $wishes = array();
        foreach ($this->wishes as $key => $cart_wish) {
                $wishes[] = $cart_wish;
        }
        //die(var_dump($this->wishes));
        return $wishes;
    }

    function insert($wish) {
        $this->wishes[$wish['id']] = $wish;
        $this->CI->session->set_userdata(array('cart_wishes' => $this->wishes));
        //now add the product also to the dwolla gateway.
        //$dwollarestclient->addGatewayProduct($wish['bookName'], $wish['price'], 1, $wish['bookName'],$wish['id']);
        //var_dump(count($this->wishes));
        return TRUE;
    }

    function destroy() {
        unset($this->wishes);
        $this->CI->session->unset_userdata('cart_wishes');
    }

    function remove($wishId) {
        unset ($this->wishes[$wishId]);
        $this->CI->session->set_userdata(array('cart_wishes' => $this->wishes));
        //remove the product fron the dwolla gateway also
        // $dwollarestclient->removeGatewayProduct($wishes[$wishId]);
    }

    function total(){
        $total = 0.0;
        foreach ($this->wishes as $wish) {
            $total += $wish['price'];
        }
        return $total;
    }

    function count(){
        return count($this->wishes);
    }

    function isEmpty(){
        return empty($this->wishes);
    }

}
