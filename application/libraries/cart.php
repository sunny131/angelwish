<?php
require_once(APPPATH.'libraries/AmazonPayments.php');

class Cart extends MY_Controller {

        public function add() {
        initializeGet();
        $cidAndWishId = $this->input->get_post('wish');
        $cidAndWishId = explode("_", $cidAndWishId);
        $cId = $cidAndWishId[0];
        $wishId = $cidAndWishId[1];
        $this->load->model('mwishes');
        $wish = $this->mwishes->getWish($cId,$wishId);
		
        $this->cart->insert($wish);
    }

    public function remove() {
        initializeGet();
        $id = $this->input->get_post('wish');
        $this->cart->remove($id);
    }

	public function checkoutDwolla(){
    	 $this->load->library('DwollaRestClient');
    	 $this->load->library('session');
    	 $this->items=$this->session->userdata('cart_wishes');
    	 if(!empty($this->items)){
    	 	$this->dwollarestclient->setMode('test');
			$this->dwollarestclient->startGatewaySession();
    	 	foreach($this->items as $item){
    	 		$this->dwollarestclient->addGatewayProduct($item['bookName'], $item['price'], 1, $item['bookName'],$item['id']);
    	 	}
    	 	
    	 	print_r($this->dwollarestclient->gatewaySession);
    	 	
    	 	$this->checkoutUrl = $this->dwollarestclient->getGatewayURL('812-734-7288',time(), 0, 0, 0, 'Granting wishes at Angelwish.org', 'http://requestb.in/rikm5vri');
			if(!$this->checkoutUrl) { 
				
				echo $this->dwollarestclient->getError(); } 
			else { 
				// Forward the user to the offsite gateway
				header('Location: '.$this->checkoutUrl);
			} 
    	 }
    	 exit;
    }
    
    public function checkout() {
        $this->amazonpayments = AmazonPayments::getInstance();
        $this->load->model('musers');
        $fname   = ucfirst($this->input->get_post('fname',true));
        $lname   = ucfirst($this->input->get_post('lname',true));
        $email   = $this->input->get_post('email',true);
        $support = $this->input->get_post('support',true);
        $donation  = $this->input->get_post('cash',true);

        //TODO: Add Server side validations

        $this->session->set_userdata('fname',$fname);
        $this->session->set_userdata('lname',$lname);
        $this->session->set_userdata('email',$email);
        
        $userId = $this->musers->addOrUpdate($fname,$lname,$email);
        
        $subtotal = $this->cart->total();
        $total    = ($support == 'on')? $subtotal*1.15 : $subtotal;
	$total 	  = $total + $donation;
        $total    = round($total,2,PHP_ROUND_HALF_UP);
        $companyId = CompanyRouter::$is_corporate ? CompanyRouter::$company['id'] : NULL;
        $checkoutUrl = $this->amazonpayments->generateAmazonURL($userId,$total,
                "Granting wishes at Angelwish.org", $companyId);

        header('Location: '.$checkoutUrl);
        exit;
    }

    public function thank_you(){
        /*$this->amazonpayments = AmazonPayments::getInstance();
        if($this->amazonpayments){
            $callerRefernce = $this->amazonpayments->handleCallback();
        
            $this->load->model('transactions');
            $txnId = $this->transactions->getTransactionDetails(array('id'),$callerRefernce);

            $this->load->model('memail');
            $this->load->model('mwishes');
            $this->mwishes->saveProcessedCart($txnId['id']);
            $emailed = $this->transactions->getTransactionDetails(array('emailInitiated'),$callerRefernce);
            if(!empty($emailed) && $emailed['emailInitiated'] == 0){
                $transactionDetails = $this->transactions->getTransactionDetailsForEmail($callerRefernce);
                $this->memail->sendDonationInitiatedConfirmation($transactionDetails);
            }
        }*/
        $data['css'] = $this->load->view('thanks.css', NULL, TRUE);
        $this->template->view('thanks',$data);
    }
}
