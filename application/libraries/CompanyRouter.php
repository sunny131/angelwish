<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(BASEPATH.'database/DB'.EXT);

/**
 * Description of CompanyRouter
 *
 * @author ashish
 */
class CompanyRouter {
    const CORPORATEFOLDER = 'corporate';
    const COOKIENAME = 'company';

    public static $company;
    public static $is_corporate;
    public static $offices;
    public static $has_corporate_offices;

    private static $DB;
    
    public static function initalize($segments){
        if (!isset(self::$instance)) {
            self::$is_corporate = FALSE;
            self::$has_corporate_offices = FALSE;
            self::$company = array("name"=>NULL,
                                 "cookiecode" => NULL,
                                 "tagline" => NULL,
                                 "welcome_text" => NULL);
            self::$DB = DB();
            
            return self::setCompany($segments);
        }
        //else throw exception - we should set company only once
    }

    private static function setCompany($segments){
        $host = $_SERVER['HTTP_HOST'];
	$dot_position = stripos($host,".");
	$company_url = $dot_position ? substr($host,0,$dot_position ) : false;
        if($company_url && self::findCompanyByURL($company_url)){
            self::$is_corporate = TRUE;
            
        }
        elseif(array_key_exists('company', $_COOKIE) && self::findCompanyByCookie($_COOKIE[self::COOKIENAME])){
            self::$is_corporate = TRUE;
        }
        
        if(self::$is_corporate == TRUE){
        $query = self::$DB->get_company_offices(self::$company["id"]);

			if($query->num_rows()>0){
			 
			    self::$has_corporate_offices = TRUE;
				foreach ($query->result() as $row){        
					self::$offices[] = $row;
				}
			 
			}
        }

        return $segments;
    }
    
    private static function findCompanyByURL($url){
        //$query = self::$DB->get_where('companies', array('url' => $url,'is_active'=>1));
        $query = self::$DB->get_company($url);
        foreach ($query->result() as $row){
           self::$company["id"] = $row->pk_company_id;
           self::$company["name"] = $row->name;
           self::$company["url"] = $row->url;
           self::$company["cookiecode"] = $row->cookie_code;
           self::$company["tagline"] = $row->tagline;
           self::$company["welcome_text"] = $row->welcome_text;
           self::$company["parenturl"] = $row->parenturl;
           self::$company["parentname"] = $row->parentname;
        }
        if($query->num_rows()>0){
            self::setCompanyCookie(self::$company["cookiecode"]);
            return true;
        }
        else
            return false;
    }

    private static function findCompanyByCookie($cookiecode){
        $query = self::$DB->get_where('companies', array('cookie_code' => $cookiecode));
        foreach ($query->result() as $row){
           self::$company["id"] = $row->pk_company_id;
           self::$company["name"] = $row->name;
           self::$company["url"] = $row->url;
           self::$company["cookiecode"] = $row->cookie_code;
           self::$company["tagline"] = $row->tagline;
           self::$company["welcome_text"] = $row->welcome_text;
        }
        if($query->num_rows()>0){
            self::setCompanyCookie(self::$company["cookiecode"]);
            return true;
        }
        else
            return false;
    }

    private static function setCompanyCookie($cookiecode){
        $cookie = array(
                   'name'   => self::COOKIENAME,
                   'value'  => $cookiecode,
                   'expire' => time()+60*60*24*365,
                   'path'   => '/'
               );
        //Delete the cookie first
        setcookie($cookie['name'], '', time()-3600,
                $cookie['path']);

        //Set the cookie now
        setcookie($cookie['name'], $cookie['value'], $cookie['expire'],
                $cookie['path']);
    }

    
}
// END CompanyRouter Class

/* End of file CompanyRouter.php */
/* Location: ./application/libraries/CompanyRouter.php */
