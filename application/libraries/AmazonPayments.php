<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');
/*COMPLETE
 * Wrapper class to handle calls to Amazon FPS API
 */
require_once(APPPATH . 'libraries/Amazon/Cbui_singleuse_pipeline.php');
require_once(APPPATH . 'libraries/Amazon/CBUIRecurringTokenPipeline.php');

class AmazonPayments {
    //TODO: Move all constants to config file
    const ACCESS_KEY = "AKIAJCNMZYBP457MZTEQ";
    const SECRET_KEY = "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";
    const RETURN_URL = '/thank-you';

    private static $instance;
    private $pipeline;
    private $service;
    private $request;
    private $amount;
    private $CI;
    private $subscribe_post;

    /**
     * Using Singleton pattern
     */
    private function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('transactions');
		$this->CI->load->model('memail');
        $this->CI->load->model('mwishes');
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new AmazonPayments();
        }
        return self::$instance;
    }

    /*
     * This is called by the page (Cart Checkout page?) that wants to send user
     * to Amazon for payments
     */

    public function generateAmazonURL($share, $userId, $amount, $reason="", $companyId = NULL, $officeId=NULL, $cart_items=null, $currencyCode = "USD",$cartId = NULL,$subscribe_post) {
	if(!empty($subscribe_post)){
	$this->initializeGenerateURL(); // for recurring
	}else{
	$this->initializeGenerateURL_singlepayment(); // for one time payment
	}
        
	
        //$protocol = $_SERVER['HTTPS'] == on ? "https://" : "http://";
        $protocol = isset($_SERVER['HTTPS']) ? "https://" : "http://";
        $returnUrl = $protocol . $_SERVER['HTTP_HOST'] . self::RETURN_URL;
		$callerReference = $this->CI->transactions->createNewTransactionwithshare($userId, $amount, $reason, $companyId, $officeId, $cartId, $currencyCode,$cart_items,$share);
        	if(!empty($subscribe_post)){
	$this->pipeline->setMandatoryParameters($callerReference, $returnUrl, $amount,$subscribe_post);
	}else{
	      $this->pipeline->setMandatoryParameters($callerReference, $returnUrl, $amount); // for one time payment
	}

        $this->pipeline->addParameter("currencyCode", "USD");
        $this->pipeline->addParameter("paymentReason", $reason);
        $this->pipeline->addParameter("cobrandingUrl", "https://www.angelwish.org/public/images/logo.png");
        $this->pipeline->addParameter("websiteDescription", "Angelwish.org");
        $url = $this->pipeline->getUrl();
        return $url;
    }
	

    /**
     * This is called by the page that is serving as callback page
     * for Amazon Payments
     */
    public function handleCallBack() {
        $atStep = $this->CI->transactions->TXN_INITIATED;

        $parameters = $this->getTokenID();
        if (empty($parameters['tokenID'])) {
            header("Location: /");
            exit;
        }
		
        $success = false;
        switch ($parameters['status']) {
            case 'A':
                $this->CI->transactions->updateTransaction(array("status" => "ABORTED",
                    "description" => "Pipeline aborted by user",
                    "amazonStatus" => "A"),
                        $parameters['callerReference'], $atStep);
                break;

            case 'SE':
                $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                    "description" => "Unknown TokenID - System Error at Amazon",
                    "amazonStatus" => "SE"),
                        $parameters['callerReference'], $atStep);
                break;

            case 'CE':
            case 'NM':
            case 'NP':
            case 'PE':
                $this->CI->transactions->updateTransaction(array("status" => "REVIEW",
                    "amazonStatus" => $parameters['status']),
                        $parameters['callerReference'], $atStep);
                break;

            case 'SA': $paymentMethod = "ABT";
            case 'SC': $paymentMethod = isset($paymentMethod) ? $paymentMethod : "Credit Card";
            case 'SB': $paymentMethod = isset($paymentMethod) ? $paymentMethod : "ACH";
            
                $this->CI->transactions->updateTransaction(array("status" => "READY",
                    "tokenId" => $parameters['tokenID'],
                    "stepCompleted" => $atStep,
                    "paymentMethod" => $paymentMethod,
                    "amazonStatus" => $parameters['status']),
                 $parameters['callerReference'], $atStep);
                $success = true;
                
                break;
            default:
                $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                    "description" => "Unknown value returned",
                    "amazonStatus" => $parameters['status']),
                        $parameters['callerReference'], $atStep);
        }

        if ($success) {
            $this->invokePay($parameters['callerReference']);
        } else {
            //TODO: Show error in processing page.
        }
        return $parameters['callerReference'];
    }

    /**
     *  Invokes Pay and handles error conditions
     *  @param String $callerReference
     */
    public function invokePay($callerReference, $descriptor = "Angelwish", $cronJob = false) {
        $atStep = $this->CI->transactions->TXN_INVOKE_PAY;
        $this->initializeInvokePay();
        $txnDetails = $this->CI->transactions->getTransactionDetails(array("tokenId", "amount", "callerReference"),
                        $callerReference);
        if(empty($txnDetails)){
            return false;
        }
        $this->amount->setValue($txnDetails['amount']);

        $descriptorPolicy = new Amazon_FPS_Model_DescriptorPolicy();
        $descriptorPolicy->setSoftDescriptorType("Dynamic");
        $senderDescription = substr($descriptor, 0, 15);

        $this->request->setDescriptorPolicy($descriptorPolicy);
        $this->request->setSenderDescription($senderDescription);
        $this->request->setSenderTokenId($txnDetails['tokenId']);
        $this->request->setCallerReference($txnDetails['callerReference']);
        $this->request->setTransactionAmount($this->amount);
        $transaction = $this->_invokePay($this->service, $this->request);

        if ($transaction['success']) {
            $this->CI->transactions->updateTransaction(array("status" => "READY",
                "transactionId" => $transaction['transaction_id'],
                "stepCompleted" => 2,
                "amazonStatus" => $transaction['transaction_status']),
                    $callerReference, $atStep);
            //over to txn status
            if (!$cronJob) {
                $this->getTransactionStatus($callerReference);
            }
        } else {
            switch ($transaction['error_code']) {
                case 'InvalidParams':
                case 'AmountOutOfRange':
                case 'AuthFailure':
                case 'DuplicateRequest':
                case 'IncompatibleTokens':
                case 'InvalidAccountState_Caller':
                case 'InvalidAccountState_Recipient':
                case 'InvalidAccountState_Sender':
                case 'InvalidClientTokenId':
                case 'InvalidTokenId_Sender':
                case 'SameSenderAndRecipient':
                case 'SignatureDoesNotMatch':
                case 'TokenNotActive_Sender':
                case 'TransactionDenied':
                case 'UnverifiedAccount_Recipient':
                case 'UnverifiedAccount_Sender':
                case 'UnverifiedBankAccount':
                case 'UnverifiedEmailAddress_Caller':
                case 'UnverifiedEmailAddress_Recipient':
                case 'UnverifiedEmailAddress_Sender':
                    $this->CI->transactions->updateTransaction(array("status" => "REVIEW",
                        "amazonStatus" => $transaction['error_code'],
                        "amazonError" => $transaction['exception_message']),
                            $callerReference, $atStep);
                    break;


                case 'AccessFailure': $description = 'Your account cannot be accessed.';
                case 'AccountLimitsExceeded': $description = isset($description) ? $description : 'You have exceeded your spending or receiving limits.
                    You can view your current limits at http://payments.amazon.com/sdui/sdui/viewlimits.
                    You can upgrade these limits by adding and verifying a bank account as a payment method.
                    Please visit Adding and Verifying Bank Accounts to learn how to add and instantly verify a bank account.';
                case 'InsufficientBalance': $description = isset($description) ? $description : 'You don\'t have sufficient funds in your account.
                    Please refund your account and try again.';

                    $this->CI->transactions->updateTransaction(array("status" => "UserAction",
                        "amazonStatus" => $transaction['error_code'],
                        "amazonError" => $transaction['exception_message'],
                        "description" => $description),
                            $callerReference, $atStep);
                    break;

                case 'InternalError':
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $transaction['error_code'],
                        "amazonError" => $transaction['exception_message']),
                            $callerReference, $atStep);
                    break;

                default:
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $transaction['error_code'],
                        "amazonError" => $transaction['exception_message']),
                            $callerReference, $atStep);
                    break;
            }
        }
    }

    /**
     *  Gets Txn Status and handles error conditions
     * @param String $callerReference
     */
    public function getTransactionStatus($callerReference) {
        $atStep = $this->CI->transactions->TXN_COMPLETE;
        $this->initializeGetTransactionStatus();

        $txnDetails = $this->CI->transactions->getTransactionDetails(array("transactionId"),
                        $callerReference);
        $this->request->setTransactionId($txnDetails['transactionId']);
        $status = $this->invokeGetTransactionStatus($this->service, $this->request);
        if ($status['success']) {
            switch ($status['status_code']) {
                case 'Success':
                    $this->CI->transactions->updateTransaction(
						array("status" => "COMPLETE","stepCompleted" => 3,"amazonStatus" => $status['status_code']),
						$status['caller_reference'], 
						$atStep
                    );
                    
                    /*****Start code For Tracking order by company ********/
					
					$query="select pk_transaction_id, amount, fk_company_id from tbl_transactions where  caller_reference='".$status['caller_reference']."' ";
					$res=$this->CI->transactions->getdata($query);
					
					if(count($res)>0){
						$trans_id=$res['0']->pk_transaction_id;
						$amount=$res['0']->amount;
						$company_id=$res['0']->fk_company_id;
						 $sql="select * from tbl_comptrans where company_id='".$company_id."'   and transaction_id='".$trans_id."'";
						$trans=$this->CI->transactions->getdata($sql);
						 
						if(count($trans)<1){
							$query="insert into  tbl_comptrans set company_id='".$company_id."', amount= '".$amount."', transaction_id='".$trans_id."'";
							$this->CI->transactions->saveDwollaOrder($query);
						} 
						
					}
					//$this->CI->transactions->saveDwollaOrder($query);
					
					/***** End code For Tracking order by company ********/
					
					
					//fire a confirmation email.
					$this->CI->transactions->sendOrderConfirmation($status['caller_reference']);
					
                    break;
                case 'Canceled':
                case 'TransactionDenied':
                    $this->CI->transactions->updateTransaction(array("status" => "UserAction",
                        "amazonStatus" => $status['status_code'],
                        "amazonError" => $status['status_message'],
                        "description" => $status['status_message']),
                            $callerReference, $atStep);
                    //TODO: Regenerate URL
                    break;
                case 'PendingNetworkResponse':
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $status['status_code'],
                        "amazonError" => $status['status_message'],
                        "description" => $status['status_message']),
                            $callerReference, $atStep);
                    break;
                case 'PendingVerification':
                case 'Expired':
                    $this->CI->transactions->updateTransaction(array("status" => "REVIEW",
                        "amazonStatus" => $status['status_code'],
                        "amazonError" => $status['status_message'],
                        "description" => $status['status_message']),
                            $callerReference, $atStep);
                    break;
            }
        } else {
            switch ($status['error_code']) {
                case 'InvalidParams':
                case 'AuthFailure':
                case 'InvalidClientTokenId':
                case 'SignatureDoesNotMatch':
                case 'InvalidTransactionId':

                    $this->CI->transactions->updateTransaction(array("status" => "REVIEW",
                        "amazonStatus" => $status['error_code'],
                        "amazonError" => $status['exception_message']),
                            $callerReference, $atStep);
                    break;


                case 'AccessFailure': $description = 'Your account cannot be accessed.';
                    $this->CI->transactions->updateTransaction(array("status" => "UserAction",
                        "amazonStatus" => $status['error_code'],
                        "amazonError" => $status['exception_message'],
                        "description" => $description),
                            $callerReference, $atStep);
                    break;

                case 'InternalError':
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $status['error_code'],
                        "amazonError" => $status['exception_message']),
                            $callerReference, $atStep);
                    break;

                default:
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $status['error_code'],
                        "amazonError" => $status['exception_message']),
                            $callerReference, $atStep);
                    break;
            }
        }
    }

    /*
     * Gets missing tokens
     */

    public function getTokenByCaller($callerReference) {
        $atStep = $this->CI->transactions->TXN_INITIATED;
        $this->initializeGetTokenByCaller();
        $this->request->setCallerReference($callerReference);

        $token = $this->invokeGetTokenByCaller($this->service, $this->request);
        if ($token['success']) {
            $this->CI->transactions->updateTransaction(array("status" => "READY",
                "stepCompleted" => 1,
                "tokenId" => $token['token_id']),
                    $token['caller_reference'],$atStep);
        } else {
            switch ($token['error_code']) {
                case 'InvalidCallerReference':
                    $this->CI->transactions->updateTransaction(array("status" => "ABORTED",
                        "amazonStatus" => $token['error_code'],
                        "amazonError" => $token['xml']),
                            $callerReference, $atStep);
                    break;
                case 'InternalError':
                    $this->CI->transactions->updateTransaction(array("status" => "RETRY",
                        "amazonStatus" => $token['error_code'],
                        "amazonError" => $token['xml']),
                            $callerReference, $atStep);
                    break;

                case 'AccountClosed':
                    $description = "Your account is closed.";
                case 'AccessFailure':
                    $description = isset($description) ?
                            $description : "Your account cannot be accessed.";

                    $this->CI->transactions->updateTransaction(array("status" => "UserAction",
                        "amazonStatus" => $token['error_code'],
                        "amazonError" => $token['xml'],
                        "description" => $description),
                            $callerReference, $atStep);
                    break;

                case 'AuthFailure':
                case 'InvalidClientTokenId':
                case 'InvalidTokenId':
                case 'SignatureDoesNotMatch':
                case 'InvalidParams':
                    $this->CI->transactions->updateTransaction(array("status" => "REVIEW",
                        "amazonStatus" => $token['error_code'],
                        "amazonError" => $token['xml']),
                            $callerReference, $atStep);
                    break;
            }
        }
    }

    /**
     * This approach allows us to load classes only when they are required
     */
    private function initializeGenerateURL() { // recurring payment class
        if (!$this->pipeline instanceof Amazon_FPS_CBUIRecurringTokenPipeline) {
            $this->pipeline = new Amazon_FPS_CBUIRecurringTokenPipeline(self::ACCESS_KEY, self::SECRET_KEY);
        }
    }
    
        private function initializeGenerateURL_singlepayment() { // single payment class
        if (!$this->pipeline instanceof Cbui_singleuse_pipeline) {
            $this->pipeline = new Cbui_singleuse_pipeline(self::ACCESS_KEY, self::SECRET_KEY);
        }
    }

    /**
     * This approach allows us to load classes only when they are required
     */
    private function initializeInvokePay() {
        if (!$this->service instanceof Amazon_FPS_Client) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Client.php');
            $this->service = new Amazon_FPS_Client(self::ACCESS_KEY, self::SECRET_KEY);
        }
        if (!$this->request instanceof Amazon_FPS_Model_PayRequest) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Model/PayRequest.php');
            $this->request = new Amazon_FPS_Model_PayRequest();
        }
        if (!$this->amount instanceof Amazon_FPS_Model_Amount) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Model/Amount.php');
            $this->amount = new Amazon_FPS_Model_Amount();
            $this->amount->setCurrencyCode("USD");
        }
        require_once (APPPATH . 'libraries/Amazon/FPS/Model/DescriptorPolicy.php');
    }

    /**
     * This approach allows us to load classes only when they are required
     */
    private function initializeGetTransactionStatus() {
        if (!$this->service instanceof Amazon_FPS_Client) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Client.php');
            $this->service = new Amazon_FPS_Client(self::ACCESS_KEY, self::SECRET_KEY);
        }
        if (!$this->request instanceof Amazon_FPS_Model_GetTransactionStatusRequest) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Model/GetTransactionStatusRequest.php');
            $this->request = new Amazon_FPS_Model_GetTransactionStatusRequest();
        }
    }

    /**
     * This approach allows us to load classes only when they are required
     */
    private function initializeGetTokenByCaller() {
        if (!$this->service instanceof Amazon_FPS_Client) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Client.php');
            $this->service = new Amazon_FPS_Client(self::ACCESS_KEY, self::SECRET_KEY);
        }
        if (!$this->request instanceof Amazon_FPS_Model_GetTransactionStatusRequest) {
            require_once(APPPATH . 'libraries/Amazon/FPS/Model/GetTokenByCallerRequest.php');
            $this->request = new Amazon_FPS_Model_GetTokenByCallerRequest();
        }
    }

    private function getTokenID() {
        $parameters = array();
        $params = array("signature", "signatureVersion", "signatureMethod",
            "expiry", "certificateUrl", "tokenID", "status", "callerReference");
        foreach ($params as $param) {
            $parameters[$param] = urlencode($this->CI->input->get($param, true));
        }

        return $parameters;
    }

    /**
     * Pay Action Sample
     *
     * Allows calling applications to move money from a sender to a recipient.
     *
     * @param Amazon_FPS_Interface $service instance of Amazon_FPS_Interface
     * @param mixed $request Amazon_FPS_Model_Pay or array of parameters
     */
    private function _invokePay(Amazon_FPS_Interface $service, $request) {
        $return_array = array("success" => FALSE);
        try {
            $response = $service->pay($request);

            if ($response->isSetPayResult()) {
                $payResult = $response->getPayResult();
                if ($payResult->isSetTransactionId()) {
                    $return_array['transaction_id'] = $payResult->getTransactionId();
                }
                if ($payResult->isSetTransactionStatus()) {
                    $return_array['transaction_status'] = $payResult->getTransactionStatus();
                }
            }
            if ($response->isSetResponseMetadata()) {
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId()) {
                    $return_array['request_id'] = $responseMetadata->getRequestId();
                }
            }
            $return_array['success'] = TRUE;
        } catch (Amazon_FPS_Exception $ex) {
            $return_array['success'] = FALSE;
            $return_array['exception_message'] = $ex->getMessage();
            $return_array['status_code'] = $ex->getStatusCode();
            $return_array['error_code'] = $ex->getErrorCode();
            $return_array['error_type'] = $ex->getErrorType();
            $return_array['request_id'] = $ex->getRequestId();
            $return_array['xml'] = $ex->getXML();
        }
        return $return_array;
    }

    /**
     * Get Transaction Status Action Sample
     *
     * Gets the latest status of a transaction.
     *
     * @param Amazon_FPS_Interface $service instance of Amazon_FPS_Interface
     * @param mixed $request Amazon_FPS_Model_GetTransactionStatus or array of parameters
     */
    private function invokeGetTransactionStatus(Amazon_FPS_Interface $service, $request) {
        $return_array = array("success" => FALSE);
        try {
            $response = $service->getTransactionStatus($request);

            if ($response->isSetGetTransactionStatusResult()) {
                $getTransactionStatusResult = $response->getGetTransactionStatusResult();

                if ($getTransactionStatusResult->isSetTransactionId()) {
                    $return_array['transaction_id'] = $getTransactionStatusResult->getTransactionId();
                }
                if ($getTransactionStatusResult->isSetTransactionStatus()) {
                    $return_array['transaction_status'] = $getTransactionStatusResult->getTransactionStatus();
                }
                if ($getTransactionStatusResult->isSetCallerReference()) {
                    $return_array['caller_reference'] = $getTransactionStatusResult->getCallerReference();
                }
                if ($getTransactionStatusResult->isSetStatusCode()) {
                    $return_array['status_code'] = $getTransactionStatusResult->getStatusCode();
                }
                if ($getTransactionStatusResult->isSetStatusMessage()) {
                    $return_array['status_message'] = $getTransactionStatusResult->getStatusMessage();
                }
            }

            if ($response->isSetResponseMetadata()) {
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId()) {
                    $return_array['request_id'] = $responseMetadata->getRequestId();
                }
            }
            $return_array["success"] = TRUE;
        } catch (Amazon_FPS_Exception $ex) {
            $return_array["success"] = FALSE;
            $return_array['exception_message'] = $ex->getMessage();
            $return_array['status_code'] = $ex->getStatusCode();
            $return_array['error_code'] = $ex->getErrorCode();
            $return_array['error_type'] = $ex->getErrorType();
            $return_array['request_id'] = $ex->getRequestId();
            $return_array['xml'] = $ex->getXML();
        }

        return $return_array;
    }

    /**
     * Get Token By Caller Action Sample
     *
     * Returns the details of a particular token installed by this calling application using the subway co-branded UI.
     *
     * @param Amazon_FPS_Interface $service instance of Amazon_FPS_Interface
     * @param mixed $request Amazon_FPS_Model_GetTokenByCaller or array of parameters
     */
    private function invokeGetTokenByCaller(Amazon_FPS_Interface $service, $request) {
        $return_array = array("success" => FALSE);
        try {
            $response = $service->getTokenByCaller($request);

            if ($response->isSetGetTokenByCallerResult()) {

                $getTokenByCallerResult = $response->getGetTokenByCallerResult();
                if ($getTokenByCallerResult->isSetToken()) {

                    $token = $getTokenByCallerResult->getToken();
                    if ($token->isSetTokenId()) {

                        $return_array['token_id'] = $token->getTokenId();
                    }
                    if ($token->isSetFriendlyName()) {

                        $return_array['friendly_name'] = $token->getFriendlyName();
                    }
                    if ($token->isSetTokenStatus()) {
                        $return_array['status'] = $token->getTokenStatus();
                    }
                    if ($token->isSetDateInstalled()) {

                        $return_array['date_installed'] = $token->getDateInstalled();
                    }
                    if ($token->isSetCallerReference()) {
                        $return_array['caller_reference'] = $token->getCallerReference();
                    }
                    if ($token->isSetTokenType()) {

                        $return_array['token_type'] = $token->getTokenType();
                    }
                    if ($token->isSetOldTokenId()) {
                        $return_array['old_token_id'] = $token->getOldTokenId();
                    }
                    if ($token->isSetPaymentReason()) {

                        $return_array['payment_reason'] = $token->getPaymentReason();
                    }
                }
            }
            if ($response->isSetResponseMetadata()) {
                $responseMetadata = $response->getResponseMetadata();
                if ($responseMetadata->isSetRequestId()) {
                    $return_array['request_id'] = $responseMetadata->getRequestId();
                }
            }
            $return_array["success"] = TRUE;
        } catch (Amazon_FPS_Exception $ex) {
            $return_array["success"] = FALSE;
            $return_array['exception_message'] = $ex->getMessage();
            $return_array['status_code'] = $ex->getStatusCode();
            $return_array['error_code'] = $ex->getErrorCode();
            $return_array['error_type'] = $ex->getErrorType();
            $return_array['request_id'] = $ex->getRequestId();
            $return_array['xml'] = $ex->getXML();
        }
        return $return_array;
    }

}
