<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH."third_party/MX/Router.php";
require_once APPPATH."libraries/CompanyRouter.php";

class MY_Router extends MX_Router {

    /** Locate the controller **/
    public function locate($segments) {
            $this->module = '';
            $this->directory = '';
            $ext = $this->config->item('controller_suffix').EXT;

            /* use module route if available */
            if (isset($segments[0]) AND $routes =
                    Modules::parse_routes($segments[0], implode('/', $segments))) {
                    $segments = $routes;
            }

            /* get the segments array elements */
            list($module, $directory, $controller) = array_pad($segments, 3, NULL);
            
            foreach (Modules::$locations as $location => $offset) {

                    /* module exists? */
                    if (is_dir($source = $location.$module.'/controllers/')) {

                            $this->module = $module;
                            $this->directory = $offset.$module.'/controllers/';

                            /* module sub-controller exists? */
                            if($directory AND is_file($source.$directory.$ext)) {
                                    return array_slice($segments, 1);
                            }

                            /* module sub-directory exists? */
                            if($directory AND is_dir($module_subdir = $source.$directory.'/')) {

                                    $this->directory .= $directory.'/';

                                    /* module sub-directory controller exists? */
                                    if(is_file($module_subdir.$directory.$ext)) {
                                            return array_slice($segments, 1);
                                    }

                                    /* module sub-directory sub-controller exists? */
                                    if($controller AND is_file($module_subdir.$controller.$ext)){
                                            return array_slice($segments, 2);
                                    }
                            }

                            /* module controller exists? */
                            if(is_file($source.$module.$ext)) {
                                    return $segments;
                            }
                    }
            }
//            require_once APPPATH.'/helpers/dump_helper.php';
//            new Dump(debug_backtrace(),NULL,TRUE);exit;
            $segments = CompanyRouter::initalize($segments);
            $company = CompanyRouter::$company;
            /* get the segments array elements */
            list($controller,$method) = array_pad($segments, 2, NULL);

            /* company controller exists? */
            if(CompanyRouter::$is_corporate && is_file(APPPATH.'controllers/'.$company['url'].'/'.
                    $company['url']."_".$controller.$ext)) {
                    $this->directory = $company['url'].'/';
                    return array($company['url']."_".$controller,$method);
            }

            /* corporate controller exists? */
            if(CompanyRouter::$is_corporate && is_file(APPPATH.'controllers/'.CompanyRouter::CORPORATEFOLDER.'/'.
                    CompanyRouter::CORPORATEFOLDER."_".$controller.$ext)) {
                    $this->directory = CompanyRouter::CORPORATEFOLDER.'/';
                    return array(CompanyRouter::CORPORATEFOLDER."_".$controller,$method);
            }

            if(strlen($controller)>0){
                /* application controller exists? */
                if(is_file(APPPATH.'controllers/'.$controller.$ext)) {
                        return $segments;
                }
            }
            else{
                /* company default controller exists? */
                if(CompanyRouter::$is_corporate && is_file(APPPATH.'controllers/'.CompanyRouter::CORPORATEFOLDER.'/'.
                        CompanyRouter::CORPORATEFOLDER.'/'.CompanyRouter::CORPORATEFOLDER.
                        "_".$this->default_controller.$ext)) {
                        $this->directory = CompanyRouter::CORPORATEFOLDER.'/'.
                                            $company['url'].'/';
                        return array($company['url']."_".$this->default_controller);
                }

                /* corporate default controller exists? */
                if(CompanyRouter::$is_corporate && is_file(APPPATH.'controllers/'.CompanyRouter::CORPORATEFOLDER.'/'.
                        CompanyRouter::CORPORATEFOLDER."_".$this->default_controller.$ext)) {
                        $this->directory = CompanyRouter::CORPORATEFOLDER.'/';
                        return array(CompanyRouter::CORPORATEFOLDER."_".$this->default_controller);
                }

                /* base default controller exists? */
                if(is_file(APPPATH.'controllers/'.$this->default_controller.$ext)) {
                        $this->directory = '';
                        return array($this->default_controller);
                }
            }
    }
}